Guide to contributing to X11-Basic_examples
===========================================

If you have written a X11-Basic program, which you would like to see 
included into this collection, please do not hesitate to contribute
your program. Send me an email, or trigger a merge request on this repro.

The language X11-Basic itself as well as the reference implentation for linux have a separate git repository. It is called X11Basic and can be found eiter 
on codeberg aswell as on github.

## License and attribution

All contributions must be properly licensed and attributed. 
If you are contributing your own original work, then you are offering it under 
a CC-BY license (Creative Commons Attribution). If it is code, you are 
offering it under the GPL-v2. You are responsible for adding your own name or 
pseudonym in the Acknowledgments file, as attribution for your contribution.

If you are sourcing a contribution from somewhere else, it must carry a
compatible license. The project was initially released under the GNU public
licence GPL-v2 which means that contributions must be licensed under open
licenses such as MIT, CC0, CC-BY, etc. You need to indicate the original source
and original license, by including a comment above your contribution. 

## Contributing with a Merge or Pull Request

The best way to contribute to this project is by making a merge/pull request:

1. Login with your codeberg account or create one now
2. [Fork](https://codeberg.org/kollo/X11-Basic_examples.git) the X11-Basic Example collection repository. 
Work on your fork.
3. Create a new branch on which to make your change, e.g.
`git checkout -b my_code_contribution`, or make the change on the `new` branch.
4. Edit the file where you want to make a change or create a new file in the 
   `contrib` directory if you're not sure where your contribution might fit.
5. Edit `ACKNOWLEGEMENTS` and add your own name to the list of contributors 
   under the section with the current year. Use your name, or a 
   codeberg/github ID, or a pseudonym.
6. Commit your change. Include a commit message describing the correction.
7. Submit a merge/pull request against the X11-Basic_example repository.

## Contributing with an Issue

If you find a mistake and you're not sure how to fix it, or you don't know how
to do a merge/pull request, then you can file an Issue. Filing an Issue will 
help us see the problem and fix it.

Create a [new Issue](https://codeberg.org/kollo/X11-Basic_examples/issues/new?issue) now!


## Thanks

We are very grateful for your support. With your help, this BASIC collection
will be a great project. 

best regards
Markus Hoffmann, October 2020


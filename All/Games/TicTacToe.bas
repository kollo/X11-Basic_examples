'
'	TIC-TAC-TOE   for X11-Basic
' Ported from Small-Basic
'

DIM MAT(9+1),PW(8+1),ST(3+1)
SIZEW ,160,160
gelb=GET_COLOR(65535,65535,0)
RESTART:
ARRAYFILL mat(),0
CLEARW
VSYNC
COLOR gelb,schwarz
TEXT 20,150,"Tic-Tac-Toe"
player=(FORM_ALERT(1,"[0][Beginner ?][ME|YOU]")-1)*2-1
winner=0

' start
WHILE winner=0
  IF player=-1
    GOSUB DISPLAY
    GOSUB USER
  ELSE
    GOSUB COMPUTER
  ENDIF
  GOSUB CHECK
  player=-player
WEND
GOSUB DISPLAY
DEFMOUSE 0
IF WINNER=-1
  ~FORM_ALERT(1,"[0][YOU WIN!][OK]")
ELSE IF WINNER=1
  ~FORM_ALERT(1,"[0][I WIN!][OK]")
ELSE
  ~FORM_ALERT(1,"[0][TIE!][OK]")
ENDIF
IF FORM_ALERT(2,"[0][Play again?][YES|NO]")=1
  GOTO RESTART
ENDIF
QUIT

'===================
PROCEDURE USER
  DEFMOUSE 0
  VSYNC
  WHILE 1
    IF MOUSEK
      x=MOUSEX
      y=MOUSEY
      REPEAT
      UNTIL MOUSEK=0
      p=INT((x-20)/40)+1
      IF y>80
        p=p+6
      ELSE IF y>40
        p=p+3
      ENDIF
      IF mat(p)=0
        GOTO L3000
      ELSE
        BEEP
      ENDIF
    ENDIF
  WEND
  L3000: ! exit while
  DEFMOUSE 2
  MAT(P)=PLAYER
RETURN

'===================
PROCEDURE COMPUTER
  PLAYER=1
  GOSUB Check

  ' WINNER MOVE
  FOR I=1 TO 8
    IF PW(I)=2
      GOSUB SELPOS
      GOTO CCFIN
    ENDIF
  NEXT

  ' DEFENCE MOVE
  FOR I=1 TO 8
    IF PW(I)=-2
      GOSUB SELPOS
      GOTO CCFIN
    ENDIF
  NEXT
  '
  ' SIMPLE MOVE - THIS LETS USER TO WIN
  ' BECAUSE IT DOES NOT CALCULATE THE
  ' FREE CELLS
  '
  IF MAT(5)=0
    P=5
  ELSE
    FOR I=1 TO 9
      IF MAT(I)=0
        P=I
        GOTO CCFIN
      ENDIF
    NEXT
  ENDIF
  CCFIN:
  MAT(P)=PLAYER
RETURN

'===================
PROCEDURE SELPOS
  IF I<4
    ST(1)=(I-1)*3+1
    ST(2)=(I-1)*3+2
    ST(3)=(I-1)*3+3
  ELSE IF I<7
    ST(1)=(I-3)
    ST(2)=(I-3)+3
    ST(3)=(I-3)+6
  ELSE IF I=7
    ST(1)=1
    ST(2)=5
    ST(3)=9
  ELSE
    ST(1)=3
    ST(2)=5
    ST(3)=7
  ENDIF
  IF MAT(ST(1))=0
    P=ST(1)
  ELSE IF MAT(ST(2))=0
    P=ST(2)
  ELSE
    P=ST(3)
  ENDIF
RETURN

'===================
PROCEDURE Check
  FOR i=1 TO 3
    PW(i)=MAT((i-1)*3+1)
    PW(i)=PW(i)+MAT((i-1)*3+2)
    PW(i)=PW(i)+MAT((i-1)*3+3)
    PW(i+3)=MAT(i)+MAT(i+3)+MAT(i+6)
  NEXT
  PW(7)=MAT(1)+MAT(5)+MAT(9)
  PW(8)=MAT(3)+MAT(5)+MAT(7)
  FOR i=1 TO 8
    IF PW(i)=-3
      WINNER=-1
    ENDIF
    IF PW(i)=3
      WINNER=1
    ENDIF
  NEXT
  IF WINNER=0
    CNT=0
    FOR i=1 TO 9
      IF MAT(i)
        CNT=CNT+1
      ENDIF
    NEXT
    IF CNT=9
      WINNER=999
    ENDIF
  ENDIF
RETURN

'===================
PROCEDURE DISPLAY
  COLOR schwarz
  PBOX 0,140,160,160
  COLOR gelb,schwarz
  TEXT 20,160,"Tic-Tac-Toe"
  COLOR GET_COLOR(65535,0,0)
  PBOX 0,0,160,140
  COLOR GET_COLOR(65535,65535,65535)
  LINE 20,40,140,40
  LINE 20,80,140,80
  LINE 60,0,60,120
  LINE 100,0,100,120
  FOR N=1 TO 9 STEP 3
    y=INT(n/3)*40+20
    FOR O=0 TO 2
      x=(o+1)*40
      IF MAT(O+N)=1
        CIRCLE x,y,10
      ELSE IF MAT(O+N)=-1
        LINE x-10,y-10,x+10,y+10
        LINE x+10,y-10,x-10,y+10
      ENDIF
    NEXT
  NEXT
  VSYNC
RETURN

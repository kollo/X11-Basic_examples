' Program for sending email with arbitrary Information  V.1.01
' x11basic example     (c) Markus Hoffmann

i=1
WHILE len(PARAM$(i))
  a$=PARAM$(i)
  IF a$="-s"
    INC i
    subject$=PARAM$(i)
  ELSE if a$="-f"
    INC i
    from$=PARAM$(i)
  ELSE if a$="-i"
    INC i
    heloid$=PARAM$(i)
  ELSE if a$="-d"
    INC i
    sdate$=PARAM$(i)
  ELSE
    to$=a$
  ENDIF
  INC i
WEND

portnr=25
server$="localhost"

SPLIT to$,"@",1,user$,domain$
IF right$(domain$)=">"
  domain$=LEFT$(domain$,LEN(domain$)-1)
ENDIF

IF len(subject$)=0
  subject$="Greeting from X11Basic User !"
ENDIF
IF len(heloid$)=0
  heloid$=env$("HOSTNAME")
ENDIF
IF len(from$)=0
  from$="<"+env$("USER")+"@"+env$("HOSTNAME")+">"
ENDIF
IF len(to$)=0 OR glob(to$,"*<*@*>")=0
  to$="<kollo@users.sourceforge.net>"
ENDIF
IF len(sdate$)=0
  sdate$=date$+" "+time$
ENDIF
inhalt$="Das ist ein Test."+CHR$(10)+env$("PWD")+CHR$(10)
inhalt$=inhalt$+"Automatisch generiert von Fakemail.bas"+CHR$(10)
inhalt$=inhalt$+system$("xbasic -e version")
OPEN "UC",#1,server$,portnr
@listen
@docmd("HELO "+heloid$)
@docmd("MAIL FROM: "+from$)
@docmd("RCPT TO: "+to$)
@docmd("DATA")

IF len(msgid$)
  PRINT #1,"Message-ID: ";msgid$
ENDIF
IF len(sdate$)
  PRINT #1,"Date: ";sdate$
ENDIF
PRINT #1,"From: "+from$
PRINT #1,"Reply-To: "+from$
PRINT #1,"To: "+to$
IF len(subject$)
  PRINT #1,"Subject: ";subject$
ENDIF
WHILE len(inhalt$)
  SPLIT inhalt$,CHR$(10),1,a$,inhalt$
  IF a$="."
    a$=".."
  ENDIF
  PRINT #1,a$
WEND
PRINT #1,"."
FLUSH #1
@listen
@docmd("QUIT")
@listen
CLOSE #1
QUIT

PROCEDURE listen
  PAUSE 0.3
  WHILE INP?(#1)
    LINEINPUT #1,rep$
    PRINT "<-";rep$
  WEND
RETURN

PROCEDURE docmd(c$)
  PRINT "->";c$
  PRINT #1,c$
  FLUSH #1
  PAUSE 1
  WHILE INP?(#1)>0
    LINEINPUT #1,rep$
    PRINT "<-";rep$
  WEND
RETURN

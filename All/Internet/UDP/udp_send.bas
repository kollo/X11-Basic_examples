'
' This Program demonstrates the use of UDP-Packets
'
' This sends messages (basically strings with the time information)
' to a receiving program (see udp_receive.bas) on a different
' host computer.
'
' You need to run the udp_receive.bas first, then you can send messages
' to it.
'
'                                               (c) Markus Hoffmann 2005

' This must be the port of the receiver
port=5555
' server$="192.168.1.2" ! The hostnale or IP adress of the receiving computer
server$="localhost"     ! if the receiver runs on the same computer

OPEN "UU",#1,"sender",port+1
CONNECT #1,server$,port
i=0
DO
  @sendmessage(i,"The time is: "+date$+" "+time$+" "+str$(i))
  WHILE INP?(#1)
    t$=@getmessage$()
    IF LEN(t$)
      a=CVI(LEFT$(t$,2))
      PRINT "received: ";a;" ";RIGHT$(t$,LEN(t$)-2)
    ENDIF
  WEND
  INC i
  PAUSE 1
LOOP
CLOSE #1
QUIT

' compose a packet. It consists of binary data and should not exceed 1500 bytes.
' This is a very simple implementation. You should encode a packet number to
' make sure they are received in order. You can also use crc() etc. to detect
' transmission errors. But this is not necessary with UDP anymore.
' You can also implement any protocol you like.

PROCEDURE sendmessage(id,m$)
  LOCAL s$
  PRINT "sending packet #";id
  s$=mki$(id)+m$
  SEND #1,s$
RETURN
PROCEDURE sendmessage_adr(id,m$,adr)
  LOCAL s$
  PRINT "sending packet #";id
  s$=mki$(id)+m$
  SEND #1,s$,adr,port
RETURN
PROCEDURE sendACK(pid,adr)
  @sendmessage_adr(6,CHR$(pid),adr)
RETURN
PROCEDURE sendnack(adr)
  @sendmessage_adr(21,"",adr)
RETURN
FUNCTION getmessage$()
  LOCAL t$,adr
  RECEIVE #1,t$,adr
  pid=CVI(MID$(t$,1,2))
  IF pid=0
    @sendACK(pid,adr)
  ENDIF
  RETURN t$
ENDFUNCTION

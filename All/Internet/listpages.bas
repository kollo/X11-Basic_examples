#!/usr/bin/xbasic

DIM words$(10000)
anzwords=0

@getwords
@savewords
QUIT

PROCEDURE savewords
  OPEN "O",#1,"/dev/shm/wikiwords.dat"
  FOR i=0 TO anzwords-1
    PRINT #1,words$(i)
  NEXT i
  CLOSE #1
RETURN

PROCEDURE getwords
  content$=@getpagecontent$("Special:Allpages")
  SPLIT content$,"title=",0,a$,content$
  WHILE len(content$)
    SPLIT content$,"title=",0,a$,content$
    IF left$(a$)=CHR$(34)
      a$=RIGHT$(a$,LEN(a$)-1)
      SPLIT a$,CHR$(34),0,a$,b$
      words$(anzwords)=a$
      INC anzwords
    ENDIF
  WEND
  PRINT anzwords;" Woerter."
RETURN

FUNCTION getpagecontent$(p$)
  LOCAL page$
  LOCAL server$
  LOCAL a$,b$,t$
  ret$=""
  server$="mskpc14.desy.de"
  page$="/wiki/index.php/"+p$
  OPEN "UC",#1,server$,80
  PRINT #1,"GET "+page$+" HTTP/1.0"+CHR$(13)
  PRINT #1,"Host: "+server$+CHR$(13)
  PRINT #1,"User-Agent: X11-Basic/1.12"+CHR$(13)
  PRINT #1,CHR$(13)
  FLUSH #1
  LINEINPUT #1,response$
  SPLIT response$," ",0,protocol$,response$
  SPLIT response$," ",0,htmlerror$,response$
  PRINT "Response: ";response$
  IF val(htmlerror$)=200
    ' Parse Header
    LINEINPUT #1,t$
    t$=REPLACE$(t$,CHR$(13),"")
    WHILE len(t$)
      PRINT t$,LEN(t$)
      SPLIT t$,":",0,a$,b$
      IF a$="Content-Length"
        length=VAL(b$)
      ENDIF
      LINEINPUT #1,t$
      t$=REPLACE$(t$,CHR$(13),"")
    WEND
    PRINT "Len=";length;" Bytes."
    IF length
      ret$=input$(#1,length)
    ELSE
      WHILE not eof(#1)
        ret$=ret$+input$(#1,100)
      WEND
    ENDIF
  ELSE
    PRINT "Error: could not get data from the WIKI!"
  ENDIF
  CLOSE #1
  RETURN replace$(ret$,CHR$(13),"")
ENDFUNCTION

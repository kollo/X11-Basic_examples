'
' generate fake email adresses (which can be identified)
'
'
'
pin=1234     ! Pin for this session

dim ln$(1024)
anzln=0
dim bn$(1024)
anzbn=0
dim gn$(1024)
anzgn=0


outputfile$="fakelist.txt"

dim providers$(100)
CLR anzproviders

read a$
WHILE a$<>"***"
  providers$(anzproviders)=a$
  inc anzproviders
  read a$
wend

RANDOMIZE

open "I",#1,"last_names.txt"
while not eof(#1)
  lineinput #1,t$
  t$=trim$(t$)
  a$=word$(t$,1)
  ln$(anzln)=lower$(a$)
  inc anzln
wend
close #1
print anzln;" last names."

open "I",#1,"boy_names.txt"
while not eof(#1)
  lineinput #1,t$
  t$=trim$(t$)
  a$=word$(t$,1)
  if len(a$)
    bn$(anzbn)=lower$(a$)+"g"
    inc anzbn
  endif
wend
close #1
print anzbn;" boy names."

open "I",#1,"girl_names.txt"
while not eof(#1)
  lineinput #1,t$
  t$=trim$(t$)
  a$=word$(t$,1)
  gn$(anzgn)=lower$(a$)
  inc anzgn
wend
close #1
print anzgn;" girl names."

! Alphabetically sort the names lists

SORT ln$(),anzln
SORT bn$(),anzbn
SORT gn$(),anzgn

' for i=0 to 200
'   print i,bn$(i)
' next i

open "O",#2,outputfile$
i=0
while i<32
  bg=int(random(2))
  a=int(random(anzbn))
  b=int(random(anzln))
  code=b+a*anzln+anzln*anzbn*pin
  if bg=0
    vorname$=UPPER$(left$(bn$(a)))+right$(bn$(a),len(bn$(a))-1)
  else
    vorname$=UPPER$(left$(gn$(a)))+right$(gn$(a),len(gn$(a))-1)
  endif
  hash=code MOD 257 ! prime
'  print hash
  if hash=0
  '  print hex$(code);" ";
    nachname$=UPPER$(left$(ln$(b)))+right$(ln$(b),len(ln$(b))-1)
    print vorname$+" "+nachname$+" <"+lower$(vorname$)+"."+lower$(nachname$)+str$(10+int(random(100)))+"@"+lower$(providers$(INT(RANDOM(anzproviders))))+">"
    print #2,vorname$+" "+nachname$+" <"+lower$(vorname$)+"."+lower$(nachname$)+str$(10+int(random(100)))+"@"+lower$(providers$(INT(RANDOM(anzproviders))))+">"
   inc i
    ' lets check if the name can be detected as fake:
    f=@isfake(vorname$,nachname$)
    if f=0
      print "Something is wrong!"
    else
      print "OK."
    endif
  endif
wend
print @isfake("Markus","Hoffmann") ! my name is real !
print @isfake(bn$(0),ln$(0))       ! This name is in the list, but not fake!

quit

' Return 0 if the given surname and last name is real (not fake)

function isfake(v$,n$)
  local vidx,nidx
  print v$+" "+n$+" ";
  vidx=@get_vidx(v$)
  nidx=@get_nidx(n$)
  if vidx<0 or nidx<0
    return 0
  endif
  code=nidx+vidx*anzln+anzln*anzbn*pin
  hash=code MOD 257 ! prime
  if hash=0
    return 1
  endif
  return 0
endfunction

function get_vidx(n$)
  local i
  for i=0 to anzbn-1
    if lower$(bn$(i))=lower$(n$)
      return i
    endif
  next i
  for i=0 to anzgn-1
    if lower$(gn$(i))=lower$(n$)
      return i
    endif
  next i
  return -1
endfunction
function get_nidx(n$)
  local i
  for i=0 to anzln-1
    if lower$(ln$(i))=lower$(n$)
      return i
    endif
  next i
  return -1
endfunction



' email providers
data gmail.com
data mail.google.com
data live.com
data icloud.com
data mail.yahoo.com
data gmx.net
data zoho.com
data mail.aol.com
data mail.com
data inbox.com
data protonmail.com
data ***


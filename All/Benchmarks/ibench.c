/*reference benchmark from http://forum.basicprogramming.org 

corresponds to x11basicibench.bas

*/


#include <stdio.h>

int main() {
    int left_edge, right_edge, top_edge, bottom_edge, max_iter,
        x_step, y_step, y0, x0, x, y, i, x_x, y_y, temp,
        the_char, accum, count;

    accum = 0;
    count = 0;
    while (count < 1545) {
        left_edge   = -420;                     // left edge = -2.1
        right_edge  =  300;                     // right edge = 1.5
        top_edge    =  300;                     // top edge = 1.5
        bottom_edge = -300;                     // bottom edge = -1.5
        x_step      =  7;                       // x step size
        y_step      =  15;                      // y step size

        max_iter    =  200;                     // max iteration depth

        y0 = top_edge;
        while (y0 > bottom_edge) {
            x0 = left_edge;
            while (x0 < right_edge) {
                y = 0;
                x = 0;
                the_char = 32;                  // char to be displayed
                x_x = 0;
                y_y = 0;
                i = 0;
                while (i < max_iter && x_x + y_y <= 800) {
                    x_x = (x * x) / 200;
                    y_y = (y * y) / 200;
                    if (x_x + y_y > 800 ) {
                        the_char = 48 + i;      // print digit 0...9
                        if (i > 9) {
                            the_char = 64;      //  print '@'
                        }
                    } else {
                        temp = x_x - y_y + x0;
                        if ((x < 0 && y > 0) || (x > 0 && y < 0)) {
                            y = (-1 * ((-1 * (x * y)) / 100)) + y0;
                        } else {
                            y = x * y / 100 + y0;
                        }
                        x = temp;
                    }
                    i = i + 1;
                }
		// putchar(the_char);
                accum = accum + the_char;
                x0 = x0 + x_step;
            }
	   // puts("");
            y0 = y0 - y_step;
        }
        if (count % 300 == 0) {
            printf("%d\n", accum);
        }
        count = count + 1;
    }
    printf("%d\n", accum);
    return 0;
}

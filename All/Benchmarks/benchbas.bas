' ASCII Mandel benchmark in X11-Basic
' see http://forum.basicprogramming.org/
' BASIC programming forum > Off-topic > Other programming languages >
' Another Interpreter (VM or AST based) speed comparison/benchmark
'
' Result:  bytecode
'          22.596u/1.492u = 15.14
'
CLR accum%,count%
WHILE count%<1545
  left_edge%=-420
  right_edge%=300
  top_edge%=300
  bottom_edge%=-300
  x_step%=7
  y_step%=15
  max_iter%=200

  y0%=top_edge%
  WHILE y0%>bottom_edge%
    x0%=left_edge%
    WHILE x0%<right_edge%
      CLR y%,x%,x_x%,y_y%,i%
      the_char%=32
      WHILE i%<max_iter% AND x_x%+y_y%<=800
        x_x%=((x%*x%) DIV 200)
        y_y%=((y%*y%) DIV 200)
        IF x_x%+y_y%>800
          the_char%=48+i%
          IF i%>9
            the_char%=64
          ENDIF
        ELSE
          temp%=x_x%-y_y%+x0%
          IF (x%<0 AND y%>0) OR (x%>0 AND y%<0)
            y%=(-1*((-1*x%*y%) DIV 100))+y0%
          ELSE
            y%=((x%*y%) DIV 100)+y0%
          ENDIF
          x%=temp%
        ENDIF
        INC i%
      WEND
      ADD accum%,the_char%
      ADD x0%,x_step%
    WEND
    SUB y0%,y_step%
  WEND
  IF (count% MOD 300)=0
    PRINT accum%
  ENDIF
  INC count%
WEND
PRINT accum%
QUIT

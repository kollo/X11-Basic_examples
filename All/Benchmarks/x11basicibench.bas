' benchmark from http://forum.basicprogramming.org
' compile ibench with
' gcc -O3 -i ibench ibench.c
' for reference
'

CLR accum%,count%
WHILE count%<1545 ! 1065
  left_edge%=-420 ! left edge = -2.1
  right_edge%=300 ! right edge = 1.5
  top_edge%=300 ! top edge = 1.5
  bottom_edge%=-300 ! bottom edge = -1.5
  x_step%=7 ! x step size
  y_step%=15 ! y step size
  ' cls
  max_iter%=200 ! max iteration depth

  y0%=top_edge%
  WHILE y0%>bottom_edge%                    ! y0%
    x0%=left_edge%
    WHILE x0%<right_edge%                   ! x0%
      CLR i%,y%,x%,x_x%,y_y%                ! y
      the_char%=32                          ! char to be displayed
      WHILE i%<max_iter% AND x_x%+y_y%<=800 ! iteration
        x_x%=x%*x%/200                      ! x*x
        y_y%=y%*y%/200                      ! y*y
        IF x_x%+y_y%>800
          the_char%=48+i%                   ! print digit 0...9
          IF i%>9                           ! if iteration count% > 9,
            the_char%=64                    ! print '@'
          ENDIF
        ELSE
          temp%=x_x%-y_y%+x0%               ! temp% = x*x - y*y + x0%
          IF ((x%<0 AND y%>0) OR (x%>0 AND y%<0))
            y%=trunc(x%*y%/100)+y0%
          ELSE
            y%=trunc(x%*y%/100)+y0%         ! y = 2*x*y + y0%
          ENDIF
          x%=temp%                          ! x = temp%
        ENDIF
        INC i%
      WEND
      ' print chr$(the_char%);
      ADD accum%,the_char%
      ADD x0%,x_step%
    WEND
    ' print
    SUB y0%,y_step%
  WEND
  IF (count% MOD 300)=0
    PRINT accum%
  ENDIF
  INC count%
WEND
PRINT accum%
QUIT

' Contribution to the word count challenge contest at
'
' http://forum.basicprogramming.org/index.php/topic,2222.0.html
'
' by vsync
'
' Written in X11-Basic
'
'
' run the script with
' xbasic  wordcountchallenge.bas
' make sure, Bible.txt is there. You can get it from here:
' http://files.allbasic.info/ScriptBasic/Bible.txt
'
file$="Bible.txt"
OPEN "I",#1,file$
t$=INPUT$(#1,LOF(#1))
CLOSE
PRINT file$;" loaded. ";LEN(t$);" Bytes."

starttime=TIMER

strip$="()[]{}|<>/@0123456789*.,;:!#?%$&+-=_~\"+CHR$(34)+CHR$(9)+CHR$(10)+CHR$(13)

FOR i=1 TO LEN(strip$)
  a$=MID$(strip$,i,1)
  t$=REPLACE$(t$,a$," ")
NEXT i
l=LEN(t$)
t$=" "+TRIM$(LOWER$(t$))+" "
l=LEN(t$)
PRINT LEN(t$);" Bytes after cleaning up."
ot$=t$
' bsave "a.txt",varptr(t$),len(t$)
wc=TALLY(t$," ")
PRINT "there should be ";wc;" words."
PRINT "Scanning words..."
DIM words$(wc),occur(wc)
wc=0
WHILE LEN(t$)>2
  ' a$=left$(t$,INSTR(t$," ")-1)
  ' a$=word$(t$,2)
  ' print a$
  a$=""
  i=0
  WHILE PEEK(VARPTR(t$)+i+1)<>ASC(" ")
    a$=a$+CHR$(PEEK(VARPTR(t$)+i+1))
    INC i
  WEND
  ' print "<";a$;"> ";tally(t$," "+a$+" ")
  IF LEN(a$)
    words$(wc)=a$
    a$=" "+a$+" "
    occur(wc)=TALLY(ot$,a$)
    again:
    t$=REPLACE$(t$,a$," ")
    IF TALLY(t$,a$)
      GOTO again
    ENDIF
    INC wc
    IF (wc MOD 20)=0
      ol=LEN(t$)
      @progress(l,l-ol)
    ENDIF
  ENDIF
WEND
PRINT
PRINT "Actually ";wc;" different unique words found."

PRINT "sorting by occurrence..."
DIM idx%(wc)
FOR j=0 TO wc-1
  idx%(j)=j
NEXT j
idx2%()=idx%()
occur2()=occur()
SORT occur(),wc,idx%()
PRINT "The whole process took: ";TIMER-starttime;" Seconds."
PRINT "Top 30 words:"
FOR j=0 TO 30
  PRINT words$(idx%(wc-1-j)),occur(wc-1-j)
NEXT j
' If you want, sort the words alphabetically:
SORT words$(),wc,idx2%()
PRINT "words aplphabetically:"
FOR j=0 TO 30
  PRINT words$(j),occur2(idx2%(j))
NEXT j

QUIT

PROCEDURE progress(a,b)
  PRINT CHR$(13);"[";STRING$(b/a*32,"-");">";STRING$((1.03-b/a)*32,"-");"| ";STR$(INT(b/a*100),3,3);"% ]  ";b;"/";a;" ";wc;"  ";
  FLUSH
RETURN

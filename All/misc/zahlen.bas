' Zahlen als Text (deutsch)        (c) Markus Hoffmann 1999
PRINT @zahl$(101001)
PRINT "Bitte mit der Maus irgendwohin klicken ..."
ROOTWINDOW
COLOR get_color(10000,10000,45535)
PBOX 0,0,640,400
COLOR get_color(65535,65535,65535)
TEXT 100,300,"Klick mich !"
VSYNC
DO
  REPEAT
  UNTIL MOUSEK
  REPEAT
    u=MOUSEX*MOUSEY
    PRINT u,@zahl$(u)
  UNTIL MOUSEK=0
LOOP
' ################################################
FUNCTION zahl$(z)
  IF z=0
    RETURN ""
  ELSE
    IF z>1999999
      RETURN @zahl$(z DIV 1000000)+"millionen"+@zahl$(z MOD 1000000)
    ELSE IF z>999999
      RETURN "einemillionen"+@zahl$(z MOD 1000000)
    ELSE IF z>1999
      RETURN @zahl$(z DIV 1000)+"tausend"+@zahl$(z MOD 1000)
    ELSE IF z>999
      RETURN "eintausend"+@zahl$(z MOD 1000)
    ELSE IF z>99
      RETURN @einser$(z DIV 100)+"hundert"+@zehner$(z MOD 100)
    ELSE
      RETURN @zehner$(z MOD 100)
    ENDIF
  ENDIF
  RETURN ""
ENDFUNC
FUNCTION zehner$(e)
  IF e=0
  ELSE if e=1
    RETURN "eins"
  ELSE if e>=0 AND e<=9
    RETURN @einser$(e)
  ELSE if e=11
    RETURN "elf"
  ELSE if e=12
    RETURN "zw�lf"
  ELSE if e=16
    RETURN "sechzehn"
  ELSE if e=17
    RETURN "siebzehn"
  ELSE if e=10 OR (e>=13 AND e<=19)
    RETURN @einser$(e-10)+"zehn"
  ELSE if e=20
    RETURN "zwanzig"
  ELSE if e>=21 AND e<=29
    RETURN @einser$(e-20)+"undzwanzig"
  ELSE if e=30
    RETURN "drei�ig"
  ELSE if e>=31 AND e<=39
    RETURN @einser$(e-30)+"unddrei�ig"
  ELSE if e=40 OR e=50 OR e=80 OR e=90
    RETURN @einser$(e DIV 10)+"zig"
  ELSE if (e>=41 AND e<=49) OR (e>=51 AND e<=59) OR (e>=81 AND e<=89) OR (e>=91 AND e<=99)
    RETURN @einser$(e MOD 10)+"und"+@einser$(e DIV 10)+"zig"
  ELSE if e=60
    RETURN "sechzig"
  ELSE if e>=61 AND e<=69
    RETURN @einser$(e-60)+"undsechzig"
  ELSE if e=70
    RETURN "siebzig"
  ELSE if e>=71 AND e<=79
    RETURN @einser$(e-70)+"undsiebzig"
  ENDIF
  RETURN ""
ENDFUNC
FUNCTION einser$(e)
  IF e=0
    RETURN ""
  ELSE if e=1
    RETURN "ein"
  ELSE if e=2
    RETURN "zwei"
  ELSE if e=3
    RETURN "drei"
  ELSE if e=4
    RETURN "vier"
  ELSE if e=5
    RETURN "f�nf"
  ELSE if e=6
    RETURN "sechs"
  ELSE if e=7
    RETURN "sieben"
  ELSE if e=8
    RETURN "acht"
  ELSE if e=9
    RETURN "neun"
  ENDIF
  RETURN ""
ENDFUNC

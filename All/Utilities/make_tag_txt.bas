' Writes a TAG.TXT file on a SD-Card wit the actual date and time
'
'

sd_id$="78"

user$=ENV$("USER")

path$="/media/"+user$+"/"+sd_id$

if not exist(path$)
  print "SD card not found "+path$
  QUIT
endif

file$=path$+"/"+"tag.txt"
if exist(file$)
  PRINT file$+" existiert schon. Wird überschrieben."
endif
OPEN "O",#1,file$
PRINT #1,"[date]"+chr$(13)
d$=left$(date$,2)
m$=mid$(date$,4,2)
y$=right$(date$,4)
t$=time$
h$=left$(t$,2)
min$=mid$(t$,4,2)
sec$=right$(t$,2)

PRINT y$+"/"+m$+"/"+d$+" "+h$+":"+min$+":"+sec$+chr$(13)
PRINT #1,y$+"/"+m$+"/"+d$+" "+h$+":"+min$+":"+sec$+chr$(13)

CLOSE #1
quit

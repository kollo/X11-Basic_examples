' Scamble algorithm for german language. (c) Markus Hoffmann 2013-03-28
' Maybe it also works for englisch.
'
'
' The text must be descrambled with a brain!
'

' open "I",#1,"a.txt"
' t$=input$(#1,lof(#1))
' close

t$="Echt krass! Gemaess einer Studie einer Universitaet, ist es "
t$=t$+"nicht wichtig, in welcher Reihenfolge die Buchstaben in "
t$=t$+"einem Wort sind, das einzige, was wichtig ist, ist dass der "
t$=t$+"erste und letzte Buchstabe an der richtigen Position "
t$=t$+"sind. Der Rest kann ein totaler Bloedsinn sein, trotzdem "
t$=t$+"kann man ihn ohne Probleme lesen. Das ist so, weil wir "
t$=t$+"nicht jeden Buchstaben einzeln lesen, sondern das Wort "
t$=t$+"als ganzes erkennen. Echt krass! Das geht wirklich! Und "
t$=t$+"dafuer gehen wir jahrelang in die Schule! "+CHR$(10)
t$=t$+"Und als absolute Steigerung dieses hier: "
t$=t$+"DIESE MITTEILUNG ZEIGT DIR, ZU WELCHEN "
t$=t$+"GROSSARTIGEN LEISTUNGEN UNSER GEHIRN FAEHIG "
t$=t$+"IST! AM ANFANG WAR ES SICHER NOCH SCHWER, DAS "
t$=t$+"ZU LESEN, ABER MITTLERWEILE KANNST DU DAS  "
t$=t$+"WAHRSCHEINLICH SCHON GANZ GUT LESEN, OHNE "
t$=t$+"DASS ES DICH WIRKLICH ANSTRENGT. DAS LEISTET "
t$=t$+"DEIN GEHIRN MIT SEINER ENORMEN "
t$=t$+"LERNFAEHIGKEIT, BEEINDRICKEND, ODER? DU "
t$=t$+"DARFST DAS GERNE KOPIEREN, WENN DU AUCH "
t$=t$+"ANDERE DAMIT BEGEISTERN WILLST."

t$=REPLACE$(t$,"."," . ")
t$=REPLACE$(t$,","," , ")

t$=TRIM$(t$)

WHILE LEN(t$)
  SPLIT t$," ",0,a$,t$
  IF a$=","
    t2$=t2$+", "
  ELSE IF a$="."
    t2$=t2$+". "
  ELSE IF a$=UPPER$(a$)
    a$=REPLACE$(a$,"E","3")
    a$=REPLACE$(a$,"A","4")
    a$=REPLACE$(a$,"I","1")
    a$=REPLACE$(a$,"S","5")
    a$=REPLACE$(a$,"Z","7")
    a$=REPLACE$(a$,"O","0")
    a$=REPLACE$(a$,"G","6")
    a$=REPLACE$(a$,"B","8")
    t2$=t2$+a$+" "
  ELSE IF LEN(a$)>12
    a1$=LEFT$(a$,2)
    a2$=RIGHT$(a$,2)
    m$=LEFT$(a$,LEN(a$)-2)
    m$=RIGHT$(m$,LEN(m$)-2)
    m$=@permutate$(m$)
    t2$=t2$+a1$+m$+a2$+" "
  ELSE
    a1$=LEFT$(a$)
    a2$=RIGHT$(a$)
    m$=LEFT$(a$,LEN(a$)-1)
    m$=RIGHT$(m$,LEN(m$)-1)
    m$=@permutate$(m$)
    t2$=t2$+a1$+m$+a2$+" "
  ENDIF
WEND
PRINT t2$
QUIT

FUNCTION permutate$(t$)
  LOCAL i
  IF LEN(t$)
    DIM p%(LEN(t$)),r(LEN(t$))
    FOR i=0 TO LEN(t$)-1
      r(i)=RANDOM(1000)
      p%(i)=PEEK(VARPTR(t$)+i)
    NEXT i
    SORT r(),LEN(t$),p%()
    FOR i=0 TO LEN(t$)-1
      POKE VARPTR(t$)+i,p%(i)
    NEXT i
  ENDIF
  RETURN t$
ENDFUNCTION

' converts *.ans files to X11-Basic print statements.
' written in X11-basic by Markus Hoffmann 2012-10-01
'
OPEN "I",#1,"mickey.ans"
PRINT "print ";
WHILE NOT EOF(#1)
  a=INP(#1)
  IF a<32
    IF LEN(t$)
      PRINT ENCLOSE$(t$)+";"
      PRINT "print ";
      t$=""
    ENDIF
    PRINT "chr$(";a;")"+";";
  ELSE
    t$=t$+CHR$(a)
  ENDIF
WEND
IF LEN(t$)
  PRINT "print "+ENCLOSE$(t$)
ENDIF
QUIT

' Programm zum Vermessen des Sichtfeldes eines Auges

' (c) Markus Hoffmann 2002

' Erstellt eine Karte der blinden Bereiche der Netzhaut, um evtl. Augenschäden
' sichtbar zu machen
'

' Ein Auge zuhalten, mit dem anderen aus ca. 30 cm vom Bildschirm
' den roten Punkt in der Mitte fixieren.
' Das Auftauchen des weissen Punktes durch einen Tastendruck quittieren.

SIZEW ,800,800
black=COLOR_RGB(0,0,0)
white=COLOR_RGB(1,1,1)
red=COLOR_RGB(1,0,0)
yellow=COLOR_RGB(1,1,0)
COLOR black
PBOX 0,0,800,800
COLOR red
PCIRCLE 400,400,6
RANDOMIZE
SHOWPAGE
PAUSE 5
DO
  PAUSE 0.3
  x=RANDOM(80)*10
  y=RANDOM(80)*10

  COLOR yellow
  PCIRCLE x,y,4
  BEEP
  SHOWPAGE
  PAUSE 0.3
  COLOR black
  PCIRCLE x,y,4
  SHOWPAGE
  t=TIMER
  KEYEVENT
  lll=TIMER-t
  IF lll*10>25
    COLOR GET_COLOR(45535,5535,5535)
    PCIRCLE x,y,7
  ENDIF
  IF lll*10>8
    COLOR GET_COLOR(5535,5535,35535)
    TEXT x-4,y+4,STR$(INT(lll*10))
  ELSE
    COLOR GET_COLOR(5535,5535,25535)
    PCIRCLE x,y,4
  ENDIF
LOOP
QUIT

#!/usr/bin/xbasic
' raw2bmp   (c) Markus Hoffmann 200    V.1.00
' converts raw 16 Bit (5:6:5) images to .bmp files. These files can then
' easily be convertet to png or whatever with convert.
'

' the defaults

w=480 ! Large screen
h=272

w=320 ! small screen
h=240
idepth=16
depth=24

i=1
CLR inputfile$,dyn,collect$
outputfilename$="b.bmp"
WHILE LEN(PARAM$(i))
  IF LEFT$(PARAM$(i))="-"
    IF param$(i)="--help" OR PARAM$(i)="-h"
      @intro
      @using
    ELSE IF PARAM$(i)="--version"
      @intro
      QUIT
    ELSE IF PARAM$(i)="-w"
      INC i
      IF LEN(PARAM$(i))
        w=VAL(PARAM$(i))
      ENDIF
    ELSE IF PARAM$(i)="-o"
      INC i
      IF LEN(PARAM$(i))
        outputfilename$=PARAM$(i)
      ENDIF
    ELSE
      collect$=collect$+PARAM$(i)+" "
    ENDIF
  ELSE
    inputfile$=PARAM$(i)
    IF NOT EXIST(inputfile$)
      PRINT "raw2bmp: "+inputfile$+": file or path not found"
      CLR inputfile$
    ENDIF
  ENDIF
  INC i
WEND
IF LEN(inputfile$)
  PRINT "<-- (";inputfile$;
  OPEN "I",#1,inputfile$
  l=lof(#1)
  h=l/w/idepth*8

  PRINT
  PRINT "The picture appears to contain ";h;" Scanlines."

  DIM r(w,h),g(w,h),b(w,h)
  PRINT ") [";
  FOR y=0 TO h-1
    FOR x=0 TO w-1
      a=cvi(input$(#1,2))
      r(x,y)=(shr(a,11) AND 0x1f)*8
      g(x,y)=(shr(a,5) AND 0x3f)*4
      b(x,y)=(a AND 0x1f)*8
    NEXT x
    PRINT ".";
    FLUSH
  NEXT y
  CLOSE
  PRINT "] ";h

  filesize=54+3*w*h
  PRINT "--> (";outputfilename$;
  OPEN "O",#1,outputfilename$
  PRINT #1,"BM";                ! 0
  PRINT #1,mkl$(filezize);      ! 2
  PRINT #1,mki$(0);mki$(0);     ! 6
  PRINT #1,mkl$(54);            !10

  PRINT #1,mkl$(40);            !14
  PRINT #1,mkl$(w);             !18
  PRINT #1,mkl$(h);             !22
  PRINT #1,mki$(1);             !24
  PRINT #1,mki$(depth);         !26
  PRINT #1,mkl$(0);             !30
  PRINT #1,mkl$(0);             !34
  PRINT #1,mkl$(0);             !38
  PRINT #1,mkl$(0);             !42
  PRINT #1,mkl$(0);             !46
  PRINT #1,mkl$(0);             !50
  PRINT ") [";
  FOR y=h-1 downto 0
    FOR x=0 TO w-1
      r=r(x,y)
      g=g(x,y)
      b=b(x,y)
      PRINT #1,CHR$(b);CHR$(g);CHR$(r);
    NEXT x
    PRINT ".";
    FLUSH
  NEXT y
  CLOSE
  PRINT "] ";h
ELSE
  PRINT "raw2bmp: No input files"
  @usage
ENDIF
QUIT

PROCEDURE intro
  PRINT "Picture converter raw2bmp V.1.00 (c) Markus Hoffmann 2007-2008"
  VERSION
RETURN
PROCEDURE using
  PRINT "Usage: raw2bmp [options] file..."
  PRINT "Options:"
  PRINT "  -h, --help               Display this information"
  PRINT "  -o <file>                Place the output into <file>"
  PRINT "  -w <with>                specify width of image in pixels [";w;"]"
RETURN


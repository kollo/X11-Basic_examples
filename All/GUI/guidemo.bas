' gui2bas V.1.00   (c) Markus Hoffmann 2003
' convertetd demo.gui  30.07.2003 18:19:56
' convertetd demo.gui  22.10.2003 17:41:03
bx=0
by=20
bw=640
bh=400
@formular     ! execute form
QUIT
PROCEDURE formular
  LOCAL ret
  ' Little selector box (c) Markus Hoffmann 07.2003
  ' convert this with gui2bas !
  ' as an example for the use of the gui system
  ' with X11-Basic
  string0$="Select option ..."+CHR$(0)+SPACE$(0)
  string1$=""+CHR$(0)
  string2$=""+CHR$(0)
  tedinfo0$=mkl$(VARPTR(string0$))+mkl$(VARPTR(string1$))+mkl$(VARPTR(string2$))+mki$(3)+mki$(0)+mki$(2)+mki$(4513)+mki$(0)+mki$(253)+mki$(0)+mki$(0)
  obj1$=mki$(2)+mki$(-1)+mki$(-1)+mki$(22)+mki$(0)+mki$(32)+mkl$(VARPTR(tedinfo0$))+mki$(16)+mki$(16)+mki$(560)+mki$(16)
  string3$="Line 1"+CHR$(0)+SPACE$(39)
  string4$="_______________________________________"+CHR$(0)
  string5$="XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"+CHR$(0)
  tedinfo1$=mkl$(VARPTR(string3$))+mkl$(VARPTR(string4$))+mkl$(VARPTR(string5$))+mki$(3)+mki$(0)+mki$(0)+mki$(4513)+mki$(0)+mki$(1)+mki$(39)+mki$(39)
  obj3$=mki$(4)+mki$(-1)+mki$(-1)+mki$(29)+mki$(8)+mki$(0)+mkl$(VARPTR(tedinfo1$))+mki$(8)+mki$(16)+mki$(240)+mki$(16)
  string6$=""+CHR$(0)+SPACE$(39)
  string7$="_______________________________________"+CHR$(0)
  string8$="XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"+CHR$(0)
  tedinfo2$=mkl$(VARPTR(string6$))+mkl$(VARPTR(string7$))+mkl$(VARPTR(string8$))+mki$(3)+mki$(0)+mki$(0)+mki$(4513)+mki$(0)+mki$(1)+mki$(39)+mki$(39)
  obj4$=mki$(5)+mki$(-1)+mki$(-1)+mki$(29)+mki$(8)+mki$(0)+mkl$(VARPTR(tedinfo2$))+mki$(8)+mki$(32)+mki$(240)+mki$(16)
  string9$=""+CHR$(0)+SPACE$(39)
  string10$="_______________________________________"+CHR$(0)
  string11$="XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"+CHR$(0)
  tedinfo3$=mkl$(VARPTR(string9$))+mkl$(VARPTR(string10$))+mkl$(VARPTR(string11$))+mki$(3)+mki$(0)+mki$(0)+mki$(4513)+mki$(0)+mki$(1)+mki$(39)+mki$(39)
  obj5$=mki$(6)+mki$(-1)+mki$(-1)+mki$(29)+mki$(8)+mki$(0)+mkl$(VARPTR(tedinfo3$))+mki$(8)+mki$(48)+mki$(240)+mki$(16)
  string12$=""+CHR$(0)+SPACE$(39)
  string13$="_______________________________________"+CHR$(0)
  string14$="XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"+CHR$(0)
  tedinfo4$=mkl$(VARPTR(string12$))+mkl$(VARPTR(string13$))+mkl$(VARPTR(string14$))+mki$(3)+mki$(0)+mki$(0)+mki$(4513)+mki$(0)+mki$(1)+mki$(39)+mki$(39)
  obj6$=mki$(7)+mki$(-1)+mki$(-1)+mki$(29)+mki$(8)+mki$(0)+mkl$(VARPTR(tedinfo4$))+mki$(8)+mki$(64)+mki$(240)+mki$(16)
  string15$="ON"+CHR$(0)
  obj8$=mki$(9)+mki$(-1)+mki$(-1)+mki$(26)+mki$(17)+mki$(1)+mkl$(VARPTR(string15$))+mki$(16)+mki$(16)+mki$(32)+mki$(16)
  string16$="OFF"+CHR$(0)
  obj9$=mki$(7)+mki$(-1)+mki$(-1)+mki$(26)+mki$(17)+mki$(0)+mkl$(VARPTR(string16$))+mki$(64)+mki$(16)+mki$(32)+mki$(16)
  obj7$=mki$(2)+mki$(8)+mki$(9)+mki$(20)+mki$(0)+mki$(0)+mkl$(16716058)+mki$(16)+mki$(96)+mki$(400)+mki$(48)
  obj2$=mki$(10)+mki$(3)+mki$(7)+mki$(20)+mki$(0)+mki$(0)+mkl$(16716032)+mki$(16)+mki$(48)+mki$(480)+mki$(160)
  ok=10
  string17$="OK"+CHR$(0)
  obj10$=mki$(11)+mki$(-1)+mki$(-1)+mki$(26)+mki$(7)+mki$(0)+mkl$(VARPTR(string17$))+mki$(520)+mki$(64)+mki$(56)+mki$(64)
  cancel=11
  string18$="CANCEL"+CHR$(0)
  obj11$=mki$(0)+mki$(-1)+mki$(-1)+mki$(26)+mki$(37)+mki$(0)+mkl$(VARPTR(string18$))+mki$(520)+mki$(144)+mki$(56)+mki$(64)
  obj0$=mki$(-1)+mki$(1)+mki$(11)+mki$(20)+mki$(0)+mki$(16)+mkl$(135424)+mki$(0)+mki$(0)+mki$(592)+mki$(224)
  tree0$=obj0$+obj1$+obj2$+obj3$+obj4$+obj5$+obj6$+obj7$+obj8$+obj9$+obj10$
  tree0$=tree0$+obj11$
  ~FORM_DIAL(0,0,0,0,0,bx,by,bw,bh)
  ~FORM_DIAL(1,0,0,0,0,bx,by,bw,bh)
  ~OBJC_DRAW(VARPTR(tree0$),0,-1,0,0)
  ret=FORM_DO(VARPTR(tree0$))
  IF ret=ok
    PRINT "You selected OK and your input was:"
    PRINT string3$
    PRINT string6$
    PRINT string9$
    PRINT string12$
  ENDIF
  ~FORM_DIAL(2,0,0,0,0,bx,by,bw,bh)
  ~FORM_DIAL(3,0,0,0,0,bx,by,bw,bh)
  SHOWPAGE
RETURN

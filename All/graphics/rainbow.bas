' Macht einen Balken mit Regenbogenfarben
' (c) Markus Hoffmann
'

for x=0 to 511
  for y=0 to 3
    x2=x/2
    phase=int(x2/51)
    level=x2 mod 51
    if phase=0
      r=255
      g=level*5
      b=0
    else if phase=1
      r=255-level*5
      g=255
      b=0
    else if phase=2
      r=0
      g=255
      b=level*5
    else if phase=3
     r=0
     g=255-level*5
     b=255
    else if phase=4
      r=level*5
      g=0
      b=255
    else
      r=255
      g=0
      b=255
    endif
    color get_color(r*256,g*256,b*256)
    plot x,y
  next y
  showpage
next x
pause 4
get 0,0,512,4,t$
t$=PNGENCODE$(t$)
bsave "rainbow.png",varptr(t$),len(t$)
quit

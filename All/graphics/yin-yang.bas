' Yin & Yang from Rosetta-Code. 
' This is a new approach in X11-Basic
' Markus Hoffmann 2019
'
CLEARW
SHOWPAGE
@drawit(100,100,100)
@drawit(300,150,80)
@drawit(500,200,60)
pause 1
end
PROCEDURE drawit(bx,by,r0)
  LOCAL r,x,y,p
  COLOR COLOR_RGB(0,0,0)
  PCIRCLE bx,by,r0
  COLOR COLOR_RGB(1,1,1)
  PCIRCLE bx,by+r0/2,r0/2
  COLOR COLOR_RGB(0,0,0)
  PCIRCLE bx,by+r0/2,r0/5
  FOR x=0 TO r0
    FOR y=0 TO 2*r0
      r=SQRT((x-r0)*(x-r0)+(y-r0)*(y-r0))
      IF r<r0
        p=ATAN2(y-r0,x-r0)
        COLOR POINT(bx+COS(p)*r,by+SIN(p)*r) XOR -1
        PLOT bx+r0-x,by+r0-y
      ENDIF
    NEXT y
    SHOWPAGE
  NEXT x
  COLOR COLOR_RGB(0,0,0)
  PCIRCLE bx,by+r0/2,r0/5
  COLOR COLOR_RGB(1,1,1)
  PCIRCLE bx,by-r0/2,r0/5
  CIRCLE bx,by,r0
  SHOWPAGE
RETURN

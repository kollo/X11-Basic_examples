' gets the high precision time from a Garmin GPS-Receiver
' e.g. the Garmin Etrex         (c) Markus Hoffmann 2004
'
' sets the systems (hardware) clock. Must be run as root
'
devicename$="/dev/ttyS1"
@loadsettings(env$("HOME")+"/.garminrc")
PRINT "Open device ",devicename$
OPEN "UX:9600,N,8,1",#1,devicename$,0

@init_device

fertig=0
totzeit=timer
@garmintime
DO
  PAUSE 0.001
  IF TIMER-totzeit>5
    @sendmessage(10,CHR$(5)+CHR$(0))
    totzeit=TIMER
    fertig=1
  ENDIF
  IF inp?(#1)
    @status(2,"READ")
    WHILE inp?(#1) AND LEN(INKEY$)=0
      @procmessage(@getmessage$())
    WEND
    @status(2,"OK")
    totzeit=TIMER
  ENDIF
  EXIT if fertig=1
LOOP
@ende

PROCEDURE init_device
  ' Garmin identifizieren
  @sendmessage(254," ")
  @procmessage(@getmessage$())
RETURN
PROCEDURE garminpos
  @sendmessage(10,mki$(2))
RETURN
PROCEDURE garmintime
  @sendmessage(10,mki$(5))
RETURN
PROCEDURE ende
  ' Quit the program, close all open files remove all installations
  CLOSE
  QUIT
RETURN
FUNCTION getmessage$()
  LOCAL s$,flag,flag2,chk,chk2
  t=TIMER
  CLR s$,flag,flag2
  DO
    IF inp?(#1)
      t$=CHR$(INP(#1))
      ' PRINT "Got:";ASC(t$)
      IF t$=CHR$(16)
        IF flag2=0
          flag2=1
        ELSE
          IF flag=0
            flag=1
          ELSE
            s$=s$+t$
            flag=0
          ENDIF
        ENDIF
      ELSE
        IF flag2=0
          PRINT "protokollfehler: muell ";ASC(t$)
        ELSE
          IF flag=1 AND t$=CHR$(3)
            GOTO t
          ELSE IF flag=1
            PRINT "Protokoll-ERROR 16"
            flag=0
          ENDIF
          s$=s$+t$
        ENDIF
      ENDIF
    ELSE
      PAUSE 0.01
    ENDIF
    IF TIMER-t>2
      @status(1,"TIMEOUT")
      s$="TIMEOUT 0 ?"
      BEEP
      fertig=1
      GOTO t
    ENDIF
  LOOP
  t:
  pid=ASC(MID$(s$,1,1))
  chk=ASC(MID$(s$,LEN(s$),1)) and 255
  chk2=0
  FOR i=1 TO LEN(s$)-1
    chk2=chk2-ASC(MID$(s$,i,1))
  NEXT i
  chk2=chk2 AND 255
  IF chk=chk2 AND pid<>6 AND pid<>21
    @sendACK(pid)
  ELSE IF chk<>chk2
    @status(1,"Uebertragungsfehler CHK")
  ENDIF
  RETURN s$
ENDFUNCTION
PROCEDURE procmessage(t$)
  LOCAL pid,plen
  pid=ASC(LEFT$(t$,1))
  plen=ASC(MID$(t$,2,1))
  IF pid=6
    ' ACK, alles OK, ignorieren
    @status(1,"OK")
  ELSE IF pid=14
    @proctimedata(t$)
    fertig=1
  ELSE IF pid=21
    @status(1,"NAK !")
  ELSE IF pid=-2 OR pid=10
    PRINT "Paket nicht erlaubt ! Pid="; pid
  ELSE IF pid=-1
    @procproductdata(t$)
  ELSE
    @status(2,"? Paket pid="+STR$(pid)+" !")
    PRINT "? Paket pid="+STR$(pid)+" !  LEN=";plen
    t$=MID$(t$,3,plen)
    FOR i=0 TO LEN(t$)-1
      PRINT hex$(ASC(MID$(t$,i+1,1)) AND 255,2,2,1)'
    NEXT i
    PRINT
  ENDIF
RETURN
PROCEDURE procproductdata(ut$)
  garminPID=CVI(MID$(ut$,3,2))
  garminVER=CVI(MID$(ut$,5,2))
  garminMES$=MID$(t$,7,LEN(ut$)-7-1)
  garminmes$=REPLACE$(garminmes$,CHR$(0)," ")
  PRINT "Connected: "
  PRINT "PID=",garminPID
  PRINT "VER=",garminVER
  PRINT "MES=",garminMES$
RETURN
PROCEDURE procpositiondata(pt$)
  posy=CVD(MID$(pt$,3,8))*180/pi
  posx=CVD(MID$(pt$,11,8))*180/pi
  PRINT @breite$(posy)
  PRINT @laenge$(posx)
RETURN
PROCEDURE proctimedata(t$)
  LOCAL month,day,year,hour,minute,second
  month=ASC(MID$(t$,3,1))
  day=ASC(MID$(t$,4,1))
  year=CVI(MID$(t$,5,2))
  hour=CVI(MID$(t$,7,2))
  minute=ASC(MID$(t$,9,1))
  second=ASC(MID$(t$,10,1))
  ndate$=STR$(day,2,2)+"."+STR$(month,2,2,1)+"."+STR$(year,4,4)
  ntime$=STR$(hour,2,2)+":"+STR$(minute,2,2,1)+":"+STR$(second,2,2,1)
  PRINT "Got Time Information:"
  PRINT "DATE=",ndate$
  PRINT "TIME=",ntime$," UTC"
  '  system "date -u +"+str$(month,2,2,1)+str$(day,2,2,1)+str$(hour,2,2,1)+str$(minute,2,2,1)+str$(year,4,4,1)+"."+str$(second,2,2,1)
  SYSTEM "date -u --set="+CHR$(34)+STR$(year,4,4,1)+STR$(month,2,2,1)+STR$(day,2,2,1)+" "+STR$(hour,2,2,1)+":"+STR$(minute,2,2,1)+":"+STR$(second,2,2,1)+CHR$(34)
  SYSTEM "hwclock --systohc"
RETURN
PROCEDURE sendACK(pid)
  @sendmessage(6,CHR$(pid))
RETURN

PROCEDURE sendmessage(id,m$)
  LOCAL i,a,chk,s$

  chk=0-id-LEN(m$)
  s$=CHR$(16)+CHR$(id)+CHR$(LEN(m$))
  IF LEN(m$)=16
    s$=s$+CHR$(16)
  ENDIF
  IF LEN(m$)
    FOR i=1 TO LEN(m$)
      a=ASC(MID$(m$,i,1)) and 255
      s$=s$+CHR$(a)
      IF a=16
        s$=s$+CHR$(16)
      ENDIF
      chk=chk-a
    NEXT i
  ENDIF
  chk=chk AND 255
  s$=s$+CHR$(chk)
  IF chk=16
    s$=s$+CHR$(16)
  ENDIF
  s$=s$+CHR$(16)+CHR$(3)
  BPUT #1,VARPTR(s$),LEN(s$)
  FLUSH #1
RETURN

PROCEDURE sendnack
  @sendmessage(21,"")
RETURN
PROCEDURE status(n,ssst$)
  PRINT "STATUS=";n;"  ";ssst$
RETURN

' Get the devicename from GPS-Earth-conf file

PROCEDURE loadsettings(f$)
  LOCAL t$,a$,b$
  IF exist(f$)
    OPEN "I",#9,f$
    WHILE not eof(#9)
      LINEINPUT #9,t$
      t$=TRIM$(t$)
      IF left$(t$)<>"#"
        SPLIT t$,"=",1,a$,b$
        IF upper$(a$)="DEVICE"
          devicename$=b$
        ENDIF
      ENDIF
    WEND
    CLOSE #9
  ENDIF
RETURN

' Testet die eingebaute Fast-Fourier-Transformation
' (geht nur, wenn man numerical recepies besitzt.)
'
ECHO off
l=2^10
c1=4
c2=86
korrnr=VAL(PARAM$(2))
CLR ghgh
DIM a(l),b(l),diff(l),c(l),d(l),s(l)
SIZEW ,MIN(l,1224),400
OPEN "I",#1,"/sgt/elsa/matlab/messungen/mondata_rampe_neu.dat"
LINEINPUT #1,t$
FOR j=0 TO 2+korrnr*2
  WORT_SEP t$," ",1,a$,t$
NEXT j
a(0)=-val(a$)/4096*2
WORT_SEP t$," ",1,a$,t$
s(0)=-val(a$)/4096*2
FOR i=0 TO l/2
  IF i>50
    LINEINPUT #1,t$
    FOR j=0 TO 2+korrnr*2
      WORT_SEP t$," ",1,a$,t$
    NEXT j
    a(i)=-val(a$)/4096*2
    WORT_SEP t$," ",1,a$,t$
    s(i)=-val(a$)/4096*2

  ELSE
    a(i)=a(0)
    s(i)=s(0)
  ENDIF
  a(l-i)=a(i)
  s(l-i)=s(i)
  b(l-i)=a(l-i)
  c(l-i)=a(l-i)
  b(i)=a(i)
  c(i)=a(i)
NEXT i
CLOSE
@uebertragungsf

DO
  COLOR get_color(10000,10000,10000)
  PBOX 0,0,MIN(l,1224),400
  COLOR get_color(65535,32000,0)
  SCOPE a(),1,-300,300
  COLOR get_color(0,65535,32000)
  SCOPE b(),0,-300,300
  COLOR get_color(65535,0,0)
  SCOPE s(),0,-300,300
  FFT a()
  COLOR get_color(65535,65535,65535)
  FOR i=0 TO l STEP 2
    LINE i/2,300,i/2,300-1000/l*sqrt(a(i)^2+a(i+1)^2)
  NEXT i

  LINE c1/2,0,c1/2,10
  LINE c2/2,0,c2/2,10
  FOR i=0 TO l-1 STEP 2
    d(0)=a(0)
    c1=a(i+1)
    c2=a(i+2)
    u1=uea(INT(i/l*uel/2)*2+1)
    u2=uea(INT(i/l*uel/2)*2+2)

    IF i<l/16
      d(i+1)=c1/sqrt(u1^2+u2^2)
      d(i+2)=c2/sqrt(u1^2+u2^2)
    ELSE
      d(i+1)=0
      d(i+2)=0
    ENDIF
    IF i<l
      a(i+1)=(c1*u1-c2*u2)
      a(i+2)=(c1*u2+c2*u1)
      ' a(i+1)=c1*sqrt(u1^2+u2^2)
      ' a(i+2)=c2*sqrt(u1^2+u2^2)
    ELSE
      a(i+1)=0
      a(i+2)=0
    ENDIF
  NEXT i
  COLOR get_color(65535,0,0)
  FOR i=0 TO l STEP 2
    LINE i/2,300,i/2,300+1000/l*sqrt(a(i)^2+a(i+1)^2)
  NEXT i

  FFT a(),-1
  FFT d(),-1

  COLOR get_color(0,32535,65535)
  SCOPE a(),0,-600/l,300
  FOR i=0 TO l
    diff(i)=-(a(i)/l*2-b(i))
  NEXT i

  ' open "O",#1,"testme"
  ' for i=0 to l
  ' f%=a(i)*20000
  ' out #1,f%
  ' next i
  ' close #1

  FOR g=1 TO 10
    COLOR get_color(65535,65535,0)
    SCOPE a(),b(),1,2/l*g,200,-1*g,200
    VSYNC
    COLOR get_color(0,0,0)
    SCOPE a(),b(),1,2/l*g,200,-1*g,200
  NEXT g

  FOR i=0 TO l
    ' a(i)=a(i)/l*2+diff(i)
    a(i)=d(i)/l*2
  NEXT i
  INC ghgh
  ALERT 0,"1",1," OK ",d
  EXIT if ghgh=2
LOOP
ALERT 0,"Fertig",1," OK ",d
QUIT

FUNCTION si(x)
  RETURN x MOD pi
ENDFUNC

PROCEDURE uebertragungsf
  LOCAL i,a$,t$
  USEWINDOW 2
  uel=2^10
  DIM uea(uel)
  OPEN "I",#1,"/sgt/elsa/matlab/messungen/mondata_puls_0.0A_neu.dat"
  FOR i=0 TO 900
    LINEINPUT #1,t$
  NEXT i
  FOR i=0 TO 500
    LINEINPUT #1,t$
    FOR j=0 TO 3+korrnr*2
      WORT_SEP t$," ",1,a$,t$
    NEXT j
    uea(i)=(VAL(a$))/800
  NEXT i
  FOR i=500 TO uel
    uea(i)=uea(499)
  NEXT i
  CLOSE #1
  SIZEW ,MIN(uel,1224),400

  COLOR get_color(10000,10000,10000)
  PBOX 0,0,MIN(uel,1224),400

  COLOR get_color(65535,32000,0)
  SCOPE uea(),0,-2000,300
  VSYNC
  FFT uea()
  COLOR get_color(65535,65535,65535)
  FOR i=0 TO uel STEP 2
    uea(i)=uea(i)/sqrt(uea(1)^2+uea(2)^2)
    uea(i+1)=uea(i+1)/sqrt(uea(1)^2+uea(2)^2)
    DRAW to i,300,i,300-300*(0.9+0.15*log(sqrt(uea(i+2)^2+uea(i+1)^2)))
  NEXT i
  COLOR get_color(65535,65535,0)
  basephas=atan2(uea(2),uea(1))
  PRINT "basephase=";basephas
  FOR i=0 TO uel-1 STEP 2
    pp=atan2(uea(i+2),uea(i+1))
    CIRCLE i,300-30*pp,2
    uea(i+2)=cos(-basephas+pp)
    uea(i+1)=sin(-basephas+pp)
  NEXT i
  VSYNC
  USEWINDOW 1
RETURN


' Kuehlanlagen einschalten

ECHO off
SIZEW 1,500,200
COLOR get_color(0,65535,0)
PBOX 0,0,500,200
m$="Kuehlanlagen einschalten:|=========================||"
PAUSE 1
ALERT 2,m$+"Fuer welchen Betriebsmodus soll|eingeschaltet werden ?",4,"PT-Betrieb|Syli|GDH/CB|ABBRUCH",balert
IF balert=4
  QUIT
ENDIF
COLOR get_color(65535,65535,65535)
SETFONT "*-34-*"
TEXT 10,100,"bitte warten..."
VSYNC

' Auf jeden Fall RKW4 einschalten

@pumpenfrage(4,2,"SUP_WASSER_DROSTURM4.PUMPE2_DC","SUP_WASSER_DROSTURM4.STATUS_DM")
@pumpenfrage(4,1,"SUP_WASSER_DROSSEL.PUMPE1_DC","SUP_WASSER_DROSSEL.STATUS_DM")

IF balert=2 OR balert=3
  ' RKW3 einschalten

  @pumpenfrage(3,7,"SUP_WASSER_ELSATURM3.PUMPE7_DC","SUP_WASSER_ELSATURM3.STATUS_DM")
  @pumpenfrage(3,8,"SUP_WASSER_ELSAMAG.PUMPE8_DC","SUP_WASSER_ELSAMAG.STATUS_DM")
  @pumpenfrage(3,5,"SUP_WASSER_ELSAHF.PUMPE5_DC","SUP_WASSER_ELSAHF.STATUS_DM")
ENDIF

IF balert=3
  ' RKW2 einschalten

  CSPUT "SUP_WASSER_EXPTURM2.PUMPE9_DC",1
  t=timer
  a=csget("SUP_WASSER_EXPTURM2.PUMPE9A_DM")
  b=csget("SUP_WASSER_EXPTURM2.PUMPE9B_DM")
  WHILE a=0 AND b=0
    PAUSE 1
    IF timer-t>30
      VOID form_alert(1,"[3][RKW2 Pumpe 9|will nicht angehen.][ STOP ]")
      QUIT
    ENDIF
    a=csget("SUP_WASSER_EXPTURM2.PUMPE9A_DM")
    b=csget("SUP_WASSER_EXPTURM2.PUMPE9B_DM")
  WEND

  CSPUT "SUP_WASSER_EXPMD.PUMPE6_DC",1
  t=timer
  a=csget("SUP_WASSER_EXPMD.PUMPE6A_DM")
  b=csget("SUP_WASSER_EXPMD.PUMPE6B_DM")
  WHILE a=0 AND b=0
    PAUSE 1
    IF timer-t>30
      VOID form_alert(1,"[3][RKW2 Pumpe 6|will nicht angehen.][ OH ]")
    ENDIF
    a=csget("SUP_WASSER_EXPTURM2.PUMPE9A_DM")
    b=csget("SUP_WASSER_EXPTURM2.PUMPE9B_DM")
  WEND
ENDIF
QUIT

PROCEDURE pumpenfrage(rkw,n,e1$,e3$)
  ' Testen, ob die Pumpe lauft und ggf. einschalten
  LOCAL a,b,t

  a=csget(e1$)

  IF a=0
    ' if form_alert(1,"[3][Die Pumpe "+str$(n)+" ist nicht an.|Soll sie eingeschaltet werden ?][Ja|Nein]")=1
    CSPUT e1$,1
    t=timer
    a=csget(e2$)
    b=csget(e3$)
    WHILE a=0 AND b=0
      PAUSE 1
      IF timer-t>30
        IF form_alert(1,"[3][RKW"+STR$(rkw)+" Pumpe "+STR$(n)+"|will nicht angehen.][ STOP |WEITER]")=1
          QUIT
        ENDIF
      ENDIF
      a=csget(e1$)
    WEND
    ' endif
  ELSE
    PRINT "RKW"+STR$(rkw)+" Pumpe "+STR$(n)+" ist an."
  ENDIF
RETURN

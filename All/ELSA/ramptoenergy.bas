' Rampt auf eine vorgegebene Energie, in der Hoffnung,
' dass der Strahl mitgenommen wird
'
PRINT "Ramptoenergy brute force"
PRINT

e1=csget("ELS_MAGNETE_DIPOL.ENERGIE_AC")
PRINT "Aktuelle Energie ist ";e1;" GeV."
PRINT "Wohin soll die Reise gehen ? (GeV) ";
INPUT e2

PRINT "HF-Leistung muss stimmen !"
PRINT "Korrektoren- muessen stimmen ! fuer grosse Rampen."

' c=csstep("ELS_MAGNETE_DIPOL.ENERGIE_AC")
IF e2>e1
  c=0.001
ELSE
  c=-0.001
ENDIF
PRINT "Step ";c*1000;" MeV"

CLR count

FOR i=e1 TO e2 STEP c
  CSPUT "ELS_MAGNETE_DIPOL.ENERGIE_AC",i
  PAUSE 0.2
  INC count
  IF (count MOD 10)=0
    PRINT "E=";i;" GeV"
  ENDIF
NEXT i
CSPUT "ELS_MAGNETE_DIPOL.ENERGIE_AC",e2
PRINT "fertig."
QUIT

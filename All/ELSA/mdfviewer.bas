' Liest und interpretiert *.mdf Dateien.

' (c) Markus Hoffmann Mai 2006

DIM m$(10000)
anzm=0

ccsdir$="/media/MH_A3/sgt/ccs/"

i=1
WHILE len(PARAM$(i))
  IF param$(i)="-b"
    INC i
    breite=@value(PARAM$(i))
  ELSE if PARAM$(i)="-o"
    INC i
    offset=@value(PARAM$(i))
  ELSE if PARAM$(i)="-cd0"
    INC i
    cd0dir$=PARAM$(i)
  ELSE if PARAM$(i)="-all"
    doevents=true
    doefs=true
  ELSE if PARAM$(i)="-events"
    doevents=true
  ELSE if PARAM$(i)="-efs"
    doefs=true
  ELSE if PARAM$(i)="--noreplace"
    doreplace=false
  ELSE if PARAM$(i)="--nodetails"
    donodetails=true
  ELSE if PARAM$(i)="--new"
    donew=true
  ELSE
    filename$=PARAM$(i)
    IF right$(filename$,4)=".mdf"
      @load_file(filename$)
      @doit
    ENDIF
  ENDIF
  INC i
WEND
QUIT

PROCEDURE load_file(f$)
  CLR anzm
  IF exist(f$)
    OPEN "I",#1,f$
    WHILE not eof(#1)
      LINEINPUT #1,t$
      m$(anzm)=t$
      INC anzm
    WEND
    CLOSE #1
    filename$=f$
    PRINT "loaded "+f$+" ("+STR$(anzm)+" elements.)"
  ENDIF
RETURN
PROCEDURE doit
  LOCAL i
  weiss=GET_COLOR(65535,65535,65535)
  grau=GET_COLOR(65535/2,65535/2,65535/2)
  schwarz=GET_COLOR(0,0,0)
  gelb=GET_COLOR(65535,65535,0)
  hellblau=GET_COLOR(0,65535,65535)
  orange=GET_COLOR(65535,65535/3,65535/3)
  FOR i=0 TO anzm-1
    t$=m$(i)
    IF left$(t$)="M"
      SPLIT t$,":",0,a$,t$
      IF a$="MenuValidation"
        PRINT "Validation=";@getval$(t$,"VALID")
      ELSE if a$="MenuVersion"
        PRINT "Version: ";@getval$(t$,"VERSION")
        titel$=@getval$(t$,"ORIGFNAME")
        PRINT "Original Name: ",titel$
        TITLEW 1,titel$
      ELSE if a$="MenuProperty"
        bx=VAL(@getval$(t$,"XP"))
        by=VAL(@getval$(t$,"YP"))
        bw=VAL(@getval$(t$,"WIDTH"))
        bh=VAL(@getval$(t$,"HEIGHT"))
        SIZEW 1,bw,bh
        bgc$=@getval$(t$,"WBGC")
        @setcolor(bgc$)
        PBOX bx,by,bw,bh
      ELSE if a$="MenuParmString"
        x=VAL(@getval$(t$,"XP"))
        y=VAL(@getval$(t$,"YP"))
        fgc$=@getval$(t$,"FGC")
        font$=@getval$(t$,"FONT")
        text$=@getval$(t$,"PARM")
        text$=LEFT$(text$,LEN(text$)-1)
        text$=RIGHT$(text$,LEN(text$)-1)
        @setcolor(fgc$)
        SETFONT font$
        TEXT x+2,y,text$
      ELSE if a$="MenuString"
        x=VAL(@getval$(t$,"XP"))
        y=VAL(@getval$(t$,"YP"))
        fgc$=@getval$(t$,"FGC")
        font$=@getval$(t$,"FONT")
        text$=@getval$(t$,"TEXT")
        text$=LEFT$(text$,LEN(text$)-1)
        text$=RIGHT$(text$,LEN(text$)-1)
        @draw_string(x,y,0,0,x,y,0,0,@convert_color(fgc$),0,font$,text$)
      ELSE if a$="MenuStringBox"
        x=VAL(@getval$(t$,"XP"))
        y=VAL(@getval$(t$,"YP"))
        w=VAL(@getval$(t$,"WIDTH"))
        h=VAL(@getval$(t$,"HEIGHT"))
        bgc$=@getval$(t$,"BGC")
        fgc$=@getval$(t$,"FGC")
        font$=@getval$(t$,"FONT")
        text$=@getval$(t$,"TEXT")
        text$=LEFT$(text$,LEN(text$)-1)
        text$=RIGHT$(text$,LEN(text$)-1)
        @draw_string(x,y-h,w,h+2,x,y,w,h,@convert_color(fgc$),@convert_color(bgc$),font$,text$)
      ELSE if a$="MenuParmLabel"
        x=VAL(@getval$(t$,"XP"))
        y=VAL(@getval$(t$,"YP"))
        font$=@getval$(t$,"FONT")
        INC i
        o$=m$(i)
        o$="TEXT="+TRIM$(o$)
        fgc$=@getval$(o$,"FGC")
        bgc$=@getval$(o$,"BGC")
        text$=@getval$(o$,"TEXT")
        text$=LEFT$(text$,LEN(text$)-1)
        text$=RIGHT$(text$,LEN(text$)-1)
        @draw_string(x,y,0,0,x,y,0,0,@convert_color(fgc$),@convert_color(bgc$),font$,text$)
      ELSE if a$="MenuParmLabelBox"
        x=VAL(@getval$(t$,"XP"))
        y=VAL(@getval$(t$,"YP"))
        w=VAL(@getval$(t$,"WIDTH"))
        h=VAL(@getval$(t$,"HEIGHT"))
        font$=@getval$(t$,"FONT")
        INC i
        o$=m$(i)
        o$="TEXT="+TRIM$(o$)
        fgc$=@getval$(o$,"FGC")
        bgc$=@getval$(o$,"BGC")
        text$=@getval$(o$,"TEXT")
        text$=LEFT$(text$,LEN(text$)-1)
        text$=RIGHT$(text$,LEN(text$)-1)
        @draw_string(x,y-h,w,h+2,x,y,w,h,@convert_color(fgc$),@convert_color(bgc$),font$,text$)
      ELSE if glob(a$,"MenuParm*Number") OR a$="MenuParmDim"
        x=VAL(@getval$(t$,"XP"))
        y=VAL(@getval$(t$,"YP"))
        fgc$=@getval$(t$,"FGC")
        font$=@getval$(t$,"FONT")
        IF glob(a$,"*Long*")
          text$="????????"
        ELSE if glob(a$,"*Prec*")
          text$="?.???????"
        ELSE if glob(a$,"*Dim")
          text$="dim"
        ELSE if glob(a$,"*Udef*")
          text$=@getval$(t$,"FORMAT")
        ELSE
          text$="?.???"
        ENDIF
        IF a$="MenuParmInNumber"
          w=VAL(@getval$(t$,"WIDTH"))
          h=VAL(@getval$(t$,"HEIGHT"))
          DEFLINE 1,1
          COLOR hellblau
          BOX x,y,x+w,y-h
          DEFLINE 0
        ELSE
          @draw_string(x,y,0,0,x,y,0,0,@convert_color(fgc$),0,font$,text$)
        ENDIF
      ELSE if a$="MenuLine"
        x=VAL(@getval$(t$,"XP"))
        y=VAL(@getval$(t$,"YP"))
        x2=VAL(@getval$(t$,"X2"))
        y2=VAL(@getval$(t$,"Y2"))
        lw=VAL(@getval$(t$,"LINEWIDTH"))
        fgc$=@getval$(t$,"FGC")
        @setcolor(fgc$)
        DEFLINE ,lw
        LINE x,y,x2,y2
      ELSE if a$="MenuArc"
        x=VAL(@getval$(t$,"XP"))
        y=VAL(@getval$(t$,"YP"))
        w=VAL(@getval$(t$,"WIDTH"))
        h=VAL(@getval$(t$,"HEIGHT"))
        a1=VAL(@getval$(t$,"ANGLE1"))
        a2=VAL(@getval$(t$,"ANGLE2"))
        lw=VAL(@getval$(t$,"LINEWIDTH"))
        fgc$=@getval$(t$,"FGC")
        @setcolor(fgc$)
        DEFLINE ,lw
        ELLIPSE x+w/2,y-h/2,w/2,h/2,a1,a2
      ELSE if a$="MenuArcFilled"
        x=VAL(@getval$(t$,"XP"))
        y=VAL(@getval$(t$,"YP"))
        w=VAL(@getval$(t$,"WIDTH"))
        h=VAL(@getval$(t$,"HEIGHT"))
        a1=VAL(@getval$(t$,"ANGLE1"))
        a2=VAL(@getval$(t$,"ANGLE2"))
        lw=VAL(@getval$(t$,"LINEWIDTH"))
        fgc$=@getval$(t$,"FGC")
        bgc$=@getval$(t$,"BGC")
        DEFLINE ,0
        @setcolor(bgc$)
        PELLIPSE x+w/2,y-h/2,w/2,h/2,a1,a2
        @setcolor(fgc$)
        DEFLINE ,lw
        ELLIPSE x+w/2,y-h/2,w/2,h/2,a1,a2
      ELSE if glob(a$,"MenuParm*Scale")
        x=VAL(@getval$(t$,"XP"))
        y=VAL(@getval$(t$,"YP"))
        w=VAL(@getval$(t$,"WIDTH"))
        h=VAL(@getval$(t$,"HEIGHT"))
        lay=VAL(@getval$(t$,"LAYOUT"))
        fgc$=@getval$(t$,"FGC")
        bgc$=@getval$(t$,"BGC")
        position=0
        DEFLINE ,0
        @setcolor(bgc$)
        PBOX x,y,x+w,y-h
        @preclear_scale()
        @setcolor(fgc$)
        @prefill_scale()
        IF position>0
          @draw_scale
        ELSE
          @draw_ramp_logo(x,y,w,h)
        ENDIF
      ELSE if a$="MenuParmVBar"
        x=VAL(@getval$(t$,"XP"))
        y=VAL(@getval$(t$,"YP"))
        w=VAL(@getval$(t$,"WIDTH"))
        h=VAL(@getval$(t$,"HEIGHT"))
        fgc$=@getval$(t$,"FGC")
        bgc$=@getval$(t$,"BGC")
        @setcolor(bgc$)
        PBOX x,y,x+w,y-h
        @setcolor(fgc$)
        PBOX x,y,x+w,y-h/2
      ELSE if a$="MenuParmHBar"
        x=VAL(@getval$(t$,"XP"))
        y=VAL(@getval$(t$,"YP"))
        w=VAL(@getval$(t$,"WIDTH"))
        h=VAL(@getval$(t$,"HEIGHT"))
        fgc$=@getval$(t$,"FGC")
        bgc$=@getval$(t$,"BGC")
        @setcolor(bgc$)
        PBOX x,y,x+w,y-h
        @setcolor(fgc$)
        PBOX x,y,x+w/2,y-h
      ELSE if a$="MenuRectangleFilled"
        x=VAL(@getval$(t$,"XP"))
        y=VAL(@getval$(t$,"YP"))
        w=VAL(@getval$(t$,"WIDTH"))
        h=VAL(@getval$(t$,"HEIGHT"))
        lw=VAL(@getval$(t$,"LINEWIDTH"))
        fgc$=@getval$(t$,"FGC")
        bgc$=@getval$(t$,"BGC")
        DEFLINE ,0
        @setcolor(bgc$)
        PBOX x,y,x+w,y-h
        @setcolor(fgc$)
        DEFLINE ,lw
        BOX x,y,x+w,y-h
      ELSE if a$="MenuRectangle"
        x=VAL(@getval$(t$,"XP"))
        y=VAL(@getval$(t$,"YP"))
        w=VAL(@getval$(t$,"WIDTH"))
        h=VAL(@getval$(t$,"HEIGHT"))
        lw=VAL(@getval$(t$,"LINEWIDTH"))
        fgc$=@getval$(t$,"FGC")
        @setcolor(fgc$)
        DEFLINE ,lw
        BOX x,y,x+w,y-h
      ELSE if a$="MenuMenu"
        x=VAL(@getval$(t$,"XP"))
        y=VAL(@getval$(t$,"YP"))
        w=VAL(@getval$(t$,"WIDTH"))
        h=VAL(@getval$(t$,"HEIGHT"))
        DEFLINE 1
        COLOR weiss
        BOX x,y,x+w,y-h
        DEFLINE 0
      ELSE if a$="MenuMenuFrame"
        x=VAL(@getval$(t$,"XP"))
        y=VAL(@getval$(t$,"YP"))
        w=VAL(@getval$(t$,"WIDTH"))
        h=VAL(@getval$(t$,"HEIGHT"))
        DEFLINE 1
        COLOR weiss
        BOX x,y,x+w,y-h
        DEFLINE 0
        @draw_frame(x,y,w,h,1)
      ELSE if a$="MenuApplic"
        x=VAL(@getval$(t$,"XP"))
        y=VAL(@getval$(t$,"YP"))
        w=VAL(@getval$(t$,"WIDTH"))
        h=VAL(@getval$(t$,"HEIGHT"))
        DEFLINE 1
        COLOR gelb
        BOX x,y,x+w,y-h
        DEFLINE 0
      ELSE if a$="MenuApplicFrame"
        x=VAL(@getval$(t$,"XP"))
        y=VAL(@getval$(t$,"YP"))
        w=VAL(@getval$(t$,"WIDTH"))
        h=VAL(@getval$(t$,"HEIGHT"))
        @draw_frame(x,y,w,h,1)
        DEFLINE 1
        COLOR gelb
        BOX x,y,x+w,y-h
        DEFLINE 0
      ELSE if a$="MenuParmInValue" OR a$="MenuParmTickDecr" OR a$="MenuParmTickIncr"
        x=VAL(@getval$(t$,"XP"))
        y=VAL(@getval$(t$,"YP"))
        w=VAL(@getval$(t$,"WIDTH"))
        h=VAL(@getval$(t$,"HEIGHT"))
        DEFLINE 1
        COLOR orange
        BOX x,y,x+w,y-h
        DEFLINE 0
      ELSE if a$="MenuParmInValueFrame"
        x=VAL(@getval$(t$,"XP"))
        y=VAL(@getval$(t$,"YP"))
        w=VAL(@getval$(t$,"WIDTH"))
        h=VAL(@getval$(t$,"HEIGHT"))
        @draw_frame(x,y,w,h,1)
        DEFLINE 1
        COLOR orange
        BOX x,y,x+w,y-h
        DEFLINE 0
      ELSE if a$="MenuParmMeter"
        x=VAL(@getval$(t$,"XP"))
        y=VAL(@getval$(t$,"YP"))
        w=VAL(@getval$(t$,"WIDTH"))
        h=VAL(@getval$(t$,"HEIGHT"))
        lw=VAL(@getval$(t$,"LINEWIDTH"))
        a1=VAL(@getval$(t$,"ANGLE1"))
        a2=VAL(@getval$(t$,"ANGLE2"))
        DEFLINE ,0
        COLOR @convert_color(bgc$)
        PELLIPSE x+w/2,y-h/2,w/2,h/2,-a1/64,-a2/64
        DEFLINE ,lw
        COLOR @convert_color(fgc$)
        ELLIPSE x+w/2,y-h/2,w/2,h/2,-a1/64,-a2/64
        PCIRCLE x+w/2-2,y-h/2-2,4
        DEFLINE ,2
        @draw_meter(x,y,w,h,30/64,a1,a2)
      ELSE if a$="MenuFrame"
        x=VAL(@getval$(t$,"XP"))
        y=VAL(@getval$(t$,"YP"))
        w=VAL(@getval$(t$,"WIDTH"))
        h=VAL(@getval$(t$,"HEIGHT"))
        r=VAL(@getval$(t$,"REVERT"))
        @draw_frame(x,y,w,h,r)
      ELSE if a$="MenuParmFrame"
        x=VAL(@getval$(t$,"XP"))
        y=VAL(@getval$(t$,"YP"))
        w=VAL(@getval$(t$,"WIDTH"))
        h=VAL(@getval$(t$,"HEIGHT"))
        r=VAL(@getval$(t$,"MASK"))
        @draw_frame(x,y,w,h,r)
      ELSE if a$="MenuBitmap"
        x=VAL(@getval$(t$,"XP"))
        y=VAL(@getval$(t$,"YP"))
        w=VAL(@getval$(t$,"WIDTH")) and 0xffff
        h=VAL(@getval$(t$,"HEIGHT")) and 0xffff
        fgc$=@getval$(t$,"FGC")
        bgc$=@getval$(t$,"BGC")
        bm$=@getval$(t$,"BITMAP")
        f$=ccsdir$+"men/bm/"+bm$
        IF exist(f$)
          @setcolor(fgc$)
          gg$=@load_bitmap$(f$)
          PRINT f$,x,y,w,h
          @putbitmap(gg$,x,y-h)
        ENDIF
      ELSE if a$="MenuParmBitmapLabel"
        x=VAL(@getval$(t$,"XP"))
        y=VAL(@getval$(t$,"YP"))
        w=VAL(@getval$(t$,"WIDTH")) and 0xffff
        h=VAL(@getval$(t$,"HEIGHT")) and 0xffff
        INC i
        o$=m$(i)
        o$="BITMAP="+TRIM$(o$)
        fgc$=@getval$(o$,"FGC")
        bgc$=@getval$(o$,"BGC")
        bm$=@getval$(o$,"BITMAP")
        bm$=LEFT$(bm$,LEN(bm$)-1)
        bm$=RIGHT$(bm$,LEN(bm$)-1)

        f$=ccsdir$+"men/bm/"+bm$
        IF exist(f$)
          @setcolor(fgc$)
          gg$=@load_bitmap$(f$)
          PRINT f$,x,y,w,h
          @putbitmap(gg$,x,y-h)
        ELSE
          PRINT f$,"nicht gefunden.."
        ENDIF
      ELSE
        PRINT a$
        PRINT t$
      ENDIF
    ELSE if LEFT$(t$)="!"
    ELSE if LEFT$(t$)=" "
    ELSE
      PRINT "unknown statement: ";t$
    ENDIF
  NEXT i
  VSYNC
  DO
    MOUSEEVENT mx,my,mb
    PRINT mx,my,mb
    EXIT if mb>2
    ' find click area
    IF mb=1
      menu$=""
      FOR i=0 TO anzm-1
        t$=m$(i)
        IF left$(t$)="M"
          SPLIT t$,":",0,a$,t$
          IF glob(a$,"MenuMenu")
            x=VAL(@getval$(t$,"XP"))
            y=VAL(@getval$(t$,"YP"))
            w=VAL(@getval$(t$,"WIDTH"))
            h=VAL(@getval$(t$,"HEIGHT"))
            IF mx>x AND my>y-h AND mx<x+w AND my<y
              menu$=@getval$(t$,"MENU")
            ENDIF
          ENDIF
        ENDIF
      NEXT i
      IF len(menu$)
        lastloaded$=filename$
        @load_file(left$(filename$,rinstr(filename$,"/"))+menu$+".mdf")
        @doit
      ENDIF
    ELSE if mb=2
      menu$=lastloaded$
      lastloaded$=filename$
      @load_file(menu$)
      @doit
    ENDIF
  LOOP
RETURN
PROCEDURE putbitmap(gg$,xx,yy)
  LOCAL x,y,w,h
  w=dpeek(VARPTR(gg$)+0)
  h=dpeek(VARPTR(gg$)+2)
  ' x=dpeek(varptr(gg$)+4)
  ' y=dpeek(varptr(gg$)+6)
  CLR x,y
  gg$=RIGHT$(gg$,LEN(gg$)-8)
  IF w=0 OR h=0 OR x<0 OR y<0 OR h>bh OR w>bw
    PRINT "ERROR bei putbitmap."
  ENDIF
  GRAPHMODE 7
  PUT_BITMAP gg$,xx+x,yy+y,w,h
  GRAPHMODE 1
RETURN
FUNCTION load_bitmap$(ff$)
  LOCAL flag,tt$,aa$,x,y,w,h,bm$,i
  bm$=""
  x=0
  y=0
  OPEN "I",#11,ff$
  WHILE not eof(#11)
    LINEINPUT #11,tt$
    tt$=TRIM$(tt$)
    IF left$(tt$)="#"
      SPLIT tt$," ",0,aa$,tt$
      SPLIT tt$," ",0,aa$,tt$
      IF glob(aa$,"*width*")
        w=VAL(tt$)
      ELSE if glob(aa$,"*height*")
        h=VAL(tt$)
      ELSE if glob(aa$,"*x_hot*")
        x=VAL(tt$)
      ELSE if glob(aa$,"*y_hot*")
        y=VAL(tt$)
      ENDIF
    ELSE if LEFT$(tt$,2)="0x"
      IF right$(tt$,2)="};"
        tt$=LEFT$(tt$,LEN(tt$)-2)
      ENDIF
      WHILE len(tt$)
        SPLIT tt$,",",0,aa$,tt$
        bm$=bm$+CHR$(VAL(aa$) xor 0x0)
      WEND
    ENDIF
  WEND
  CLOSE #11
  RETURN mki$(w)+mki$(h)+mki$(x)+mki$(y)+bm$
ENDFUNCTION
PROCEDURE draw_ramp_logo(xx,yy,ww,hh)
  LINE xx+ww/2-ww/8-ww/8/2,yy-hh/2+hh/4,xx+ww/2-ww/8,yy-hh/2+hh/4
  LINE xx+ww/2-ww/8,yy-hh/2+hh/4,xx+ww/2-ww/8/2,yy-hh+hh/2-hh/4
  LINE xx+ww/2-ww/8/2,yy-hh+hh/2-hh/4,xx+ww/2+ww/8/2,yy-hh+hh/2-hh/4
  LINE xx+ww/2+ww/8/2,yy-hh+hh/2-hh/4,xx+ww/2+ww/8,yy-hh/2+hh/4
  LINE xx+ww/2+ww/8,yy-hh/2+hh/4,xx+ww/2+ww/8+ww/8/2,yy-hh/2+hh/4
RETURN
PROCEDURE draw_scale()
RETURN
PROCEDURE preclear_scale()

RETURN
PROCEDURE prefill_scale()

RETURN

PROCEDURE draw_string(xx,yy,ww,hh,ox,oy,ow,oh,fgc,bgc,font$,text$)
  IF ww>0 AND hh>0 ! erase old string
    COLOR bgc
    DEFLINE ,0
    PBOX ox-2,oy-oh,ox+ow+2,oy+3
  ENDIF
  COLOR fgc
  SETFONT font$
  TEXT xx+2,oy,text$
RETURN

PROCEDURE draw_frame(xx,yy,ww,hh,rr)
  DEFLINE 0,2
  IF rr=1
    COLOR grau
  ELSE
    COLOR schwarz
  ENDIF
  LINE xx,yy,xx+ww,yy
  LINE xx+ww,yy,xx+ww,yy-hh
  IF rr=1
    COLOR schwarz
  ELSE
    COLOR grau
  ENDIF
  LINE xx,yy,xx,yy-hh
  LINE xx,yy-hh,xx+ww,yy-hh
  DEFLINE 0,1
RETURN
PROCEDURE draw_meter(xx,yy,width,height,position,angle1,angle2)
  LOCAL phi,w2,dphi1,dphi2,dx,dy
  w2=width/2
  phi=(180-position)-(180-((angle1/64)+(angle2/64)))
  IF phi>360
    SUB phi,360
  ENDIF
  dphi1=INT(phi+0.5)+90
  dphi2=INT(phi+0.5)+270
  IF dphi1<0
    dphi1=360-dphi1
  ELSE if dphi1>360
    SUB dphi1,360
  ENDIF
  IF dphi2<0
    dphi2=360-dphi2
  ELSE if dphi2>360
    SUB dphi2,360
  ENDIF
  dx=-(w2-2)*cos(phi+0.5)
  dy=-(w2-2)*sin(phi+0.5)
  LINE x+w2,y-w2,x+w2+INT(dx+0.5),y-w2-int(dy+0.5)
  PCIRCLE xx+width/2-2,yy-height/2-2,4
RETURN

FUNCTION getval$(tt$,f$)
  LOCAL a$,val$
  val$=""
  tt$=TRIM$(tt$)
  WHILE len(tt$)
    WORT_SEP tt$," ",1,a$,tt$
    a$=TRIM$(a$)
    WORT_SEP a$,"=",1,name$,val$
    EXIT if UPPER$(name$)=UPPER$(f$)
    val$=""
  WEND
  RETURN val$
ENDFUNCTION
FUNCTION convert_color(t$)
  LOCAL r,g,b,a$
  t$=LEFT$(t$,LEN(t$)-1)
  t$=RIGHT$(t$,LEN(t$)-1)
  SPLIT t$,",",0,a$,t$
  r=VAL(a$)
  SPLIT t$,",",0,a$,t$
  g=VAL(a$)
  SPLIT t$,",",0,a$,t$
  b=VAL(a$)
  RETURN get_color(r,g,b)
ENDFUNCTION
PROCEDURE setcolorfb(t$,a$)
  COLOR @convert_color(t$),@convert_color(a$)
RETURN
PROCEDURE setcolor(t$)
  COLOR @convert_color(t$)
RETURN

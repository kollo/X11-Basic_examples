' Programm zum Nachtraeglichen Skalieren von Korrektordatensaetzen
' Gehoert zum Harmcorr-Experten. Falls man die Korrektur nachtraeglich
' applizieren will, ohne die Lage des Strahls in der Extraktionsbeamline zu
' veraendern

' Markus Hoffmann 1999

ECHO off
DIM hkicks(32),vkicks(32)
FILESELECT "Bitte Korrektordatenfile auswaehlen...","/sgt/elsa/data/korrektoren/*.kicks","",f$
IF len(f$)
  IF not EXIST(f$)
    VOID form_alert(1,"[3][File |"+f$+" |existiert nicht !][ OH ]")
  ELSE
    @read_orbit(f$)
    OPEN "O",#1,"my_kicks.kicks"
    PRINT #1,"Korrektor-Einstellungen (geschrieben von csxbasic)"
    FOR i=1 TO 32
      PRINT #1,"HZ "+STR$(i)+" "+STR$(hkicks(i-1)*1.2/1.9)+" "+STR$(vkicks(i-1)*1.2/1.9)
    NEXT i
    CLOSE
  ENDIF
ENDIF
QUIT

PROCEDURE read_orbit(fname$)
  '
  ' Globale Korrektur einlesen
  '
  LOCAL t$,i,a$,b$
  PRINT "Orbit wird eingelesen..."
  IF fname$=""
    INPUT "Name des Korrekturdatensatzes = ",fname$
    filename$="/sgt/elsa/data/korrektoren/"+fname$+".kicks"
  ELSE
    filename$=fname$
  ENDIF
  IF exist(filename$)
    OPEN "I",#1,filename$
    LINEINPUT #1,t$
    PRINT filename$+" [";
    WHILE not eof(#1)
      LINEINPUT #1,t$
      WORT_SEP t$," ",TRUE,a$,b$
      IF a$="HZ"
        WORT_SEP b$," ",TRUE,a$,b$
        i=VAL(a$)-1
        WORT_SEP b$," ",TRUE,a$,b$
        hkicks(i)=VAL(a$)
        vkicks(i)=VAL(b$)
        PRINT ".";
      ENDIF
    WEND
    CLOSE #1
    PRINT "]"
  ELSE
    ALERT 3,"File nicht gefunden !|"+filename$,1," OH ",balert
  ENDIF
RETURN

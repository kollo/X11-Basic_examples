' X-Y-Steuerung der Strahlschieber Dez. 1998 Markus Hoffmann
ECHO off
'
' CSXBASIC-Programm
' letzte Bearbeitung 11.9.1999     Markus Hoffmann
'
SIZEW ,560,450
DEFMOUSE 2
sw=100

filename$="/sgt/elsa/data/polq1/strahlschieber.dat"

weiss=GET_COLOR(65535,65535,65535)
grau=GET_COLOR(65535/1.2,65535/1.2,65535/1.2)
schwarz=GET_COLOR(0,0,0)
rot=GET_COLOR(65535,0,0)
gelb=GET_COLOR(65535,65535,0)
gruen=GET_COLOR(32000,65535,32000)

' Cops-Daten einlesen

RESTORE copsdata
DIM copx(50),copy(50),csx(50),csy(50)
DIM bcx(50),bcy(50),sname$(50),scale(50),resx(50),resy(50)

CLR c,anzcops
WHILE c<>-1
  READ c
  IF c<>-1
    copx(anzcops)=c
    READ csx(anzcops),copy(anzcops),csy(anzcops),sname$(anzcops),scale(anzcops)
    bcx(anzcops)=anzcops mod 5
    bcy(anzcops)=anzcops div 5
    INC anzcops
  ENDIF
WEND
PRINT "Anzahl: ",anzcops

COLOR grau
PBOX 0,0,560,450
COLOR schwarz
SETFONT "*Courier*-14-*"
TEXT 150,380,"Strahlschieber an der 120kV Quelle"
@button(200,420,"REDRAW",0)
@button(300,420,"sichern",0)
@button(400,420,"laden",0)
@button(500,420,"QUIT",0)
@button(500,360,"Hilfe",0)

@redraw
DEFMOUSE 0
DO
  VSYNC
  MOUSEEVENT x,y,k
  IF @inbutton(500,360,"Hilfe",x,y)
    @button(500,360,"Hilfe",true)
    VSYNC
    h$="Hilfe zm Strahlschiebermenue|"
    h$=h$+"============================||"
    h$=h$+"Mit der Maus k�nnen die roten Punkte in den |"
    h$=h$+"K�sten verschoben werden.||"
    h$=h$+"Durch Anklicken der gelben Kaestchen erfolgt ein|"
    h$=h$+"kleinster Schritt in die jeweilige Richtung.|"

    VOID form_alert(1,"[1]["+h$+"][  OK  ]")=1
    @button(500,360,"Hilfe",0)
    VSYNC
  ELSE if @inbutton(500,420,"QUIT",x,y)
    @button(500,420,"QUIT",true)
    VSYNC
    IF form_alert(1,"[2][Wirklich beenden ?][QUIT|CANCEL]")=1
      QUIT
    ENDIF
    @button(500,420,"QUIT",0)
    VSYNC
  ELSE if @inbutton(200,420,"REDRAW",x,y)
    @button(200,420,"REDRAW",TRUE)
    DEFMOUSE 2
    VSYNC
    @redraw
    DEFMOUSE 0
    @button(200,420,"REDRAW",0)
    VSYNC
  ELSE if @inbutton(300,420,"sichern",x,y)
    @button(300,420,"sichern",TRUE)
    VSYNC
    @sichern
    @button(300,420,"sichern",0)
    VSYNC
  ELSE if @inbutton(400,420,"laden",x,y)
    @button(400,420,"laden",TRUE)
    DEFMOUSE 2
    VSYNC
    @laden
    @redraw
    @button(400,420,"laden",0)
    DEFMOUSE 0
    VSYNC
  ELSE
    selected=@find_ob(x,y)
    IF selected>=0 AND selected<anzcops
      ' ist es gelbes Kaestchen ?
      kr=@gelb(selected,x,y)
      IF kr>0
        IF kr=1
        ELSE if kr=2
        ELSE if kr=3
        ELSE if kr=4
        ELSE
          PRINT "Fehler !"
        ENDIF
      ELSE if @getcop(selected,x,y)

        MOTIONEVENT x,y,,,k2
        WHILE k2=256
          newx=@ox(selected,x)
          newy=@oy(selected,y)
          IF abs(newx)<scale(selected)/2 AND abs(newy)<scale(selected)/2
            CSPUT "POL_SOURCE1_COPS"+STR$(copx(selected))+".STROM"+STR$(csx(selected))+"_AC",abs(newx)
            CSPUT "POL_SOURCE1_COPS"+STR$(copy(selected))+".STROM"+STR$(csy(selected))+"_AC",abs(newy)
            CSPUT "POL_SOURCE1_COPS"+STR$(copx(selected))+".POL"+STR$(csx(selected))+"_DM",(sgn(newx)+1)/2
            CSPUT "POL_SOURCE1_COPS"+STR$(copy(selected))+".POL"+STR$(csy(selected))+"_DM",(sgn(newy)+1)/2
            @drawstrahls(10+(sw+10)*bcx(selected),10+(sw+10)*bcy(selected),newx,newy,scale(selected))
          ENDIF
          VSYNC
          MOTIONEVENT x,y,,,k2
        WEND
        @drawcop(selected)
      ENDIF
    ENDIF
  ENDIF
LOOP
PAUSE 5
END

PROCEDURE redraw
  LOCAL i
  FOR i=0 TO anzcops-1
    @drawcop(i)
  NEXT i
RETURN

FUNCTION ox(n,mx)
  RETURN (mx-10-(sw+10)*bcx(n)-sw/2)*scale(n)/sw
ENDFUNC
FUNCTION oy(n,my)
  RETURN -(my-10-(sw+10)*bcy(n)-sw/2)*scale(n)/sw
ENDFUNC
FUNCTION kx(n,a)
  RETURN 10+(sw+10)*bcx(n)+sw/2+a/scale(n)*sw
ENDFUNC
FUNCTION ky(n,b)
  RETURN 10+(sw+10)*bcy(n)+sw/2-b/scale(n)*sw
ENDFUNC

FUNCTION find_ob(fx,fy)
  LOCAL i,x,y
  x=fx div (sw+10)
  y=fy div (sw+10)
  FOR i=0 TO anzcops-1
    IF x=bcx(i) AND y=bcy(i)
      RETURN i
    ENDIF
  NEXT i
  RETURN -1
ENDFUNC

FUNCTION getcop(n,gx,gy)
  LOCAL hspx,hspy,a,b,sx,sy,wx,wy
  a=csget("POL_SOURCE1_COPS"+STR$(copx(n))+".STROM"+STR$(csx(n))+"_AC")
  a=-(1-2*csget("POL_SOURCE1_COPS"+STR$(copx(n))+".POL"+STR$(csx(n))+"_DM"))*a
  b=csget("POL_SOURCE1_COPS"+STR$(copy(n))+".STROM"+STR$(csy(n))+"_AC")
  b=-(1-2*csget("POL_SOURCE1_COPS"+STR$(copy(n))+".POL"+STR$(csy(n))+"_DM"))*b
  sx=10+(sw+10)*bcx(n)
  sy=10+(sw+10)*bcy(n)
  wx=a
  wy=b
  hspx=sx+sw/2+wx/scale(n)*sw
  hspy=sy+sw/2-wy/scale(n)*sw
  IF (gx-hspx)^2+(gy-hspy)^2<16
    RETURN true
  ENDIF
  RETURN false
ENDFUNC
FUNCTION gelb(n,gx,gy)
  ' noch unvoll
  LOCAL hspx,hspy,a,b,sx,sy,wx,wy
  sx=10+(sw+10)*bcx(n)
  sy=10+(sw+10)*bcy(n)
  hspx=sx+sw/2+wx/scale(n)*sw
  hspy=sy+sw/2-wy/scale(n)*sw
  IF (gx-hspx)^2+(gy-hspy)^2<16
    RETURN true
  ENDIF
  RETURN false
ENDFUNC

PROCEDURE drawcop(n)
  LOCAL a,b
  a=csget("POL_SOURCE1_COPS"+STR$(copx(n))+".STROM"+STR$(csx(n))+"_AC")
  a=-(1-2*csget("POL_SOURCE1_COPS"+STR$(copx(n))+".POL"+STR$(csx(n))+"_DM"))*a
  b=csget("POL_SOURCE1_COPS"+STR$(copy(n))+".STROM"+STR$(csy(n))+"_AC")
  b=-(1-2*csget("POL_SOURCE1_COPS"+STR$(copy(n))+".POL"+STR$(csy(n))+"_DM"))*b

  @drawstrahls(10+(sw+10)*bcx(n),10+(sw+10)*bcy(n),a,b,scale(n))
  TEXT 10+(sw+10)*bcx(n),10+(sw+10)*bcy(n),sname$(n)

RETURN

PROCEDURE drawstrahls(sx,sy,wx,wy,ssmax)
  COLOR weiss
  PBOX sx,sy,sx+sw,sy+sw
  COLOR schwarz
  BOX sx,sy,sx+sw,sy+sw
  COLOR gruen
  LINE sx,sy+sw/2,sx+sw,sy+sw/2
  LINE sx+sw/2,sy,sx+sw/2,sy+sw
  COLOR rot
  PCIRCLE sx+sw/2+wx/ssmax*sw,sy+sw/2-wy/ssmax*sw,4
  COLOR schwarz
  CIRCLE sx+sw/2+wx/ssmax*sw,sy+sw/2-wy/ssmax*sw,4
  ' @button(sx,sy,chr$(1),0)
  COLOR gelb
  PBOX sx+sw/2-3,sy,sx+sw/2+3,sy+6
  PBOX sx+sw/2-3,sy+sw,sx+sw/2+3,sy+sw-6
  PBOX sx,sy+sw/2-3,sx+6,sy+sw/2+3
  PBOX sx+sw,sy+sw/2-3,sx+sw-6,sy+sw/2+3
  COLOR schwarz
  BOX sx+sw/2-3,sy,sx+sw/2+3,sy+6
  BOX sx+sw/2-3,sy+sw,sx+sw/2+3,sy+sw-6
  BOX sx,sy+sw/2-3,sx+6,sy+sw/2+3
  BOX sx+sw,sy+sw/2-3,sx+sw-6,sy+sw/2+3
RETURN
PROCEDURE button(button_x,button_y,button_t$,sel)
  LOCAL x,y,w,h
  DEFLINE ,1
  DEFTEXT 1,0.05,0.1,0
  button_l=ltextlen(button_t$)

  x=button_x-button_l/2-10
  y=button_y-10
  w=button_l+20
  h=20
  COLOR grau
  PBOX x+5,y+5,x+w+5,y+h+5
  COLOR abs(sel)*schwarz+abs(not sel)*weiss
  PBOX x,y,x+w,y+h
  IF sel=-1
    COLOR weiss
  ELSE
    COLOR schwarz
  ENDIF
  BOX x,y,x+w,y+h
  BOX x-1,y-1,x+w+1,y+h+1

  LTEXT button_x-button_l/2,button_y-5,button_t$
RETURN

FUNCTION inbutton(button_x,button_y,button_t$,mx,my)
  LOCAL x,y,w,h

  DEFTEXT 1,0.05,0.1,0
  button_l=ltextlen(button_t$)

  x=button_x-button_l/2-10
  y=button_y-10
  w=button_l+20
  h=20
  IF mx>=x AND my>=y AND mx<=x+w AND my<=y+h
    RETURN true
  ELSE
    RETURN false
  ENDIF
ENDFUNC

PROCEDURE sichern
  LOCAL i,a,b,nam1$,nam2$,wert1,wert2,pol1,pol2,pnam1$,pnam2$
  OPEN "O",#1,filename$
  PRINT #1,"# Dieses File wurde erzeugt von /sgt/elsa/bas/strahlschieber.bas "
  PRINT #1,"# Am "+date$+" um "+time$+" von "+env$("USER")
  PRINT #1,"#================================================================"
  PRINT #1,"#"
  FOR i=0 TO anzcops-1
    nam1$="POL_SOURCE1_COPS"+STR$(copx(i))+".STROM"+STR$(csx(i))+"_AC"
    wert1=csget(nam1$)
    pnam1$="POL_SOURCE1_COPS"+STR$(copx(i))+".POL"+STR$(csx(i))+"_DM"
    pol1=csget(pnam1$)
    a=-(1-2*pol1)*wert1
    nam2$="POL_SOURCE1_COPS"+STR$(copy(i))+".STROM"+STR$(csy(i))+"_AC"
    wert2=csget(nam2$)
    pnam2$="POL_SOURCE1_COPS"+STR$(copy(i))+".POL"+STR$(csy(i))+"_DM"
    pol2=csget(pnam2$)
    b=-(1-2*pol2)*wert2
    PRINT #1,"# "+sname$(i)+": x="+STR$(a)+" y="+STR$(b)
    PRINT #1,pnam1$+", "+STR$(pol1)
    PRINT #1,nam1$+", "+STR$(wert1)
    PRINT #1,pnam2$+", "+STR$(pol2)
    PRINT #1,nam2$+", "+STR$(wert2)
  NEXT i
  CLOSE #1
RETURN

PROCEDURE laden
  LOCAL t$,wert
  IF not EXIST(filename$)
    VOID form_alert(1,"[3]["+filename$+"|existiert nicht !|Ein Datensatz muss erst durch SICHERN|erzeugt werden !][ABBRUCH]")
  ELSE
    OPEN "I",#1,filename$
    WHILE not eof(#1)
      INPUT #1,t$
      IF left$(t$)<>"#"
        INPUT #1,wert
        CSSET t$,wert
      ENDIF
    WEND
    CLOSE #1
  ENDIF
RETURN

copsdata:
DATA 1,1,1,2,"SSQuelle",1
DATA 1,3,1,4,"SSA1",0.5
DATA 1,5,1,6,"SSA2",1
DATA 1,6,1,8,"SSB12",1
DATA 2,1,2,2,"SSB11",1
DATA 2,3,2,4,"SSB21",1
DATA 2,5,2,6,"SSB22",1
DATA 2,7,2,8,"SSC",1
DATA 3,1,3,2,"SSD11",1
DATA 3,3,3,4,"SSD12",1
DATA 3,5,3,6,"SSD2",1
DATA 3,7,3,8,"SSE1",1
DATA 4,1,4,2,"SSE2",1
DATA 4,3,4,4,"SSF1",1
DATA 4,5,4,6,"SSF2",1
DATA 4,7,4,8,"SSLinac",4
DATA -1,-1,-1,-1,"",-1

' Calculates Surfaces of two hollow Balls and saves it into a file
' It then can be displayed with 3Dshow.bas
' (c) Markus Hoffmann 1990

PRINT "MAKE-WORLD   (c) Markus Hoffmann 1990"
meldung$="Surfaces of two hollow Balls"+CHR$(0)
maxworld%=10000
mf%=maxworld%*13*8
world%=MALLOC(mf%)
maxworld%=mf%/13/8
PRINT
PRINT "Maxworld:"'maxworld%
'
'
x_1=-1
x_2=1
y_1=-1
y_2=1
z_1=-0.1
z_2=1
'
sx=0.05
sy=0.05
sz=0.1
'
CLR anzworld%
PRINT "Berechne Welt:"
PRINT "Koordinatenkreuz..."
' @add4fl(0,x_1,y_2,z_1/2,x_2,y_2,z_1/2,x_2,y_2,z_2/2,x_1,y_2,z_2/2)
' @add4fl(0,x_1,y_2,z_1/2,x_1,y_2,z_2/2,x_1,y_1,z_2/2,x_1,y_1,z_1/2)
PRINT "WWFkt."
' Außenkugel: Transparent
r=0.5
spsp=PI/15
st=PI/15
FOR phi=0 TO 2*PI STEP spsp
  FOR theta=0.05 TO PI STEP st
    PRINT chr$(13);"Flächen:"'anzworld%;
    FLUSH
    @polar1(r,theta,phi)
    @polar2(r,theta+st,phi)
    @polar3(r,theta+st,phi+spsp)
    @polar4(r,theta,phi+spsp)
    @add4fl(0,x1,y1,z1,x2,y2,z2,x3,y3,z3,x4,y4,z4)
  NEXT theta
NEXT phi
' Innenkukel: Solid
r=0.3
spsp=PI/10
st=PI/10
PRINT
FOR phi=0 TO 2*PI STEP spsp
  FOR theta=0.05 TO PI STEP st
    PRINT chr$(13);"Flächen:"'anzworld%;
    @polar1(r,theta,phi)
    @polar2(r,theta+st,phi)
    @polar3(r,theta+st,phi+spsp)
    @polar4(r,theta,phi+spsp)
    @add4fl(16,x1,y1,z1,x2,y2,z2,x3,y3,z3,x4,y4,z4)
  NEXT theta
NEXT phi
PRINT
'
PRINT "Welt: ";anzworld%;" Elemente..."
@saveworld
@ende
'
PROCEDURE polar1(r,th,ph)
  x1=r*COS(ph)*SIN(th)
  y1=r*SIN(ph)*SIN(th)
  z1=r*COS(th)
RETURN
PROCEDURE polar2(r,th,ph)
  x2=r*COS(ph)*SIN(th)
  y2=r*SIN(ph)*SIN(th)
  z2=r*COS(th)
RETURN
PROCEDURE polar3(r,th,ph)
  x3=r*COS(ph)*SIN(th)
  y3=r*SIN(ph)*SIN(th)
  z3=r*COS(th)
RETURN
PROCEDURE polar4(r,th,ph)
  x4=r*COS(ph)*SIN(th)
  y4=r*SIN(ph)*SIN(th)
  z4=r*COS(th)
RETURN
'
PROCEDURE add4fl(nu,x1,y1,z1,x2,y2,z2,x3,y3,z3,x4,y4,z4)
  IF anzworld%<maxworld%
    BMOVE varptr(x1),world%+anzworld%*104+0,8
    BMOVE varptr(y1),world%+anzworld%*104+8,8
    BMOVE varptr(z1),world%+anzworld%*104+2*8,8
    BMOVE varptr(x2),world%+anzworld%*104+3*8,8
    BMOVE varptr(y2),world%+anzworld%*104+4*8,8
    BMOVE varptr(z2),world%+anzworld%*104+5*8,8
    BMOVE varptr(x3),world%+anzworld%*104+6*8,8
    BMOVE varptr(y3),world%+anzworld%*104+7*8,8
    BMOVE varptr(z3),world%+anzworld%*104+8*8,8
    BMOVE varptr(x4),world%+anzworld%*104+9*8,8
    BMOVE varptr(y4),world%+anzworld%*104+10*8,8
    BMOVE varptr(z4),world%+anzworld%*104+11*8,8
    INC anzworld%
  ENDIF
RETURN
PROCEDURE saveworld
  FILESELECT "Welt abspeichern als ...","./*.xxx","welt.xxx",sa$
  IF LEN(sa$)
    OPEN "O",#1,sa$
    anzfl=anzworld%
    sortstart=0
    BPUT #1,VARPTR(meldung$),64
    BPUT #1,VARPTR(anzfl),8
    BPUT #1,VARPTR(sortstart),8
    BPUT #1,world%,anzworld%*8*13
    CLOSE #1
  ENDIF
RETURN
'
PROCEDURE ende
  FREE world%
  QUIT
RETURN

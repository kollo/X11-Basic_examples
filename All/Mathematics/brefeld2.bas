' Errechnet Beispiele zu Brefelds Vermutung
' Das Problem wird ausführlich hier diskutiert:
' http://www.brefeld.homepage.t-online.de/neunstellig-1.html
' Algorithm in X11-Basic
' by Markus Hoffmann 2015
' Verbesserter Algorithmus, schneller aber komplizierter
'

FOR base%=2 TO 36 STEP 2
  CLR count%
  t=TIMER
  bh$=RADIX$(INT(base%/2),base%)
  bh%=INT(base%/2)
  bhm1%=INT(base%/2)-1
  d$=@extend$("")
  PRINT "There are ";count%;" solutions for base ";base%;"; ";INT(TIMER-t);" Seconds."
NEXT base%
QUIT

FUNCTION extend$(a$)
  LOCAL i%,b$,c$,l%
  l%=LEN(a$)
  IF l%=base%-1
    PRINT a$;" solution.base";base%
    INC count%
  ENDIF
  IF l%=bhm1%
    b$=bh$
    IF INSTR(a$,b$)=0
      c$=a$+b$
      IF @isvalid(c$)
        c$=@extend$(c$)
        IF LEN(c$)>l%
          RETURN c$
        ENDIF
      ENDIF
    ENDIF
  ELSE if l%>0 AND odd(l%)
    FOR i%=2 TO base%-1 STEP 2
      b$=RADIX$(i%,base%)
      IF INSTR(a$,b$)=0
        c$=a$+b$
        IF @isvalid(c$)
          c$=@extend$(c$)
          IF LEN(c$)>l%
            RETURN c$
          ENDIF
        ENDIF
      ENDIF
    ENDIF
  NEXT i%
ELSE
  FOR i%=1 TO base%-1 STEP 2
    IF i%=bh% AND l%<bhm1%
      ADD i%,2
    ENDIF
    b$=RADIX$(i%,base%)
    IF INSTR(a$,b$)=0
      c$=a$+b$
      IF @isvalid(c$)
        c$=@extend$(c$)
        IF LEN(c$)>l%
          RETURN c$
        ENDIF
      ENDIF
    ENDIF
  NEXT i%
ENDIF
RETURN ""
ENDFUNCTION

FUNCTION isvalid(p$)
  LOCAL i%,a&
  FOR i%=1 TO LEN(p$)
    a&=@valradix&(LEFT$(p$,i%))
    ' print a,a mod i
    IF (a& MOD i%)
      RETURN 0
    ENDIF
  NEXT i%
  RETURN -1
ENDFUNCTION

FUNCTION valradix&(t$)
  LOCAL i%,a&
  CLR a&,o
  FOR i%=0 TO LEN(t$)-1
    o=PEEK(VARPTR(t$)+i%)
    IF o>=ASC("a")
      ADD o,10-ASC("a")
    ELSE IF o>=ASC("A")
      ADD o,10-ASC("A")
    ELSE
      SUB o,ASC("0")
    ENDIF
    a&=a&*base%+INT(o)
  NEXT i%
  '  print "valradix ",t$,"->",a
  RETURN a&
ENDFUNCTION

' very simple program to demonstrate calculations in X11-Basic
' by Markus hoffmann 1999-04-01
'
' calculates the square root of a number without using the
' built-in function sqr()
'
' demonstrates GOTO

PRINT "Dieses Programm rechnet die Quadratwurzel einer Zahl"
INPUT "Zahl=",z
r124=1
105:
r123=r124
r124=(r123^2+z)/(2*r123)
IF ABS(r124-r123)-0.00001>0
  PRINT r124
  GOTO 105
ENDIF
PRINT "Das Ergebnis des Algoritmus:"'r124
PRINT "Zum Vergleich: sqr(";z;")=";SQR(z)
PRINT "Abweichung:"'ABS(SQR(z)-r124)
END

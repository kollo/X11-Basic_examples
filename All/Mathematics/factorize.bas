a=1111111111

smallprimes()=[2,3,5,7,11,13,17,19,23,29,31,37,41,43,47]
PRINT a;"=";
FOR i=0 TO 14
  WHILE (a MOD smallprimes(i))=0
    PRINT smallprimes(i);"*";
    FLUSH
    a=a/smallprimes(i)
  WEND
  EXIT if a<lim
NEXT i

IF a<53*53
  PRINT a
  QUIT
ENDIF

' Alle Primzahlen bestimmen bis lim

l=@sieve(a)

' print "l=";l

IF f(a)=1
  PRINT a
  QUIT
ENDIF

FOR i=0 TO anzprimes-1

  WHILE (a MOD primes(i))=0
    PRINT primes(i);"*";
    FLUSH
    a=a/primes(i)
  WEND
  EXIT if a<i
ENDIF
NEXT i
PRINT
QUIT

e=@euclid(a,l)

IF e=1
  PRINT a
  QUIT
ELSE
  PRINT "GGT: ";e
ENDIF

QUIT

FUNCTION euclid(a,b)
  LOCAL h
  WHILE b<>0
    h=a mod b
    a=b
    b=h
  WEND
  RETURN a
ENDFUNCTION

FUNCTION sieve(s)
  LOCAL i
  prod=1
  DIM f(s+1)
  DIM primes(s)
  CLR anzprimes
  ARRAYFILL f(),1
  PRINT "[";a;":";
  FLUSH
  FOR i=2 TO s
    IF f(i)
      IF 2*i<s
        FOR k=2*i TO s STEP i
          CLR f(k)
          ' f(k)=0
        NEXT k
        FLUSH
      ENDIF
      ' PRINT i,
      prod=prod*i
      ' exit if prod>s
      primes(anzprimes)=i
      INC anzprimes
    ENDIF
  NEXT i
  PRINT anzprimes;
  PRINT "]";
  FLUSH
  ' PRINT
  ' PRINT c;" primes up to ";s;" found. ";
  RETURN prod
ENDFUNCTION

' calculate all prime numbers up to 1000000 and measures the time needed
' using a sieve algotithm
' written in X11-Basic by Markus Hoffmann 2000-02-28

' "sieve.bas" , a prime number sieve benchmark

@sieve(1000000)

QUIT

PROCEDURE sieve(s)
  LOCAL i
  tc=CTIMER
  DIM f(s+1)
  ARRAYFILL f(),1
  CLR c
  FOR i=2 TO s
    IF f(i)
      IF 2*i<s
        FOR k=2*i TO s STEP i
          CLR f(k)
          ' f(k)=0
        NEXT k
        FLUSH
      ENDIF
      PRINT i,
      INC c
    ENDIF
  NEXT i
  PRINT
  PRINT c;" primes up to ";s;" found. ";
  tc=CTIMER-tc
  PRINT "CPU time: ";tc;" s"
RETURN

' Writes out (to stdout) a latex file with a table with Decibel values
' (c) by Markus Hoffmann
' 2008-03-28
'
'
PRINT "\documentclass[12pt,a4paper,dvips,openany,final]{scrartcl}"
PRINT "\begin{document} "
PRINT "\subsection*{dB Tabelle}"
PRINT "\begin{tabular}{|r|cc|}"
PRINT "\hline"
PRINT "{\bf dB} & {\bf Amplitude} & {\bf Leistung}\\"
PRINT "\hline"
db=-90
DO
  PRINT STR$(db,5,5);" & ";STR$(10^(db/20),6,3);" & ";STR$(10^(db/10),6,3);" \\"
  ADD db,10
  EXIT IF db>60
LOOP
PRINT "\hline"
PRINT "\end{tabular}"
PRINT "\begin{tabular}{|r|cc|}"
PRINT "\hline"
PRINT "{\bf dB} & {\bf Amplitude} & {\bf Leistung}\\"
PRINT "\hline"
db=-10
DO
  PRINT STR$(db,5,5);" & ";STR$(10^(db/20),6,3);" & ";STR$(10^(db/10),6,3);" \\"
  ADD db,1
  EXIT IF db>10
LOOP
PRINT "\hline"
PRINT "\end{tabular}"
PRINT "\end{document}"
QUIT

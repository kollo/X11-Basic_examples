' Test if a number is prime in X11-basic by vsync
'
'
INPUT "Enter a (big) number: ",a$
a&=VAL(a$)
PRINT a&,
b&=nextprime(a&-1)
IF b&=a&
  PRINT " is prime."
ELSE
  PRINT " is not prime, but ";b&;" is!"
ENDIF
QUIT

' Random Number statistics M.H. 2003
' Is there something wrong with the random generator ?

DIM m(10)
n=20000
RANDOMIZE
t=CTIMER
PRINT "MIN:"
FOR a=1 TO n
  m=10
  FOR b=1 TO 10
    m=MIN(m,ROUND(RANDOM(100)/10))
  NEXT b
  m(m)=m(m)+1
NEXT a

FOR a=0 TO 10
  PRINT a,": ",ROUND(m(a)/n,5)
NEXT a

ARRAYFILL m(),0
PRINT "MAX:"
FOR a=1 TO n
  m=0
  FOR b=1 TO 10
    m=MAX(m,ROUND(RANDOM(100)/10))
  NEXT b
  m(m)=m(m)+1
NEXT a
FOR a=0 TO 10
  PRINT a,": ",ROUND(m(a)/n,5)
NEXT a
PRINT "CPU time: ",CTIMER-t;" sec."
QUIT

' Solves a sudoku puzzle (c) Markus Hoffmann 2015
'
' this program produces one solution and ends. There could be also
' more than one solutions.
'
' This defines the sudoku
get$()=[ \
" ","3"," "," "," "," "," "," "," "; \
" "," "," ","1","9","5"," "," "," "; \
" "," ","8"," "," "," "," ","6"," "; \
"8"," "," "," ","6"," "," "," "," "; \
"4"," "," ","8"," "," "," "," ","1"; \
" "," "," "," ","2"," "," "," "," "; \
" ","6"," "," "," "," ","2","8"," "; \
" "," "," ","4","1","9"," "," ","5"; \
" "," "," "," "," "," "," ","7"," "]

CLR count%
DIM search(81)
CLR search()
DIM sudoku(9,9),solution(9,9)
FOR i%=0 TO 8
  FOR j%=0 TO 8
    IF get$(i%,j%)=" " OR get$(i%,j%)=""
      sudoku(i%,j%)=0
    ELSE
      sudoku(i%,j%)=VAL(get$(i%,j%))
    ENDIF
  NEXT j%
NEXT i%
' ungültige Zahlen ermitteln
DIM un$(9,9)

CLS
t=TIMER
' print out the sudoku
FOR i=0 TO 8
  FOR j=0 TO 8
    PRINT sudoku(i,j);" ";
  NEXT j
  PRINT
NEXT i

' solving
a=@doit(sudoku())
CLS
IF a=2
  PRINT "Sudoku could not be solved!"
ELSE
  PRINT "Sudoku solved."
ENDIF

' print out the solution
FOR i=0 TO 8
  FOR j=0 TO 8
    PRINT solution(i,j);" ";
  NEXT j
  PRINT
NEXT i
PRINT count%;" steps taken in ";TIMER-t;" seconds."
IF UNIX?
  QUIT
ELSE
  END
ENDIF

FUNCTION doit(s())
  LOCAL i%,j%,fi%,fj%,t$,k%
  HOME
  INC count%
  ' Freies Feld ermitteln
  FOR i%=0 TO 8
    f=1
    FOR j%=0 TO 8
      IF s(i%,j%)=0
        f=0
        BREAK
      ENDIF
    NEXT j%
    EXIT IF f=0
  NEXT i%
  IF i%>8 OR j%>8
    solution()=s()
    RETURN 1
  ENDIF
  IF s(i%,j%)=0
    fi%=i%
    fj%=j%
  ELSE
    solution()=s()
    RETURN 1
  ENDIF
  ' Ungültige berechnen
  un$=@ungueltige$(s(),fi%,fj%)
  ' lite mit gültigen Zahlen erstellen
  t$=@gueltige$(un$)
  IF timer-tnn>1
    ' print out the solution
    FOR i%=0 TO 8
      FOR j%=0 TO 8
        PRINT s(i%,j%);" ";
      NEXT j%
      PRINT
    NEXT i%
    PRINT STR$(SP,2);"|";
    FOR ff=1 TO sp
      PRINT search(ff);
    NEXT ff
    PRINT "> "
    tnn=timer
    PRINT at(9,20);"Freies Feld: ";fi%;"-";fj%;" ";un$;" <";t$;"> "
  ENDIF
  IF len(t$)=0
    RETURN 2
  ENDIF
  search(sp)=LEN(t$)
  ' Liste ausprobieren
  FOR k%=0 TO LEN(t$)-1
    ' print "probiere: ";k%;" ";mid$(t$,k%+1)
    s(fi%,fj%)=VAL(MID$(t$,k%+1))
    search(sp)=LEN(t$)-k%
    a=@doit(s())
    IF a=1
      RETURN 1
      ' else if a=2
    ELSE
      ' print "a=";a
    ENDIF
  NEXT k%
  ' Rueckgabewerte:
  ' 0 = esgibt noch freie felder
  ' 1 = Sudoku gelöst
  ' 2 = Sudoku illegal

  RETURN 0
ENDFUNCTION

FUNCTION ungueltige$(s(),i%,j%)
  LOCAL k%,a%,b%,x%,y&,un$
  CLR un$
  ' ungültige Zahlen ermitteln
  FOR k%=0 TO 8
    un$=un$+STR$(s(i%,k%))
  NEXT k%
  FOR k%=0 TO 8
    un$=un$+STR$(s(k%,j%))
  NEXT k%
  ' 3x3 Felder auswerten
  a%=INT(i%/3)*3
  b%=INT(j%/3)*3
  ' print i%,j%,a%,b%
  FOR x%=a% TO a%+2
    FOR y%=b% TO b%+2
      un$=un$+STR$(s(x%,y%))
    NEXT y%
  NEXT x%
  RETURN un$
ENDFUNCTION
FUNCTION gueltige$(a$)
  LOCAL i,r$
  FOR i=1 TO 9
    IF INSTR(a$,STR$(i))=0
      r$=r$+STR$(i)
    ENDIF
  NEXT i
  RETURN r$
ENDFUNCTION

' ############################ (c) Markus Hoffmann 1990
' ## mehr-K�rper Simulation ##
' ############################
'
'

bw%=700
bh%=700

SIZEW ,bw%,bh%
schwarz=COLOR_RGB(0,0,0)
weiss=COLOR_RGB(1,1,0.15)
anz%=20
dt=0.5
DIM x(anz%+1),y(anz%+1),z(anz%+1),vx(anz%+1),vy(anz%+1),vz(anz%+1),m(anz%+1)
SHOWPAGE
PAUSE 0.1
GET_GEOMETRY 1,bx%,by%,bw%,bh%
'
ARRAYFILL m(),1
FOR i%=0 TO anz%-1
  x(i%)=RANDOM(20)+bw%/2
  y(i%)=RANDOM(20)+bh%/2
NEXT i%
m(3)=9
x(3)=2
y(3)=150
m(2)=3
x(2)=15
vx(2)=-5*9/10
y(2)=110
m(1)=14
x(1)=150
y(1)=100
vx(1)=5
'
COLOR schwarz
PBOX bx%,by%,bw%,bh%
COLOR rot
TEXT 0,50,"many body simulation in 2D"
'~XBIOS(5,L:s1,L:s2,-1)
'SWAP s1,s2
'
'
CLR mges,spx,spy
FOR i%=1 TO anz%-1
  mges=mges+m(i%)
NEXT i%
DO
  IF MOUSEK
    x(2)=MOUSEX
    y(2)=MOUSEY
    m(2)=10
  ENDIF

  FOR i%=1 TO anz%-1
    x=x(i%)-(spx-bw%/2)
    y=y(i%)-(spy-bh%/2)
    IF x<0 OR y<0 OR x>bw% OR y>bh% AND 0
      ' PRINT x,y
      PAUSE 50/200
      SWAP x(i%),x(anz%)
      SWAP y(i%),y(anz%)
      SWAP m(i%),m(anz%)
      DEC anz%
      IF anz%=0
        @b
      ENDIF
      CLR mges,spx,spy
      FOR i%=0 TO anz%-1
        mges=mges+m(i%)
      NEXT i%
      x=x(i%)-(spx-bw%/2)
      y=y(i%)-(spy-bh%/2)
    ENDIF
    BOX x-m(i%),y-m(i%),x+m(i%),y+m(i%)
  NEXT i%
  x=spx-(spx-bw%/2)
  y=spy-(spy-bh%/2)
  LINE x-2,y,x+2,y
  LINE x,y-2,x,y+2
  ' PRINT AT(2,20);TIMER-t'''
  SHOWPAGE
  PAUSE 0.01
  COLOR schwarz
  PBOX 0,0,bw%,bh%
  COLOR weiss
  '  ~XBIOS(5,L:s1,L:s2,-1)
  t=TIMER
  '  SWAP s1,s2
  CLR spx,spy,spz
  FOR i%=1 TO anz%-1
    f=dt/m(i%)
    FOR j%=1 TO anz%-1
      IF i%<>j%
        ADD vx(i%),f*@fx(x(i%),y(i%),z(i%),m(i%),x(j%),y(j%),z(j%),m(j%))
        ADD vy(i%),f*@fy(x(i%),y(i%),z(i%),m(i%),x(j%),y(j%),z(j%),m(j%))
        ADD vz(i%),f*@fz(x(i%),y(i%),z(i%),m(i%),x(j%),y(j%),z(j%),m(j%))
      ENDIF
    NEXT j%
    ADD x(i%),vx(i%)*dt
    ADD y(i%),vy(i%)*dt
    ADD z(i%),vz(i%)*dt
    ADD spx,x(i%)*m(i%)
    ADD spy,y(i%)*m(i%)
    ADD spz,z(i%)*m(i%)
  NEXT i%
  spx=spx/mges
  spy=spy/mges
  spz=spz/mges
LOOP
QUIT

PROCEDURE b
  PRINT anz%
  END
RETURN

FUNCTION fx(x1,y1,z1,m1,x2,y2,z2,m2)
  RETURN (x2-x1)*m1*m2/((x2-x1)^2+(y2-y1)^2+(z2-z1)^2)
ENDFUNC

FUNCTION fy(x1,y1,z1,m1,x2,y2,z2,m2)
  RETURN (y2-y1)*m1*m2/((x2-x1)^2+(y2-y1)^2+(z2-z1)^2)
ENDFUNC

FUNCTION fz(x1,y1,z1,m1,x2,y2,z2,m2)
  RETURN (z2-z1)*m1*m2/((x2-x1)^2+(y2-y1)^2+(z2-z1)^2)
ENDFUNC

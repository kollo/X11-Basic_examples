'
' Program for simulating the phase space echo effect
' in Proton accelerators                 (c) Markus Hoffmann 03/2003\
' Theoorie see: G. Bassi "Stochastic Beam Dynamics in Storage Rings"
' make a movie with:
' convert b???.xpm movie.mng
'
' This simulation uses a double potential RF Bucket

n=1000 ! number of particles to be tracked
q=0.2  ! quadrupol kick value
dq=0.4 ! dipole kick value
dt=0.1 ! time resolution
bx1=100
by1=80
bw=256
bh=256
bx2=356
by2=80
a1=1.2875
a2=4.1201
DIM y(n),x(n)
DIM y2(n),x2(n)
CLR count
' @init_random
' @init_gauss
@init_line
FOR i=0 TO n-1
  y2(i)=y(i)
  x2(i)=x(i)
NEXT i
SIZEW ,512,512
BOUNDARY 0
schwarz=GET_COLOR(0,0,0)
weiss=GET_COLOR(65535,65535,65535)
grau=GET_COLOR(20000,20000,20000)
rot=GET_COLOR(65535,0,0)
gruen=GET_COLOR(0,65535,0)
gelb=GET_COLOR(65535,65535,0)
COLOR schwarz
PBOX bx1,by1,bx1+bw,by1+bh+100
COLOR grau
LINE bx1,by1+bw+50,bx1+bw,by1+bw+50
DO
  COLOR schwarz
  PBOX bx1,by1,bx1+bw,by1+bh
  COLOR rot
  TEXT bx1,by1+10,STR$(count)
  TEXT bx1+bw-80,by1+bh+95,"(c) 2003 MH"
  COLOR weiss
  meanx=0
  FOR i=0 TO n-1
    y(i)=y(i)-dt*(a1*sin(x(i))+a2*sin(4*x(i)))
    x(i)=x(i)+y(i)*dt
    PCIRCLE bx1+bw/2+x(i)*bw/2,by1+bh/2+y(i)*bh/5,2
    ADD meanx,x(i)
  NEXT i
  meanx=meanx/n
  COLOR gelb
  LINE oo,ooo,bx1+count/3,by1+bw+meanx*150+50
  oo=bx1+count/3
  ooo=by1+bw+meanx*150+50
  VSYNC
  GET bx1,by1,bx1+bw-bx1,by1+bh-by1+100,t$
  IF count>=1000
    QUIT
  ENDIF
  BSAVE "b"+STR$(count,3,3,1)+".xpm",VARPTR(t$),LEN(t$)
  IF mousek=2
    PRINT "quadrupole kick"
    COLOR gruen
    LINE bx1+count/3,by1+bw+meanx*70+70,bx1+count/3,by1+bw+meanx*70+30
    FOR i=0 TO n
      SUB y(i),q*sin(x(i))
    NEXT i
    REPEAT
    UNTIL mousek=0
  ELSE if mousek=4
    PRINT "dipole kick"
    COLOR rot
    LINE bx1+count/3,by1+bw+meanx*70+70,bx1+count/3,by1+bw+meanx*70+30
    FOR i=0 TO n
      ADD y(i),dq
    NEXT i
    REPEAT
    UNTIL mousek=0
  ENDIF
  INC count
LOOP
QUIT
PROCEDURE init_gauss
  ' initializes phase space with 2D gaussian distribution
  LOCAL i
  FOR i=0 TO n-1
    y(i)=gasdev()/5
    x(i)=gasdev()/5
  NEXT i
RETURN
PROCEDURE init_random
  ' initializes phase space with 2D gaussian distribution
  LOCAL i
  FOR i=0 TO n-1
    y(i)=rnd()/1.4
    x(i)=rnd()/1.4
  NEXT i
RETURN
PROCEDURE init_line
  ' initializes phase space with a simple line distribution
  LOCAL i
  FOR i=0 TO n-1
    y(i)=(i+n/20)/(0.43*n+n/20)
    x(i)=0
  NEXT i
RETURN

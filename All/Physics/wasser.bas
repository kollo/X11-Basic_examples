' Simulation of a liquid
' (c) Markus Hoffmann
'
' klick mouse button to add molecules.

DIM x(10000),y(10000),dx(10000),dy(10000)
anzmol=100
bh%=380
bw%=400

FOR i=0 TO anzmol-1
  x(i)=(i MOD bw%/16)*16
  y(i)=bh%-10-(i DIV bw%/16)*16
  dx(i)=0
  dy(i)=0
NEXT i

anzmol=2

DO
  CLEARW
  TEXT 200,20,"Simulation of a liquid"
  TEXT 200,40,"klick mousebutton to add molecules"
  TEXT 600,20,STR$(anzmol)
  FOR i=0 TO anzmol-1
    CIRCLE x(i),y(i),4
  NEXT i
  VSYNC
  PAUSE 0.01
  IF MOUSEK
    x(anzmol)=mousex
    y(anzmol)=mousey
    INC anzmol
  ENDIF
  FOR i=0 TO anzmol-1
    CLR kx,ky
    FOR j=0 TO anzmol-1
      IF i<>j
        d=@dist(x(i),y(i),x(j),y(j))
        IF d>0 AND d<100
          p=@pot(d)
          ADD kx,p*(x(j)-x(i))/d
          ADD ky,p*(y(j)-y(i))/d
        ENDIF
      ENDIF
    NEXT j
    ' print kx,ky
    dx(i)=(dx(i)+kx/100)*0.92
    dy(i)=(dy(i)+ky/100)*0.92+0.1
    ADD x(i),dx(i)
    ADD y(i),dy(i)
    IF x(i)<0
      x(i)=1
      dx(i)=-0.5*dx(i)
    ELSE IF x(i)>bw%
      x(i)=bw%
      dx(i)=-0.5*dx(i)
    ENDIF
    IF y(i)<0
      y(i)=1
      dy(i)=-0.5*dy(i)
    ELSE IF y(i)>bh%
      y(i)=bh%-RANDOM(2)
      dy(i)=-0.5*dy(i)
    ENDIF
  NEXT i
LOOP
QUIT

DEFFN dist(x1,y1,x2,y2)=sqrt((x2-x1)^2+(y2-y1)^2)
DEFFN pot(dd)=-exp(-dd^2/10/10)*100+0.1

' Draws a tune diagram with resonances up to order 12
' (c) Markus Hoffmann  1998 2001-10-27
'
'

b%=200 ! size of the diagram
SIZEW ,b%+80,b%
CLEARW 1
' Prepare 16 random colors and draw the legend
DIM col(17)
FOR i=1 TO 16
  col(i)=GET_COLOR(RANDOM(65535),RANDOM(65535),RANDOM(65535))
  COLOR col(i)
  LINE b%+10,10*i,b%+40,10*i
  COLOR GET_COLOR(65535,65535,65535)
  TEXT b%+50,10*i+3,STR$(i)
NEXT i
TEXT b%+30,10*17+3,"order"
' Create and draw the diagram
FOR x%=0 TO b%-1
  FOR y%=0 TO b%-1
    qx=x%/b%
    qy=y%/b%
    FOR n%=6 DOWNTO 0
      FOR m%=6 DOWNTO 0
        ' PRINT n*qx+m*qy''
        IF n%>0 OR m%>0
          IF ABS(FRAC(n%*qx+m%*qy))<0.1/(n%+m%+1)
            COLOR col(n%+m%)
            PLOT x%,y%
            fl=1
          ENDIF
        ENDIF
        EXIT IF fl
      NEXT m%
      EXIT IF fl
    NEXT n%
    fl=FALSE
  NEXT y%
  SHOWPAGE
NEXT x%
IF NOT ANDROID?
  MOUSEEVENT
  QUIT
ELSE
  END
ENDIF

' Demonstrationsprogramm (c) Markus Hoffmann
'

' Mathematisches Pendel

l=0.98  ! L�nge in Meter
g=9.81  ! Gravitationskonstante

r=0.1   ! D�mpfung (Geschwindigkeitsproportionalit�tsfaktor)
' Maximalausschlag in Bogenma�  (-Pi bis Pi)
x=PI
' Anfangsgeschwindigkeit in m/s
x_=0
' Erregerfunktion
liy=50
ll=140  ! L�nge des Bildschirmpendelmodells
gn=5    ! Integrationsgenauigkeit (f�r gro�e Werte ruckartiger Bildaufbau)
my=220
'
rot=GET_COLOR(65530,0,0)
gelb=GET_COLOR(65530,40000,0)
grau=GET_COLOR(65530/2,65530/2,65530/2)
weiss=GET_COLOR(65530,65530,65530)
schwarz=GET_COLOR(0,0,0)
lila=GET_COLOR(65530,0,65530)
blau=GET_COLOR(10000,10000,65530)
gruen=GET_COLOR(0,30000,0)

COLOR weiss,weiss
VSYNC
GET_GEOMETRY 1,bx,by,bw,bh
PBOX bx,by,bw,bh
COLOR schwarz
DIM xx(2),xx(2),yy(2),ee(2)
LINE 0,liy+6,bw,liy+6
LINE 0,liy-1,bw,liy-1
LINE @kx(-PI),liy+10,@kx(-PI),liy-5
LINE @kx(PI),liy+10,@kx(PI),liy-5
LINE @kx(0),liy+10,@kx(0),liy-5

FOR i=0 TO 18
  LINE @kx(RAD(i*10)),liy-1,@kx(RAD(i*10)),liy-4
  TEXT 1+@kx(RAD(i*10))-LEN(STR$(i))*6/2,liy-10,STR$(i)
  LINE @kx(RAD(-i*10)),liy-1,@kx(RAD(-i*10)),liy-4
  TEXT 1+@kx(RAD(-i*10))-LEN(STR$(i))*6/2,liy-10,STR$(i)
NEXT i
BOX 9,59,21,bh-1
LINE 0,@ky(0),26,@ky(0)
FOR i=0 TO 10
  LINE 4,@ky(i),9,@ky(i)
  LINE 4,@ky(-i),9,@ky(-i)
  '  TEXT 1+@kx(RAD(i*10))-LEN(STR$(i))*6/2,liy-10,STR$(i)
  '  LINE @ky(RAD(-i*10)),liy-1,@kx(RAD(-i*10)),liy-4
  '  TEXT 1+@kx(RAD(-i*10))-LEN(STR$(i))*6/2,liy-10,STR$(i)
NEXT i
' CIRCLE bw/2,my,ll+10
DEFTEXT 1,0.07,0.14,0
COLOR gruen
titel$="Angeregtes gedaempftes mathematisches Pendel. Echtzeitsimulation"
LTEXT bw/2-ltextlen(titel$)/2,7,titel$
dt=0.01
COLOR schwarz
t=TIMER
v=1
x=0
DEFMOUSE 3
DO
  MOUSE mx,fmy,k
  IF k=1
    x=(MOUSEX-bw/2)/100
    x_=(MOUSEY-bh/2)/10
  ELSE IF k=1024
    END
  ENDIF
  @m(x)
  COLOR weiss
  LINE 0,5,200,5
  COLOR schwarz
  dt=(TIMER-t)/gn
  LINE 0,5,dt*gn*500,5
  PLOT @kx(x),@ky(x_)-20
  IF dt>0
    t=TIMER
    FOR i=1 TO gn
      @dgl2
      IF SGN(x_)<>SGN(v)
        um=t-ts
        umx=x
        um=2
        ts=t
        v=x_
      ENDIF
    NEXT i
    IF x>0
      x=((x+PI) MOD 2*PI)-PI
    ELSE
      x=-(((-x+PI) MOD 2*PI)-PI)
    ENDIF
  ENDIF
LOOP
END

PROCEDURE dgl2
  x__=-g/l*SIN(x)-r*x_+@err(t)/l   ! ged�mpftes mathematisches Pendel
  x_=x_+dt*x__           ! erste Ableitung
  x=x+x_*dt           ! Bewegungsgleichung
  t=t+dt
RETURN
PROCEDURE m(x)
  IF um
    PRINT AT(70,22);"T="'STR$(um,5)'
    PRINT AT(70,23);"E="'STR$(ABS((COS(umx))-1)*g*2*l,5)'
    DEC um
  ENDIF
  y=@ky(x_)
  e=@kx(@err(t))
  IF y<>yy(sc)
    COLOR weiss
    PBOX 10,my,20,MAX(yy(sc),60)
    COLOR blau
    PBOX 10,my,20,MAX(y,60)
    yy(sc)=y
    COLOR schwarz
  ENDIF
  IF e<>ee(sc)
    COLOR weiss
    LINE ee(sc)-3,liy,ee(sc)-3,liy+5
    LINE ee(sc)+3,liy+5,ee(sc)+3,liy
    '
    COLOR schwarz
    '
    LINE e-3,liy,e-3,liy+5
    LINE e+3,liy+5,e+3,liy
    '
    ee(sc)=e
  ENDIF
  IF x<>xx(sc)
    COLOR weiss
    LINE @kx(xx(sc))-3,liy,@kx(xx(sc))+3,liy+5
    LINE @kx(xx(sc))-3,liy+5,@kx(xx(sc))+3,liy
    '
    LINE bw/2,my,bw/2+ll*SIN(xx(sc)),my+ll*COS(xx(sc))
    PCIRCLE bw/2+ll*SIN(xx(sc)),my+ll*COS(xx(sc)),8
    COLOR schwarz
    '
    LINE @kx(x)-3,liy,@kx(x)+3,liy+5
    LINE @kx(x)-3,liy+5,@kx(x)+3,liy
    '
    LINE bw/2,my,bw/2+ll*SIN(x),my+ll*COS(x)
    PCIRCLE bw/2+ll*SIN(x),my+ll*COS(x),7
    xx(sc)=x
  ENDIF
  sc=(sc+1) MOD 2
  VSYNC
  IF abs(s-timer)<1
    PAUSE 0.02-timer+s
  ENDIF
  s=TIMER
RETURN

FUNCTION err(t)
  RETURN 0.3*SIN(0.5*SQR(g/l)*t)
ENDFUNCTION

' DEFFN err(t)=(MOUSEX-bw/2)/100   ! Hier wird mit der Maus erregt ...
' DEFFN err(t)=0
FUNCTION kx(okkx)
  RETURN bw/2+100*okkx
ENDFUNCTION
FUNCTION ky(y)
  RETURN my+10*y
ENDFUNCTION

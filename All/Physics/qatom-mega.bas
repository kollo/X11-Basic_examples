' converted from GFA-Basic to X11-Basic
CLS
PRINT "Qatom... Fuer das Atomphysikseminar Jan. 1995 Prof. Meschede"
PRINT "von Markus Hoffmann"
PRINT
PRINT "Thema: Klassische Elektronenbahnen in Rydbergatomen"
' input "Anzahl der Laeufer ? (n>3) ",n
n=20
PRINT n
DIM phi(n,n),v(n,n)
FOR i=0 TO n-1
  FOR j=0 TO n-1
    v(i,j)=i+21+j
    phi(i,j)=0
  NEXT j
NEXT i
schwarz=COLOR_RGB(0,0,0)
weiss=COLOR_RGB(1,0,0)
CIRCLE 320,200,200
BOUNDARY 0
DO
  COLOR schwarz
  PCIRCLE 320,200,199
  COLOR weiss
  FOR j=0 TO n-1
    FOR i=0 TO n-1
      ADD phi(i,j),v(i,j)*0.005
      PCIRCLE 320+COS(phi(i,j))*10*j,200+SIN(phi(i,j))*10*j,5
    NEXT i
  NEXT j
  EXIT IF MOUSEK>500
  SHOWPAGE
  PAUSE 0.02
LOOP
QUIT

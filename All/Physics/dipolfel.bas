' #################################### (c) Markus Hoffmann 1993
' ## Wie entsteht ein Dipolfeld ... ##
' ####################################

k=20
SIZEW ,k*8+8,k*8+8
CLEARW
DIM p(k+1,k+1),s(k+1,k+1)
ARRAYFILL p(),1
p(2,2)=1
DO
  p(k/2,k/2)=1
  p(k/2,k/2+1)=0
  @show
  SHOWPAGE
  @make
LOOP
END
PROCEDURE show
  FOR x=1 TO k-1
    FOR y=1 TO k-1
      IF INT(p(x,y)*32)<>INT(s(x,y)*32)
        COLOR p(x,y)*250
        PBOX x*8,y*8,x*8+7,y*8+7
      ENDIF
    NEXT y
  NEXT x
RETURN
PROCEDURE make
  FOR x=1 TO k-1
    FOR y=1 TO k-1
      s(x,y)=(p(x,y)+p(x+1,y)+p(x-1,y)+p(x,y+1)+p(x,y-1))/5
    NEXT y
  NEXT x
  SWAP s(),p()
RETURN

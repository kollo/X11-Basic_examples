' Taupunktformeln (c) Markus Hoffmann 2011
' Formeln nach:

' http://www.faqs.org/faqs/meteorology/temp-dewpoint/

' Bezeichnungen:
' r = relative Luftfeuchte
' T = Temperatur in �C
' TK = Temperatur in Kelvin (TK = T + 273.15)
' TD = Taupunkttemperatur in �C
' DD = Dampfdruck in hPa
' SDD = S�ttigungsdampfdruck in hPa

' Parameter:
' a = 7.5, b = 237.3 f�r T >= 0
' a = 7.6, b = 240.7 f�r T < 0 �ber Wasser (Taupunkt)
' a = 9.5, b = 265.5 f�r T < 0 �ber Eis (Frostpunkt)

' R* = 8314.3 J/(kmol*K) (universelle Gaskonstante)
' mw = 18.016 kg (Molekulargewicht des Wasserdampfes)
'AF = absolute Feuchte in g Wasserdampf pro m3 Luft

' Formeln:

'   SDD(T) = 6.1078 * 10^((a*T)/(b+T))
'   2. DD(r,T) = r/100 * SDD(T)
'   3. r(T,TD) = 100 * SDD(TD) / SDD(T)
'   4. TD(r,T) = b*v/(a-v) mit v(r,T) = log10(DD(r,T)/6.1078)
'   5. AF(r,TK) = 10^5 * mw/R* * DD(r,T)/TK; AF(TD,TK) = 10^5 * mw/R* * SDD(TD)/TK

a=7.5
b=237.3

T=20
r=50

sdd=6.1078*10^((a*T)/(b+T))
PRINT "Temperatur: ";T;"� Celsius"
PRINT "rel. Feuchte: ";r;"%"
PRINT "Saettingungsdampfdruck Wasser: ";sdd;" hPa."

dd=r/100*sdd

PRINT "Dampfdruck Wasser: ";dd;" hPa."

v=log10(dd/6.1078)
td=b*v/(a-v)

PRINT "Taupunkt-T=: ";td;" C."

' dd=6.1078*10^((a*Td)/(b+Td))

' Luftdruck
P=1013.25
' virtual Temperature
tv=(T+273.15)/(1-0.379*dd/p)
PRINT "Virtuelle Temperatur bei ";p;" hPa: ";tv;" K (";(tv-273.15);" C."

W=0.62197*dd/(p-dd)

PRINT "Wassergehalt/trockener Luft: ";w*1000;" g/kg"

' wet bulb (K"uhlgrenztemperatur)

gamma=0.00066*p*0.1
delta=4098*dd*0.1/(td+237.3)^2
twb=(gamma*T+delta*td)/(gamma+delta)
PRINT "wet bulb temperature: ";twb;" C."

af:
mw=18.016
Rs=8314.3

TK=T+273.15

af=1e5*mw/Rs*dd/TK

PRINT af,"g Wasserdampf pro m3 Luft"
PRINT
T_neu=18
TK_NEU=T_neu+273.15

dd_neu=af*TK_NEU*Rs/mw/1e5

sdd_neu=6.1078*10^((a*T_neu)/(b+T_neu))

r_neu=100*dd_neu/sdd_neu

'r=100*10^((a*Td)/(b+Td))/(10^((a*T)/(b+T)))
PRINT r_neu
QUIT

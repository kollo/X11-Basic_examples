DIM px(10),py(10)
bx=200
by=200
SIZEW ,bx,by
anzpunkt=3

lambda=50

px(0)=90
py(0)=100
px(1)=130
py(1)=100
px(2)=110
py(2)=130
rot=GET_COLOR(65535,0,0)
weiss=GET_COLOR(65535,65535,65535)
gelb=GET_COLOR(65535/2,65535/2,0)
DIM feld(bx,by)
ARRAYFILL feld(),-1
counter=0
DO
  WHILE feld(x,y)>=0
    x=RANDOM(bx)
    y=RANDOM(by)
  WEND

  'for x=0 to bx
  '  for y=0 to by
  CLR sum
  FOR i=0 TO anzpunkt-1
    ADD sum,cos(2*pi*(sqrt((px(i)-x)^2+(py(i)-y)^2)/lambda))
  NEXT i

  sum=sum/anzpunkt
  ' if sum^2<0.1
  ' color weiss
  ' else if sum^2<0.4
  ' color gelb
  ' else
  ' color rot
  ' endif
  feld(x,y)=sum^2
  COLOR int(sum^2*20)*10
  PLOT x,y
  '  next y
  INC counter
  IF timer-t>5
    VSYNC
    t=timer
    PRINT str$(counter/bx/by*100,3,3)
  ENDIF
LOOP
VSYNC
'next z

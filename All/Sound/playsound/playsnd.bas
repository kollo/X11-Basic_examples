' Plays ATARI ST XBIOS(32) Sound files (.X32) on the speaker
'
' (c) Markus Hoffmann 2003
' adapted to modified sound command 2011
'
' do a:
' xbasic playsnd.bas popcorn.snd

speed=1

WAVE 1,2
WAVE 2,2
WAVE 3,2

bin=1/40/max(speed,4)
name$=PARAM$(2)
IF exist(name$)
  OPEN "I",#1,name$
  b$=input$(#1,lof(#1))
  CLOSE #1
  kammertona1=440
  PRINT name$
  FOR i=0 TO LEN(b$) STEP 2
    b=PEEK(VARPTR(b$)+i) and 255
    ' print i,b
    IF b=0
      b2=PEEK(VARPTR(b$)+i+1) and 255
      pt1=(pt1 AND -256) or b2
      IF pt1>0
        SOUND 1,140000/pt1
        ' pause bin
        ' sub b2,bin
      ELSE
        SOUND 1,0
      ENDIF
    ELSE if b=1
      b2=PEEK(VARPTR(b$)+i+1) and 255
      pt1=(pt1 AND (-256*256+255)) or b2*256
      IF pt1>0
        SOUND 1,140000/pt1
        ' pause bin
        ' sub b2,bin
      ELSE
        SOUND 1,0
      ENDIF
    ELSE if b=2
      b2=PEEK(VARPTR(b$)+i+1) and 255
      pt2=(pt2 AND -256) or b2
      IF pt2>0
        SOUND 2,140000/pt2
        '  pause bin
        '  sub b2,bin
      ELSE
        SOUND 2,0
      ENDIF
    ELSE if b=3
      b2=PEEK(VARPTR(b$)+i+1) and 255
      pt2=(pt2 AND (-256*256+255)) or b2*256
      IF pt2>0
        SOUND 2,140000/pt2
        '  pause bin
        '  sub b2,bin
      ELSE
        SOUND 2,0
      ENDIF

    ELSE if b=4
      b2=PEEK(VARPTR(b$)+i+1) and 255
      pt3=(pt3 AND -256) or b2
      IF pt3>0
        SOUND 3,140000/pt3
      ELSE
        SOUND 3,0
      ENDIF
    ELSE if b=5
      b2=PEEK(VARPTR(b$)+i+1) and 255
      pt3=(pt3 AND (-256*256+255)) or b2*256
      IF pt3>0
        SOUND 3,140000/pt3
      ELSE
        SOUND 3,0
      ENDIF
    ELSE if b=7
      b2=PEEK(VARPTR(b$)+i+1) and 255
      PRINT "TONKAN�LE:"'BIN$(b2,8)''
      se1=BTST(b2,0)
      se2=BTST(b2,1)
      se3=BTST(b2,2)
    ELSE if b=8
      lt1=PEEK(VARPTR(b$)+i+1) and 255
      SOUND 1,,lt1/15
    ELSE if b=9
      lt2=PEEK(VARPTR(b$)+i+1) and 255
      SOUND 2,,lt2/15
    ELSE if b=10
      lt3=PEEK(VARPTR(b$)+i+1) and 255
      SOUND 3,,lt3/15
    ELSE if b=11 OR b=12 OR b=13 OR b>=8*16
      b2=PEEK(VARPTR(b$)+i+1) and 255
      PRINT str$(i,5,5);": ";STR$(pt1,5,5);" ";STR$(pt2,5,5);" ";,pt3,b2,lt1,lt2,lt3

      PAUSE b2/50
    ELSE
      b2=PEEK(VARPTR(b$)+i+1) and 255
    ENDIF
  NEXT i
ENDIF
QUIT

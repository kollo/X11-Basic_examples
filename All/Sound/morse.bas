' Program outputs morse codes via sound card.
' (c) Markus Hoffmann        V.1.00       1990 

DIM m$(256)
v=3
freq=600    ! Hz Tone

WAVE 2,1,0.01,0.1,0.8,0.1 ! set a nice sound

SOUND 2,freq,1,0.3
CLS
PRINT AT(1,1);CHR$(27);"p     Morsetrainer        (c) Markus Hoffmann        V.1.00       1990           ";CHR$(27);"q";
WAVE 1,3,0.01,0.1,0.8,0.1 ! set a nice sound
SOUND 1,0,1
PAUSE 10/50
FOR i=ASC("A") TO ASC("Z")
  READ a$
  m$(i)=a$
NEXT i
READ a$,a$,a$
'READ m$(ASC("�"))
'READ m$(ASC("�"))
'READ m$(ASC("�"))
FOR i=ASC("1") TO ASC("9")
  READ a$
  m$(i)=a$
NEXT i
READ m$(ASC("0"))
PRINT
PRINT
@morsstr("Please select a file to read, Bitte File auswaehlen.")
FILESELECT ,"./*.txt","",f$
IF EXIST(f$)
  OPEN "I",#1,f$
  WHILE NOT EOF(#1)
    LINEINPUT #1,t$
    @morsstr(TRIM$(t$))
    PRINT
    PAUSE (4*v+4)/50
  WEND
  CLOSE
ELSE
  t$=f$+" konnte nicht gefunden werden..."
  @morsstr(t$)
ENDIF
END
'
'
'   Teil f�r   I-N-T �bung
'
'
DO
  IF c=5
    PRINT " PP ";
    PAUSE 5*v/50
    INC f
    CLR c
  ENDIF
  ' EXIT IF f=2
  INC c
  @m(RANDOM(3))
  IF INP?(2)
    i|=INP(2)
    IF i|=ASC("+")
      INC v
    ELSE IF i|=ASC("-")
      DEC v
    ELSE
      END
    ENDIF
    PRINT "V=";v
  ENDIF
  PAUSE 2*v/50
LOOP
PROCEDURE m(n)
  ' PRINT n
  SELECT n
  CASE 0
    PRINT "I ";
    @k
    PAUSE 1*v/50
    @k
  CASE 1
    PRINT "N ";
    @l
    PAUSE 1*v/50
    @k
  CASE 2
    PRINT "T ";
    @l
  ENDSELECT
  PAUSE 2*v/50
RETURN
'
'
' ----------------------
'
'
PROCEDURE morsstr(t$)
  t$=UPPER$(t$)
  FOR u=1 TO LEN(t$)
    IF INP?(-2)
      i=INP(-2)
      IF i=ASC("+")
        INC v
      ELSE IF i=ASC("-")
        DEC v
      ELSE
        CLOSE
        QUIT
      ENDIF
      PRINT "V=";v
    ENDIF
    @morse(ASC(MID$(t$,u,1)))
  NEXT u
RETURN
PROCEDURE morse(a)
  IF LEN(m$(a))
    PRINT chr$(a);
    FLUSH
    FOR i=1 TO LEN(m$(a))
      IF MID$(m$(a),i,1)="-"
        @l
      ELSE
        @k
      ENDIF
      PAUSE 1*v/50
    NEXT i
  ELSE
    PRINT " ";
    PAUSE 4*v/50
  ENDIF
  PAUSE 3*v/50
RETURN
PROCEDURE l
  SOUND 1,freq
  PAUSE 3*v/50
  SOUND 1,0
RETURN
PROCEDURE k
  SOUND 1,freq
  PAUSE 1*v/50
  SOUND 1,0
RETURN
m_data:
' a-z
DATA .-,-...,-.-.,-..,.,..-.,--.,....,..,.---,-.-,.-..,--,-.,---,.--.,--.-,.-.,...,-,..-,...-,.--,-..-,-.--,--..
' � � �
DATA .-.-,---.,..--
' 1-0
DATA .----,..---,...--,....-,.....,-....,--...,---..,----.,-----

' tonedial (c) Markus Hoffmann 1990 
' (original Version GFA-Basic)
'
' produces sounds with which one can dial the phone. 
'
PRINT "Freizeichen..."

WAVE 1,1 ! sin waves
WAVE 2,1 ! sin waves
WAVE 3,1 ! sin waves

FOR iu=0 TO 12
  SOUND 3,440,1
  PAUSE 10/50
  SOUND 3,0
  PAUSE 14/50
NEXT iu
' WAVE 4
PAUSE 50/50
@init

@dial("0202623560")
PAUSE 1
@dial("04039198968")
PAUSE 1
@dial("0228632170")

END

PROCEDURE init
  LOCAL f1,f2
  DIM d1u(13)
  DIM d2u(13)
  FOR iu=1 TO 12
    READ f1,f2
    d1u(iu)=f1
    d2u(iu)=f2
  NEXT iu
  d1u(0)=d1u(11)
  d2u(0)=d2u(11)
RETURN

PROCEDURE dial(t$)
  PRINT "dialing: "+t$
  FOR tu=0 TO LEN(t$)-1
    @tone(VAL(CHR$(PEEK(varptr(t$)+tu))))
    PAUSE 2/50
  NEXT tu
RETURN
PROCEDURE tone(iu)
  SOUND 1,d1u(iu),1
  SOUND 2,d2u(iu),1
  PAUSE 7/50
  SOUND 1,0
  SOUND 2,0
RETURN
'
'
DATA 1209,697
DATA 1336,697
DATA 1477,697
DATA 1209,770
DATA 1336,770
DATA 1477,770
DATA 1209,852
DATA 1336,852
DATA 1477,852
DATA 1209,941
DATA 1336,941
DATA 1477,941

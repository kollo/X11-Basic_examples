' This program decodes a monochrome picture (32000 Bytes) *.pic from
' ATARI ST and displays it. The content is animated.
'
' demonstrates SCOPE
'
'  zeigt ein ATARI-ST Monochrom-Bild an (32000 Bytes)
'  V. 1.08 (c) Markus Hoffmann   2002-05-06
'
'
DIM a(18000),b(18000)
ARRAYFILL a(),0      ! Array with x coordinates of all black pixels
ARRAYFILL b(),0      ! Array with y coordinates of all black pixels
CLEARW
GPRINT "shows ATARI ST monochrome pictures (32000 Bytes)"
FILESELECT "Select picture file ...","pictures/*.pic","f1.pic",f$
IF EXIST(f$)
  OPEN "I",#1,f$
  CLR x,y,count
  WHILE y<400
    a=INP(#1)
    IF a
      FOR i=0 TO 7
        IF BTST(a,i) ! Black pixel found
          ' plot x-i+8,y
          a(count)=x-i+8 ! store it in the arrays
          b(count)=y
          INC count
        ENDIF
      NEXT i
      IF (count MOD 1000)<5 AND count>2000
        @showit
      ENDIF
    ENDIF
    ADD x,8
    IF x>=640
      SUB x,640
      INC y
      PRINT y,count
    ENDIF
  WEND
  @showit
  CLOSE #1
  ALERT 0,"Fertig !",1," OK ",balert
ENDIF
QUIT
PROCEDURE showit
  FOR t=1 TO 15            ! 15 animations
    PBOX 0,0,640,400
    COLOR COLOR_RGB(1,1,1/6)
    SCOPE b(),a(),1,t/10,200-200*t/10,t/10,320-320*t/10
    COLOR COLOR_RGB(1,0,1)
    SCOPE a(),b(),1,t/10,,t/10
    SHOWPAGE
    PAUSE 0.02
    COLOR COLOR_RGB(0,0,1/6)
  NEXT t
RETURN

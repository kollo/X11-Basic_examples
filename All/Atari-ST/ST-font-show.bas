' Displays Atari-ST 8*16 fixed Fonts (c) Markus Hoffmann 2004-04-12
'
scale=2 ! define the size of the characters
text$="This example demonstrates the use of the old Atari-ST fonts!"+CHR$(14)+CHR$(15)+" We used "
COLOR COLOR_RGB(0.1,0,0)
CLEARW 1
GET_GEOMETRY 1,bx%,by%,bw%,bh%
PBOX bx%,by%,bw%,200
COLOR COLOR_RGB(1,1,0)

FILESELECT "load FONT","./*.fnt","",f$
IF LEN(f$)
  IF EXIST(f$)
    OPEN "I",#1,f$
    text$=text$+f$
    f$=INPUT$(#1,4096)
    CLOSE #1
    FOR i=0 TO 256
      text$=text$+CHR$(i)
    NEXT i
  ENDIF
  @text(text$)
ENDIF
END
PROCEDURE text(t$)
  LOCAL i
  FOR i=0 TO LEN(t$)-1
    char=PEEK(VARPTR(t$)+i) AND 255
    @char(x,y,char)
    SHOWPAGE
    PAUSE 0.1
    ADD x,8*scale
    IF x>=bw% OR char=31 OR x>=8*scale*32
      x=0
      ADD y,16*scale
    ENDIF
  NEXT i
RETURN
PROCEDURE char(x,y,c)
  LOCAL i,j
  FOR i=0 TO 15
    FOR j=0 TO 7
      IF BTST(PEEK(VARPTR(f$)+c+i*256),7-j)=0
        PBOX x+j*scale,y+i*scale,x+j*scale+scale-1,y+i*scale+scale-1
      ENDIF
    NEXT j
  NEXT i
RETURN

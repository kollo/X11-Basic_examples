DIM enable(220)
DIM herab(220)
DIM transfer(10)
DIM rt(220)
DIM vt(220)

ntransfer=3
sollstrom=50
ARRAYFILL enable(),0
FOR j=0 TO 3*6-1
  FOR i=0 TO 9
    enable(i+j*11)=1
    herab(i+j*11)=0
  NEXT i
  enable(i+j*11)=1
NEXT j
SIZEW ,660,400
schwarz=GET_COLOR(0,0,0)
weiss=GET_COLOR(65535,65535,65535)
rot=GET_COLOR(65535,0,0)
gelb=GET_COLOR(65535,65535,0)
blau=GET_COLOR(0,0,65535)

FOR i=0 TO 28*3-1
  herab(i)=50
NEXT i

DO
  @display
  k=RANDOM(32)+20
  FOR i=0 TO ntransfer-1
    transfer(i)=k
  NEXT i

  @injection
  IF ziel=-1
    PRINT "Mit dieser Petra-Fuellung ist durch Topping up"
    PRINT "keine Verbesserung mehr zu erzielen."
    PRINT "Versuchen Sie kleinere Bunchstroeme in PETRA."
    PAUSE 100
    QUIT
  ELSE
    FOR i=ziel TO ziel+ntransfer-1
      herab(i)=herab(i)+transfer(i-ziel)
    NEXT i
  ENDIF
LOOP

PAUSE 100
QUIT

PROCEDURE test(n)
  LOCAL i,sx,sy,ss,rms
  herat()=herab()
  FOR i=n TO n+ntransfer-1
    herat(i)=herat(i)+transfer(i-n)
  NEXT i
  sx=0
  sy=0
  ss=0
  rms=0
  FOR i=0 TO 219
    ADD sx,cos(i/220*2*pi)*herat(i)
    ADD sy,sin(i/220*2*pi)*herat(i)
    ADD rms,(herat(i)-sollstrom)^2
    ADD ss,herat(i)
  NEXT i
  rms=rms/220
  IF ss>0
    sx=sx/ss
    sy=sy/ss
  ENDIF
  testrms=rms
  testvec=sqrt(sx*sx+sy*sy)
RETURN
PROCEDURE test2(n)
  LOCAL i,j,sx,sy,ss,rms,trms,nb
  herat()=herab()
  FOR i=n TO n+ntransfer-1
    herat(i)=herat(i)+transfer(i-n)
  NEXT i
  sx=0
  sy=0
  ss=0
  rms=0
  trms=0
  nb=0
  FOR i=0 TO 219
    ADD ss,herat(i)
    IF enable(i)
      ADD trms,(herat(i)-sollstrom)^2
      INC nb
    ENDIF
  NEXT i
  FOR i=0 TO 219
    IF enable(i)
      ADD sx,herat(i)
      ADD rms,(sx-ss/nb*i)^2
    ENDIF
  NEXT i
  trms=trms/nb
  rms=rms/nb
  testvec=rms
  testrms=trms
RETURN
PROCEDURE test3(n)
  LOCAL i,j,sx,sy,ss,rmsx,rmsy
  herat()=herab()
  FOR i=n TO n+ntransfer-1
    herat(i)=herat(i)+transfer(i-n)
  NEXT i
  sx=0
  sy=0
  ss=0
  rmsy=0
  rmsx=0
  FOR i=0 TO 219
    ADD ss,herat(i)
  NEXT i
  FOR i=0 TO 219
    ADD sx,herat(i)
    ADD sy,herat((i+110) MOD 220)
    ADD rmsx,(sx-ss/220*i)^2
    ADD rmsy,(sy-ss/220*i)^2
  NEXT i
  PRINT rmsx,rmsy
  testvec=sqrt(rmsx*rmsx+rmsy*rmsy)/220
  testrms=0
RETURN

PROCEDURE injection
  nbunch=0
  sx=0
  sy=0
  ss=0
  rms=0
  FOR i=0 TO 219
    IF enable(i)
      ADD sx,cos(i/220*2*pi)*herab(i)
      ADD sy,sin(i/220*2*pi)*herab(i)
      ADD rms,(herab(i)-sollstrom)^2
      ADD ss,herab(i)
      INC nbunch
    ENDIF
  NEXT i
  rms=rms/nbunch
  IF ss>0
    sx=sx/ss
    sy=sy/ss
    ' print "ss=";int(100*sqrt(sx*sx+sy*sy))

    testvecmin=100000000
    kmin=-1
    testrmsmax=0
    FOR k=0 TO 220-1-ntransfer STEP ntransfer
      IF enable(k)
        @test2(k)
        IF testrms<rms
          COLOR gelb
          PCIRCLE k*3,250+testvec/100,3
          COLOR blau
          PCIRCLE k*3,100*(sqrt(rms)-sqrt(testrms)),2
          IF testvec<testvecmin
            testvecmin=testvec
            kmin=k
            testrmsmin=testrms
          ENDIF
          IF testrmsmax<sqrt(rms)-sqrt(testrms)
            testrmsmax=sqrt(rms)-sqrt(testrms)
          ENDIF
          VSYNC
        ENDIF
      ENDIF
    NEXT k
    ziel=kmin
  ELSE
    ziel=0
  ENDIF
  PRINT "Ziel=";ziel,"RMS=";sqrt(testrmsmin);"(";sqrt(rms);")","IST:";STR$(sqrt(rms)-sqrt(testrmsmin),5,5);" MAX:";STR$(sqrt(testrmsmax),5,5)
RETURN
PROCEDURE display
  COLOR schwarz
  PBOX 0,0,660,400
  sx=0
  sy=0
  ss=0
  COLOR weiss
  LINE 0,200-sollstrom,660,200-sollstrom,
  FOR i=0 TO 219
    ' print i,herab(i)
    PBOX i*3,200-herab(i),i*3+2,200
    PCIRCLE 500+100*cos(i/220*2*pi),300-100*sin(i/220*2*pi),sqrt(herab(i)/4)
    ADD sx,cos(i/220*2*pi)*herab(i)
    ADD sy,sin(i/220*2*pi)*herab(i)
    ADD ss,herab(i)
    IF enable(i)
      COLOR rot
      PCIRCLE i*3,203,2
      COLOR weiss
    ENDIF
  NEXT i
  LINE 500,297,500,303
  LINE 497,300,503,300
  COLOR rot
  IF ss>0
    sx=sx/ss
    sy=sy/ss
    PCIRCLE 500+100*sx,300-100*sy,2
  ENDIF
  VSYNC
RETURN

' Demonstration DOOCS server. Run this with doocsxbasic.
'  (c) Markus Hoffmann Aug. 2007
'
DIM array(100)

ttt$()=["Mon","Din","Mit"]

a=1.111
b=2.234
information$="Herzlichen Glueckwunsch"

DOOCSEXPORT a,b,cmd,status,status$,information$,array(),ttt$()

DOOCSCALLBACK a,mycall1,mycall2
' doocscallback b,mycall3

DOOCSSERVER "TESTSERVER"

FOR i=0 TO 100
  array(i)=i
NEXT i
t=timer
DO
  PRINT time$,a,b,(a-b)
  EXIT if a=0.5
  EXIT if a>800
  PAUSE 0.2
  IF timer-t>1
    t=timer
    a=0
    b=a
  ENDIF
  IF a<>b
    status$="Differenz !"
  ELSE
    status$="OK"
  ENDIF
LOOP

QUIT

PROCEDURE mycall1
  PRINT "Write-Callback auf a",a
RETURN
PROCEDURE mycall2
  ' print "Read-Callback auf a",a
  INC a
RETURN
PROCEDURE mycall3
  PRINT "Write-Callback auf b",b,a
RETURN

schwarz=GET_COLOR(0,0,0)
weiss=GET_COLOR(65535,65535,0)
DO
  PRINT "Wert: ";doocsget("TEST.DOOCS/LOCALHOST_8889/TRIGFUNCTION/SIN.AMPL")
  PRINT "String: <";doocsget$("TEST.DOOCS/LOCALHOST_8889/TRIGFUNCTION/SIN.AMPL");">"

  a()=doocsget("TEST.DOOCS/LOCALHOST_8889/TRIGFUNCTION/SIN.TD")

  b()=a(:,1)
  COLOR schwarz
  PBOX 0,0,640,400

  COLOR weiss
  SCOPE b(),1,10,200

  PRINT "Typ: ";doocstyp("TEST.DOOCS/LOCALHOST_8889/TRIGFUNCTION/SIN.AMPL")
  PRINT "Len: ";doocssize("TEST.DOOCS/LOCALHOST_8889/TRIGFUNCTION/SIN.AMPL")
  PRINT "Info: ";doocsinfo$("TEST.DOOCS/LOCALHOST_8889/TRIGFUNCTION/SIN.AMPL")
  PRINT "Info: ";doocsinfo$("TEST.DOOCS/LOCALHOST_8889/TRIGFUNCTION/SIN.TD")

  tim=doocstimestamp("TEST.DOOCS/LOCALHOST_8889/TRIGFUNCTION/SIN.AMPL")
  TEXT 10,10,"Timestamp: "+STR$(tim)+" "+unixtime$(tim)+" "+unixdate$(tim)

  DOOCSPUT "TEST.DOOCS/LOCALHOST_8889/TRIGFUNCTION/SIN.AMPL",2.145
  SHOWPAGE
  PAUSE 0.1
LOOP
QUIT

' So holt man daten aus dem HERA Archiv
' (c) Markus Hoffmann    Letzte bearbeitung am 11.11.2002
'
' Es werden nur Daten genommen bei denen Die Hera-Energie 27.5 GeV ist.

' Filenamen des Output-Files:
outputfile$="udata.dat"
anzpar=32      ! Max. Anzahl der zu holenden Parameter
DIM p$(anzpar)
anzpar=0

' Hier bitte start und stop-Zeit eingeben
' Unix-Zeit (= Sekunden ab 01.01.1970 00:00)

startperiod=1037090000-60*60*24*4 ! Start des Zeitfensters
endperiod=1037090000

DO
  READ t$
  p$(anzpar)=t$
  EXIT if t$="***"
  INC anzpar
LOOP

' Parameterliste: Diese Parameter werden dann geholt und auf
' gemeinsame Zeitbasis getrimmt.
' Das Ende der Liste wird mit "***" gekennzeichnet.
' Die Syntax der Parameter ist:
' Server/Arrayindex[Parname]

DATA "HISTORY/#0[HEDCCur]"
DATA "HISTORY/#0[HPDCCur]"

DATA "HISTORY/#248[HEOrbitX]"
DATA "HISTORY/#248[HEOrbitY]"
DATA "HISTORY/#247[HEOrbitX]"
DATA "HISTORY/#247[HEOrbitY]"

DATA "HISTORY/#195[HEVACS]"
DATA "***"

vmask=0x1fffff           ! Maskiert die Parameter die nicht fehlen duerfen

DIM advance(anzpar),lastvalue(anzpar),writevalue(anzpar),anzen(anzpar)
DIM idata(anzpar,16000)
DIM itime(anzpar,16000)

ARRAYFILL anzen(),0

maxper=60*60*24          ! 1 Tag in Sekunden
days=INT((endperiod-startperiod)/maxper)
PRINT days;" Tage."
gotdays=0

'  gotdays gibt einen Offset an, ab wann er die Daten holen soll

IF gotdays=0
  OPEN "O",#2,outputfile$
  PRINT #2,"% Untergrundstudien von Markus Hoffmann. Daten vom "+date$+" "+time$
  PRINT #2,"% Zeitfenster vom ";startperiod;" bis ";endperiod;" =";days;" Tage."
  PRINT #2,"% 1 Run #"
  PRINT #2,"% 2 i"
  PRINT #2,"% 3 imax"
  PRINT #2,"% 4 UNIX timestamp"
  FOR i=0 TO anzpar-1
    PRINT p$(i);"[";anzen(i);"]"
    PRINT #2,"% ";i+5;": ";p$(i);"[";anzen(i);"]"
  NEXT i
  CLOSE #2
ELSE
  OPEN "A",#1,outputfile$
  PRINT #1,"% Weitergefuehrt ab dem ";gotdays;". Tag am "+date$+" "+time$
  CLOSE #1
ENDIF
runnr=0

' Hier erstmal die Energie holen (tageweise) und die Runs bestimmen

FOR i=gotdays TO days-1
  a()=tinehistory("HISTORY/#0[HEMAGEN]",startperiod+i*maxper,startperiod+(i+1)*maxper)
  anz=dim?(a())/2
  PRINT anz
  FOR j=0 TO anz-1
    IF startfenster=0
      IF a(j,0)>27.4 AND a(j,0)<27.7
        startfenster=a(j,1)
        stopfenster=0
      ENDIF
    ELSE if stopfenster=0
      IF a(j,0)<27.4 OR a(j,0)>27.7
        stopfenster=a(j,1)
        m=INT((stopfenster-startfenster)/60)
        IF m>10
          tag=i
          ' die ersten 10 Minuten weglassen:
          ADD startfenster,10*60
          PRINT tag;". Tag: ";startfenster;" bis ";stopfenster,m;" Min."
          @getdata(startfenster,stopfenster)
        ENDIF
        startfenster=0
      ENDIF
    ENDIF
  NEXT j
  IF i>5
    gotdays=i-1
  ENDIF
NEXT i
gotdays=days-1

QUIT

' Diese Routine holt nun die Daten !

PROCEDURE getdata(a,o)
  LOCAL j,i,anz,otime
  ARRAYFILL anzen(),0
  ARRAYFILL itime(),0
  ARRAYFILL advance(),0
  ARRAYFILL lastvalue(),1e-25
  CLR imax,scip,count
  PRINT "collect: [";
  FOR j=0 TO anzpar-1
    b()=tinehistory(p$(j),a,o)
    anzen(j)=dim?(b())/2
    count=MAX(count,anzen(j))
    FOR i=0 TO anzen(j)-1
      itime(j,i)=b(i,1)
      idata(j,i)=b(i,0)
    NEXT i
    PRINT ".";
    FLUSH
  NEXT j
  PRINT "]=";count

  OPEN "A",#2,outputfile$
  m=(o-a)/60
  h=INT(m/60)
  m=m mod 60
  PRINT #2,"% Periode vom ";a;" bis ";o;" ";tag;". Tag. ";h;"h";STR$(m,2,2,1)
  CLR i,scip,oimax,fxcount

  WHILE imax<count
    IF imax-oimax>int(count/100)
      @progress(count,imax)
      oimax=imax
    ENDIF
    otime=1e20
    FOR j=0 TO anzpar-1
      IF anzen(j)
        IF itime(j,advance(j))<otime AND advance(j)<anzen(j)
          otime=itime(j,advance(j))
        ENDIF
      ENDIF
    NEXT j
    CLR unvollstaendig,changed
    FOR j=0 TO anzpar-1
      IF itime(j,advance(j))=otime
        writevalue(j)=idata(j,advance(j))
        IF writevalue(j)<>lastvalue(j)
          changed=bset(changed,j)
          lastvalue(j)=writevalue(j)
        ENDIF
        WHILE itime(j,advance(j))=otime AND advance(j)<anzen(j)
          advance(j)=advance(j)+1
        WEND
      ELSE
        writevalue(j)=lastvalue(j)
      ENDIF
      IF writevalue(j)=1e-25 OR abs(writevalue(j))>1e30
        unvollstaendig=bset(unvollstaendig,j)
      ENDIF
      imax=MAX(imax,advance(j))
    NEXT j
    IF (unvollstaendig AND vmask)=0 AND changed<>0
      PRINT #2,runnr;" ";i;" ";imax;" ";otime;" ";
      FOR j=0 TO anzpar-1
        PRINT #2,writevalue(j);" ";
      NEXT j
      PRINT #2
    ELSE
      INC scip
    ENDIF
    INC i
  WEND
  @progress(count,imax)
  PRINT " Skipped "+STR$(scip/i*100,3,3);"%  ";
  IF scip/i=1
    PRINT bin$(unvollstaendig,anzpar)
  ELSE
    PRINT
    INC runnr
  ENDIF
  CLOSE #2
RETURN
PROCEDURE progress(a,b)
  LOCAL t$
  PRINT chr$(13);"[";STRING$(b/a*32,"-");">";STRING$((1.03-b/a)*32,"-");"| ";STR$(INT(b/a*100),3,3);"% ]";
  FLUSH
RETURN


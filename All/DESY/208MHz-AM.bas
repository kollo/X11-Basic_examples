anzchannel=4
th=110
bw=900
hellblau=GET_COLOR(65535/2,65535,65535)
schwarz=GET_COLOR(0,0,0)
weiss=GET_COLOR(65535,65535,65535)
gelb=GET_COLOR(65535,65535,0)
orange=GET_COLOR(65535,65535/2,0)
gruen=GET_COLOR(0,65535,0)
rot=GET_COLOR(65535,0,0)
pink=GET_COLOR(65535,0,65535)
grau=GET_COLOR(65535/2,65535/2,65535/2)
grau2=GET_COLOR(65535/3,65535/3,65535/3)
status$()=["OK","?","BUSY","STOP","WARNING","ERROR"]
statusc()=[gruen,gelb,gelb,rot,pink,rot,weiss]
sysname$()=["Sys. A I","Sys. A Q","Sys. B I","Sys. B Q","Sys. C I","Sys. C Q","Sys. D I","Sys. D Q"]
sysname2$()=["Sys. A Amp","Sys. A Phas","Sys. B Amp","Sys. B Phas","Sys. C Amp","Sys. C Phas","Sys. D Amp","Sys. D Phas"]
DIM sysiq(8,219),sysap(8,219)
DIM singlestat(8),singlebits(8)
DIM sym$(8)
sym$(0)=inline$("$$$$$$$$$$&$$($%D$'D$\$$H$7D$\$$$$$$$$$$$$$$") ! Anker
sym$(0)=inline$("T$0T'$D4%&$&4'-06=,UG,*%D<(662-+%&$,('$0T$0$") ! Smiley
sym$(1)=inline$("$$$$$$$$$$$$$2$C,%$4(3TccKcJ=UD<$$$$$$$$$$$$") ! Car
sym$(1)=inline$("\$3T%WD2*$T<'$$0$$\$%\$'D$*$$4$$D$+$$`$'D$($") ! ?
sym$(2)=inline$("($$43$%*<(1G,=E5Z&MD%_DVW'b*%H-^,EQ6>VTL$$$$") ! Busybee
sym$(3)=inline$("$$$$$$$$$$&$$<$%D$+D%cDCK'J$$<$%D$*$$B$+$$$$") ! Airport
sym$(3)=inline$("\$@4&$D4%&$&43JV5A9)Q9J98=91HD-$%&$,(%$,\$@$") ! STOP
sym$(4)=inline$("9%%8*)4<9%U8+)4<C%D4(%$42%$\('D42%$\('D4$$$$") ! Restaurant
sym$(4)=inline$("T$+$$6$',$04%M$*X$[,'<D<E%D(0,4TTJ*&<$+$cc`$") ! Baustelle
sym$(5)=inline$("D$&$$<$'D$B$$,$$D$'$$8$%T$($$$$$$$$$$$$$$$$$") ! Flag
sym$(5)=inline$("<$&8$$$%(D.,'\$3\'cTCc%cb3c\ac%[`+SD3`$C$$@$") ! Bombe
sym$(6)=inline$("$$&$$D$%D$2$$H$(4$8D&>$/`%`$$$$$$$$$$$$$$$$$") ! Camp
sym$(7)=inline$("$$$$$$$$0$&Tc_>$QL&ZD3cccccK]UD<*%D$$$$$$$$$") ! Train
sym$(8)=inline$("$$$$$$$$b%`,(,D5F%+L)bD;F%*,(4D4b%`$$$$$$$$$") ! Help
sym$(8)=inline$("$$'$$W$0'''ccTD4&%$,(,D5F%+\+T$$$$$$$$$$$$$$") ! Haus
sym$(8)=inline$("$$$$$$$$$$$$$$$$$$'$$@$%T$($$$$$$$$$$$$$$$$$") ! Point
sym$(8)=inline$("$$$$$$$$\$(D$6$'\$;D'B$1\$[D'B$)\$($$$$$$$$$") ! Tanke
sym$(8)=inline$("$$$$$3$3*%E<*P$'<$>T'?$1@$_T'T$$$$$$$$$$$$$$") ! Telefon

SIZEW 1,bw,anzchannel*th+100
TITLEW 1,"HERA-p 208 MHz Amplitudenmodulation"
INFOW 1,"HERA-p 208 MHz AM"
@desktop_init

SETFONT "-*-helvetica-medium-r-*-*-34-*"
'color grau
'pbox 0,0,bw,anzchannel*th+100
COLOR weiss
TEXT 10,40,"HERA-p 208 MHz Amplitudenmodulation"
DO
  IF timer-timert>5
    @redraw
    timert=timer
  ELSE
    PAUSE 0.2
  ENDIF
  VSYNC
  @mausfrage
LOOP

TINEPUT "LMBDAC8/#0[FUNCTION]",a()

QUIT

PROCEDURE redraw
  LOCAL i,j
  FOR i=0 TO anzchannel-1
    @showit(i,10,50+th*i)
    @mausfrage
  NEXT i
  FOR i=0 TO INT(anzchannel/2)-1
    FOR j=0 TO 220-1
      sysap(2*i,j)=MIN(sqrt(sysiq(2*i,j)^2+sysiq(2*i+1,j)^2),1.5)
      sysap(2*i+1,j)=atan2(sysiq(2*i,j),sysiq(2*i+1,j))
    NEXT j
    @showap(i,350,50+th*i*2)
  NEXT i
  @show_sysstatus
RETURN

PROCEDURE show_sysstatus
  LOCAL x,y
  x=10
  y=60+th*anzchannel
  COLOR schwarz
  PBOX x,y,10+20,20+y
  PBOX x+30,y,40+160,20+y
  ' color weiss
  ' box 10,50+th*anzchannel,10+20,70+th*anzchannel
  ' box 40,50+th*anzchannel,40+160,70+th*anzchannel
  sysstatus=@global_status()
  COLOR statusc(sysstatus)
  PUT_BITMAP sym$(sysstatus),x+2,y+2,16,16
  SETFONT "-*-helvetica-bold-r-*-*-20-*"
  TEXT x+35,17+y,status$(sysstatus)

RETURN
PROCEDURE showit(n,x,y)
  LOCAL i
  PRINT "SHOWIT: ",n,x,y
  COLOR schwarz
  PBOX x,y,x+220+100,y+100
  COLOR weiss
  BOX x-1,y,x+221,Y+100
  BOX x+223,y,x+320,Y+100
  SETFONT "-*-helvetica-medium-r-*-*-20-*"
  TEXT x+140,y+90,sysname$(n)
  COLOR gelb
  a()=tinevget("LMBDAC8/#"+STR$(n)+"[FUNCTION]")
  IF ccserr<>0
    COLOR rot
    SETFONT "-*-helvetica-medium-r-*-*-34-*"
    TEXT x+20,y+70,"ERROR: "+STR$(ccserr)
    singlestat(n)=6
  ELSE
    IF dim?(a())=220
      sysiq(n,:)=a()
      SCOPE a(),0,-50,y+50,1,x
    ENDIF
  ENDIF
  scale=tineget("LMBDAC8/#"+STR$(n)+"[AMPLITUDE]")
  SETFONT "-*-helvetica-bold-r-*-*-10-*"
  TEXT x+225,y+56,"Scale="+STR$(scale,6,6)
  SETFONT "-*-helvetica-bold-r-*-*-20-*"
  stat=tineget("LMBDAC8/#"+STR$(n)+"[STATUS]")
  COLOR statusc(stat)
  TEXT x+225,y+90,status$(stat)
  bits=tineget("LMBDAC8/#"+STR$(n)+"[BITS]")
  singlebits(n)=bits
  FOR i=0 TO 19
    IF btst(bits,19-i)
      COLOR gruen
    ELSE
      COLOR grau2
    ENDIF
    PCIRCLE x+230+(i MOD 8)*12,y+10+12*(i DIV 8),5
    COLOR weiss
    CIRCLE x+230+(i MOD 8)*12,y+10+12*(i DIV 8),5
  NEXT i
  VSYNC
RETURN
FUNCTION global_status
  LOCAL a,i
  a=0
  FOR i=0 TO anzchannel-1
    IF singlestat(i)=6
      a=MAX(a,4)
    ELSE
      a=MAX(a,singlestat(i))
    ENDIF
  NEXT i
  RETURN a
ENDFUNCTION
PROCEDURE showap(n,x,y)
  LOCAL i,a(),b()
  COLOR schwarz
  PBOX x,y,x+220,y+100
  PBOX x,y+th,x+220,y+100+th
  PCIRCLE x+230+100,y+th,100
  COLOR weiss
  BOX x-1,y,x+221,Y+100
  BOX x-1,y+th,x+221,Y+100+th
  CIRCLE x+230+100,y+th,100
  SETFONT "-*-helvetica-medium-r-*-*-20-*"
  TEXT x+110,y+90,sysname2$(n*2)
  TEXT x+110,y+90+th,sysname2$(n*2+1)
  COLOR orange
  a()=sysap(2*n,:)
  SCOPE a(),0,-50,y+100,1,x
  b()=sysap(2*n+1,:)
  SCOPE b(),0,-50/PI,y+50+th,1,x
  a()=sysiq(2*n,:)
  b()=sysiq(2*n+1,:)
  COLOR grau2
  SCOPE a(),b(),0,-70,y+th,-70,x+330
  COLOR orange
  SCOPE a(),b(),1,-70,y+th,-70,x+330
  VSYNC

RETURN
PROCEDURE do_help
  LOCAL t$
  t$="Amplitudenmodulation der 208 MHz HF-Systeme A,B,C,D|\
|Derzeit nur als Test-Version, da die Hardware noch Probleme macht.\
|Die gelben Kurven (links) sind die zurueckgelesenen Tabellen nach der Scalierung\
|In orange die Amplituden und Phasen, die sich daraus ergeben.\
|Die System-Bits haben folgende Bedeutungen:\
|obere Reihe von links:  0011 1111\
"
  IF form_alert(1,"[1]["+t$+"][ OK | mehr ...]")=2
    t$="208 MHz AM fuer HERA-p||"
    t$=t$+"Fuer weitere Fragen bitte an Markus Hoffmann wenden|"
    ~form_alert(1,"[1]["+t$+"][ OK ]")
  ENDIF
RETURN
PROCEDURE load_table(n)
  LOCAL i,a$,b$,t$
  FILESELECT "load Table for Sys."+CHR$(ASC("A")+n),"./*.tab","",f$
  IF len(f$)
    IF exist(f$)
      DEFMOUSE 2
      VSYNC
      OPEN "I",#1,f$
      i=0
      WHILE not eof(#1)
        LINEINPUT #1,t$
        t$=TRIM$(t$)
        IF left$(t$)<>"#"
          WORT_SEP t$," ",1,a$,b$
          sysiq(2*n,i)=VAL(a$)
          sysiq(2*n+1,i)=VAL(b$)
          INC i
          EXIT if i=220
        ENDIF
      WEND
      CLOSE #1
      TINEPUT "LMBDAC8/#"+STR$(2*n)+"[FUNCTION]",sysiq(2*n,:)
      TINEPUT "LMBDAC8/#"+STR$(2*n+1)+"[FUNCTION]",sysiq(2*n+1,:)
      zn=2*n
      @showit(zn,10,50+th*zn)
      @showit(zn+1,10,50+th*(zn+1))
      @showap(n,350,50+th*zn)
      DEFMOUSE 0
    ELSE
      ~form_alert(1,"[3][File does not exist !][OK]")
    ENDIF
  ENDIF
RETURN
PROCEDURE define_table(n)
  LOCAL x,y,w,h,tree0$,ret
  LOCAL string0$,string1$,string2$,string3$,string4$,string5$
  LOCAL string6$,string7$,string8$,string9$,string10$,string11$
  LOCAL obj0$,obj1$,obj2$,obj3$,obj4$,obj5$
  LOCAL a(),b()
  a()=sysiq(2*n,:)
  b()=sysiq(2*n+1,:)

  string0$="Define Function for System "+CHR$(ASC("A")+n)+":"+CHR$(0)
  obj1$=mki$(2)+mki$(-1)+mki$(-1)+mki$(26)+mki$(0)+mki$(17)+mkl$(VARPTR(string0$))+mki$(16)+mki$(16)+mki$(560)+mki$(16)
  obj2$=mki$(3)+mki$(-1)+mki$(-1)+mki$(20)+mki$(64)+mki$(32)+mkl$(135424)+mki$(16)+mki$(64)+mki$(224)+mki$(128)
  obj3$=mki$(4)+mki$(-1)+mki$(-1)+mki$(20)+mki$(64)+mki$(32)+mkl$(135424)+mki$(16)+mki$(224)+mki$(224)+mki$(128)
  obj4$=mki$(5)+mki$(-1)+mki$(-1)+mki$(20)+mki$(64)+mki$(32)+mkl$(135424)+mki$(256)+mki$(96)+mki$(224)+mki$(224)
  string1$="Polarplot:"+CHR$(0)
  string2$="Amplitude:"+CHR$(0)
  string3$="Phase:"+CHR$(0)
  obj5$=mki$(6)+mki$(-1)+mki$(-1)+mki$(28)+mki$(0)+mki$(0)+mkl$(VARPTR(string1$))+mki$(256)+mki$(80)+mki$(0)+mki$(0)
  obj6$=mki$(7)+mki$(-1)+mki$(-1)+mki$(28)+mki$(0)+mki$(0)+mkl$(VARPTR(string2$))+mki$(16)+mki$(48)+mki$(0)+mki$(0)
  obj7$=mki$(8)+mki$(-1)+mki$(-1)+mki$(28)+mki$(0)+mki$(0)+mkl$(VARPTR(string3$))+mki$(16)+mki$(208)+mki$(0)+mki$(0)
  save=8
  string4$="save ..."+CHR$(0)
  obj8$=mki$(9)+mki$(-1)+mki$(-1)+mki$(26)+mki$(5)+mki$(0)+mkl$(VARPTR(string4$))+mki$(512)+mki$(64)+mki$(64)+mki$(16)
  clear=9
  string5$="clear"+CHR$(0)
  obj9$=mki$(10)+mki$(-1)+mki$(-1)+mki$(26)+mki$(5)+mki$(0)+mkl$(VARPTR(string5$))+mki$(512)+mki$(96)+mki$(64)+mki$(16)
  ok=10
  string6$="OK"+CHR$(0)
  obj10$=mki$(11)+mki$(-1)+mki$(-1)+mki$(26)+mki$(7)+mki$(0)+mkl$(VARPTR(string6$))+mki$(512)+mki$(128)+mki$(64)+mki$(64)
  cancel=11
  string7$="CANCEL"+CHR$(0)
  obj11$=mki$(0)+mki$(-1)+mki$(-1)+mki$(26)+mki$(37)+mki$(0)+mkl$(VARPTR(string7$))+mki$(512)+mki$(208)+mki$(64)+mki$(64)
  '# END DOIT2: 	0	13
  obj0$=mki$(-1)+mki$(1)+mki$(11)+mki$(20)+mki$(0)+mki$(16)+mkl$(135424)+mki$(0)+mki$(0)+mki$(592)+mki$(384)
  '# END DOIT2: 	-1	13
  tree0$=obj0$+obj1$+obj2$+obj3$+obj4$+obj5$+obj6$+obj7$+obj8$+obj9$+obj10$
  tree0$=tree0$+obj11$

  ~form_center(varptr(tree0$),x,y,w,h)
  ~form_dial(0,0,0,0,0,x,y,w,h)
  ~form_dial(1,0,0,0,0,x,y,w,h)
  DO
    ' Hier jetzt die Funktionen zeichnen
    ~objc_draw(varptr(tree0$),0,-1,0,0)
    COLOR hellblau
    SCOPE a(),b(),0,-112,y+96+112,-112,x+256+112
    COLOR orange
    SCOPE a(),b(),1,-112,y+96+112,-112,x+256+112
    SCOPE a(),0,-64,y+64+64,224/220,x+16
    SCOPE b(),0,-64,y+224+64,224/220,x+16
    ret=form_do(VARPTR(tree0$))
    IF ret=cancel
      EXIT if true
    ELSE if ret=ok
      DEFMOUSE 2
      VSYNC
      sysiq(2*n,:)=a()
      sysiq(2*n+1,:)=b()
      TINEPUT "LMBDAC8/#"+STR$(2*n)+"[FUNCTION]",sysiq(2*n,:)
      TINEPUT "LMBDAC8/#"+STR$(2*n+1)+"[FUNCTION]",sysiq(2*n+1,:)
      DEFMOUSE 0
      EXIT if true
    ELSE if ret=clear
      ARRAYFILL a(),0
      ARRAYFILL b(),0
      @objc_change(varptr(tree0$),ret,0)
      ~objc_draw(varptr(tree0$),ret,1,0,0)
    ELSE if ret=save
      again:
      FILESELECT "save Table ...","./*.tab","sys_"+CHR$(ASC("A")+n)+".tab",f$
      IF len(f$)
        IF exist(f$)
          IF form_alert(1,"[3][File already exist !][overwrite|CANCEL]")=2
            GOTO again
          ENDIF
        ENDIF
        DEFMOUSE 2
        OPEN "O",#1,f$
        PRINT #1,"# Funktionstabelle I   Q   erzeugt am "+date$+" "+time$
        PRINT #1,"# System "+CHR$(ASC("A")+n)
        PRINT #1,"# LEN=220"
        FOR i=0 TO 219
          PRINT #1,a(i);" ";b(i)
        NEXT i
        CLOSE #1
        DEFMOUSE 0
      ENDIF
      @objc_change(varptr(tree0$),ret,0)
      ~objc_draw(varptr(tree0$),ret,1,0,0)
    ELSE if ret=2
      WHILE mousek
        i=MAX(0,MIN(INT((mousex-16-x)/224*220),219))
        a(i)=MIN(1,MAX(-1,-(mousey-64-64-y)/64))
        COLOR hellblau
        SCOPE a(),b(),0,-112,y+96+112,-112,x+256+112
        COLOR orange
        SCOPE a(),b(),1,-112,y+96+112,-112,x+256+112
        SCOPE a(),0,-64,y+64+64,224/220,x+16
        SCOPE b(),0,-64,y+224+64,224/220,x+16
        VSYNC
        COLOR weiss
        PBOX x+16,y+64,x+16+224,y+64+128
        PBOX x+16,y+224,x+16+224,y+224+128
        PBOX x+256,y+96,x+256+224,y+96+224
      WEND
    ELSE if ret=3
      WHILE mousek
        i=MAX(0,MIN(INT((mousex-16-x)/224*220),219))
        b(i)=MIN(1,MAX(-1,-(mousey-224-64-y)/64))
        COLOR hellblau
        SCOPE a(),b(),0,-112,y+96+112,-112,x+256+112
        COLOR orange
        SCOPE a(),b(),1,-112,y+96+112,-112,x+256+112
        SCOPE a(),0,-64,y+64+64,224/220,x+16
        SCOPE b(),0,-64,y+224+64,224/220,x+16
        VSYNC
        COLOR weiss
        PBOX x+16,y+64,x+16+224,y+64+128
        PBOX x+16,y+224,x+16+224,y+224+128
        PBOX x+256,y+96,x+256+224,y+96+224
      WEND
    ELSE
      PRINT ret
    ENDIF
  LOOP
  ~form_dial(2,0,0,0,0,x,y,w,h)
  ~form_dial(3,0,0,0,0,x,y,w,h)
  zn=2*n
  @showit(zn,10,50+th*zn)
  @showit(zn+1,10,50+th*(zn+1))
  @showap(n,350,50+th*zn)
RETURN
PROCEDURE do_settings
  LOCAL x,y,w,h,tree0$,ret
  LOCAL string0$,string1$,string2$,string3$,string4$,string5$
  LOCAL string6$,string7$,string8$,string9$,string10$,string11$
  LOCAL obj0$,obj1$
  string0$="Settings ..."+CHR$(0)
  obj1$=mki$(2)+mki$(-1)+mki$(-1)+mki$(26)+mki$(0)+mki$(17)+mkl$(VARPTR(string0$))+mki$(16)+mki$(16)+mki$(560)+mki$(16)
  string1$="Interface Device:"+CHR$(0)
  obj2$=mki$(3)+mki$(-1)+mki$(-1)+mki$(28)+mki$(0)+mki$(0)+mkl$(VARPTR(string1$))+mki$(16)+mki$(48)+mki$(136)+mki$(16)
  string2$="Breite:"+CHR$(0)
  obj3$=mki$(4)+mki$(-1)+mki$(-1)+mki$(28)+mki$(0)+mki$(0)+mkl$(VARPTR(string2$))+mki$(16)+mki$(64)+mki$(56)+mki$(16)
  string3$="L�nge:"+CHR$(0)
  obj4$=mki$(5)+mki$(-1)+mki$(-1)+mki$(28)+mki$(0)+mki$(0)+mkl$(VARPTR(string3$))+mki$(16)+mki$(80)+mki$(56)+mki$(16)
  string4$="bis"+CHR$(0)
  obj5$=mki$(6)+mki$(-1)+mki$(-1)+mki$(28)+mki$(0)+mki$(0)+mkl$(VARPTR(string4$))+mki$(280)+mki$(64)+mki$(56)+mki$(16)
  string5$="bis"+CHR$(0)
  obj6$=mki$(7)+mki$(-1)+mki$(-1)+mki$(28)+mki$(0)+mki$(0)+mkl$(VARPTR(string5$))+mki$(280)+mki$(80)+mki$(56)+mki$(16)
  devtext=7
  string6$="/dev/ttyS0"+CHR$(0)+SPACE$(39)
  string7$="_______________________________________"+CHR$(0)
  string8$="XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"+CHR$(0)
  tedinfo0$=mkl$(VARPTR(string6$))+mkl$(VARPTR(string7$))+mkl$(VARPTR(string8$))+mki$(3)+mki$(0)+mki$(0)+mki$(4513)+mki$(0)+mki$(1)+mki$(39)+mki$(39)
  obj7$=mki$(8)+mki$(-1)+mki$(-1)+mki$(29)+mki$(8)+mki$(0)+mkl$(VARPTR(tedinfo0$))+mki$(160)+mki$(48)+mki$(240)+mki$(16)
  br1=8
  string9$="N59:00:00.00"+CHR$(0)+SPACE$(12)
  string10$="____________"+CHR$(0)
  string11$="XXXXXXXXXXXX"+CHR$(0)
  tedinfo1$=mkl$(VARPTR(string9$))+mkl$(VARPTR(string10$))+mkl$(VARPTR(string11$))+mki$(3)+mki$(0)+mki$(0)+mki$(4513)+mki$(0)+mki$(1)+mki$(12)+mki$(12)
  obj8$=mki$(9)+mki$(-1)+mki$(-1)+mki$(29)+mki$(8)+mki$(0)+mkl$(VARPTR(tedinfo1$))+mki$(160)+mki$(64)+mki$(96)+mki$(16)
  br2=9
  string12$="N59:00:00.00"+CHR$(0)+SPACE$(12)
  string13$="____________"+CHR$(0)
  string14$="XXXXXXXXXXXX"+CHR$(0)
  tedinfo2$=mkl$(VARPTR(string12$))+mkl$(VARPTR(string13$))+mkl$(VARPTR(string14$))+mki$(3)+mki$(0)+mki$(0)+mki$(4513)+mki$(0)+mki$(1)+mki$(12)+mki$(12)
  obj9$=mki$(10)+mki$(-1)+mki$(-1)+mki$(29)+mki$(8)+mki$(0)+mkl$(VARPTR(tedinfo2$))+mki$(320)+mki$(64)+mki$(96)+mki$(16)
  lon1=10
  string15$="E009:00:00.00"+CHR$(0)+SPACE$(13)
  string16$="_____________"+CHR$(0)
  string17$="XXXXXXXXXXXXX"+CHR$(0)
  tedinfo3$=mkl$(VARPTR(string15$))+mkl$(VARPTR(string16$))+mkl$(VARPTR(string17$))+mki$(3)+mki$(0)+mki$(0)+mki$(4513)+mki$(0)+mki$(1)+mki$(13)+mki$(13)
  obj10$=mki$(11)+mki$(-1)+mki$(-1)+mki$(29)+mki$(8)+mki$(0)+mkl$(VARPTR(tedinfo3$))+mki$(160)+mki$(80)+mki$(104)+mki$(16)
  lon2=11
  string18$="E009:00:00.00"+CHR$(0)+SPACE$(13)
  string19$="_____________"+CHR$(0)
  string20$="XXXXXXXXXXXXX"+CHR$(0)
  tedinfo4$=mkl$(VARPTR(string18$))+mkl$(VARPTR(string19$))+mkl$(VARPTR(string20$))+mki$(3)+mki$(0)+mki$(0)+mki$(4513)+mki$(0)+mki$(1)+mki$(13)+mki$(13)
  obj11$=mki$(12)+mki$(-1)+mki$(-1)+mki$(29)+mki$(8)+mki$(0)+mkl$(VARPTR(tedinfo4$))+mki$(320)+mki$(80)+mki$(104)+mki$(16)
  ok=12
  string21$="OK"+CHR$(0)
  obj12$=mki$(13)+mki$(-1)+mki$(-1)+mki$(26)+mki$(7)+mki$(0)+mkl$(VARPTR(string21$))+mki$(520)+mki$(64)+mki$(56)+mki$(64)
  cancel=13
  string22$="CANCEL"+CHR$(0)
  obj13$=mki$(0)+mki$(-1)+mki$(-1)+mki$(26)+mki$(37)+mki$(0)+mkl$(VARPTR(string22$))+mki$(520)+mki$(144)+mki$(56)+mki$(64)
  '# END DOIT2: 	0	15
  obj0$=mki$(-1)+mki$(1)+mki$(13)+mki$(20)+mki$(0)+mki$(16)+mkl$(135424)+mki$(0)+mki$(0)+mki$(592)+mki$(224)
  '# END DOIT2: 	-1	15
  tree0$=obj0$+obj1$+obj2$+obj3$+obj4$+obj5$+obj6$+obj7$+obj8$+obj9$+obj10$
  tree0$=tree0$+obj11$+obj12$+obj13$
  ~form_center(varptr(tree0$),x,y,w,h)
  ~form_dial(0,0,0,0,0,x,y,w,h)
  ~form_dial(1,0,0,0,0,x,y,w,h)
  ret=form_do(VARPTR(tree0$))
  ~form_dial(2,0,0,0,0,x,y,w,h)
  ~form_dial(3,0,0,0,0,x,y,w,h)
RETURN
PROCEDURE mausfrage
  LOCAL t$,mouse_x,mouse_y,mouse_k
  MOUSE mouse_x,mouse_y,mouse_k
  IF mouse_k=1
    ob=objc_find(VARPTR(tree0$),mouse_x,mouse_y)
    IF ob>0
      IF ob=o_quit
        @objc_change(varptr(tree0$),ob,1)
        ~objc_draw(varptr(tree0$),ob,1,0,0)
        VSYNC
        WHILE mousek=mouse_k
        WEND
        QUIT
      ELSE if ob=o_help
        @objc_change(varptr(tree0$),ob,1)
        ~objc_draw(varptr(tree0$),ob,1,0,0)
        VSYNC
        @do_help
        @objc_change(varptr(tree0$),ob,0)
        ~objc_draw(varptr(tree0$),ob,1,0,0)
      ELSE if ob=o_settings
        @objc_change(varptr(tree0$),ob,1)
        ~objc_draw(varptr(tree0$),ob,1,0,0)
        VSYNC
        @do_settings
        @objc_change(varptr(tree0$),ob,0)
        ~objc_draw(varptr(tree0$),ob,1,0,0)
      ELSE if ob=o_reset
        @objc_change(varptr(tree0$),ob,1)
        ~objc_draw(varptr(tree0$),ob,1,0,0)
        VSYNC
        TINEPUT "LMBDAC8/#8[CMD]",1
        @objc_change(varptr(tree0$),ob,0)
        ~objc_draw(varptr(tree0$),ob,1,0,0)
        @redraw
      ELSE if ob=o_stop
        @objc_change(varptr(tree0$),ob,1)
        ~objc_draw(varptr(tree0$),ob,1,0,0)
        VSYNC
        TINEPUT "LMBDAC8/#8[CMD]",3
        @objc_change(varptr(tree0$),ob,0)
        ~objc_draw(varptr(tree0$),ob,1,0,0)
      ELSE IF ob=o_stop
        @objc_change(varptr(tree0$),ob,1)
        ~objc_draw(varptr(tree0$),ob,1,0,0)
        VSYNC
        TINEPUT "LMBDAC8/#8[CMD]",2
        @objc_change(varptr(tree0$),ob,0)
        ~objc_draw(varptr(tree0$),ob,1,0,0)
      ELSE IF ((ob-6) MOD 6)=0 OR ((ob-6) MOD 6)=4
        @objc_change(varptr(tree0$),ob,1)
        ~objc_draw(varptr(tree0$),ob,1,0,0)
        VSYNC
        TINEPUT "LMBDAC8/#"+STR$(2*((ob-6) DIV 6)+abs(((ob-6) MOD 6)=4))+"[CMD]",5
        @objc_change(varptr(tree0$),ob,0)
        ~objc_draw(varptr(tree0$),ob,1,0,0)
        zn=2*((ob-6) DIV 6)+abs(((ob-6) MOD 6)=4)
        n=2*((ob-6) DIV 6)
        @showit(zn,10,50+th*zn)
        @showap(n,350,50+th*2*n)
      ELSE if ((ob-6) MOD 6)=1 OR ((ob-6) MOD 6)=5
        @objc_change(varptr(tree0$),ob,1)
        ~objc_draw(varptr(tree0$),ob,1,0,0)
        VSYNC
        TINEPUT "LMBDAC8/#"+STR$(2*((ob-6) DIV 6)+abs(((ob-6) MOD 6)=5))+"[CMD]",4
        @objc_change(varptr(tree0$),ob,0)
        ~objc_draw(varptr(tree0$),ob,1,0,0)
      ELSE if ((ob-6) MOD 6)=2
        @objc_change(varptr(tree0$),ob,1)
        ~objc_draw(varptr(tree0$),ob,1,0,0)
        @load_table(((ob-6) div 6))
        @objc_change(varptr(tree0$),ob,0)
        ~objc_draw(varptr(tree0$),ob,1,0,0)
      ELSE if ((ob-6) MOD 6)=3
        @objc_change(varptr(tree0$),ob,1)
        ~objc_draw(varptr(tree0$),ob,1,0,0)
        @define_table(((ob-6) div 6))
        @objc_change(varptr(tree0$),ob,0)
        ~objc_draw(varptr(tree0$),ob,1,0,0)
      ELSE
        PRINT "Klicked on Object #";ob
      ENDIF
      WHILE mousek=mouse_k
      WEND
    ENDIF
  ENDIF
RETURN
PROCEDURE objc_change(adr,o,s)
  DPOKE adr+24*o+10,s
RETURN
PROCEDURE desktop_init
  DIM obj$(30)
  o_help=1
  string0$="HELP"+CHR$(0)
  obj1$=mki$(2)+mki$(-1)+mki$(-1)+mki$(26)+mki$(5)+mki$(32)+mkl$(VARPTR(string0$))+mki$(16*45)+mki$(anzchannel*th+100-3*16)+mki$(56)+mki$(16*2)
  o_settings=2
  string1$="Settings..."+CHR$(0)
  obj2$=mki$(3)+mki$(-1)+mki$(-1)+mki$(26)+mki$(5)+mki$(32)+mkl$(VARPTR(string1$))+mki$(16*30)+mki$(anzchannel*th+100-3*16)+mki$(96)+mki$(16)
  o_start=3
  string2$="START"+CHR$(0)
  obj3$=mki$(4)+mki$(-1)+mki$(-1)+mki$(26)+mki$(5)+mki$(0)+mkl$(VARPTR(string2$))+mki$(16*15)+mki$(anzchannel*th+100-3*16)+mki$(56)+mki$(16)
  o_stop=4
  string3$="STOP"+CHR$(0)
  obj4$=mki$(5)+mki$(-1)+mki$(-1)+mki$(26)+mki$(5)+mki$(0)+mkl$(VARPTR(string3$))+mki$(16*20)+mki$(anzchannel*th+100-3*16)+mki$(56)+mki$(16)
  o_reset=5
  string4$="RESET"+CHR$(0)
  obj5$=mki$(6)+mki$(-1)+mki$(-1)+mki$(26)+mki$(5)+mki$(0)+mkl$(VARPTR(string4$))+mki$(16*25)+mki$(anzchannel*th+100-3*16)+mki$(56)+mki$(16)
  obcount=6
  ' Hier jetzt noch die Kanalspezifischen Buttons einbauen
  stringaa$="~"+CHR$(0)
  stringbb$="N"+CHR$(0)
  stringcc$="load ..."+CHR$(0)
  stringdd$="define ..."+CHR$(0)
  FOR i=0 TO anzchannel-1
    obj$(obcount)=mki$(obcount+1)+mki$(-1)+mki$(-1)+mki$(26)+mki$(5)+mki$(0)+mkl$(VARPTR(stringaa$))+mki$(16*49)+mki$(i*th+60)+mki$(16)+mki$(16)
    INC obcount
    obj$(obcount)=mki$(obcount+1)+mki$(-1)+mki$(-1)+mki$(26)+mki$(5)+mki$(0)+mkl$(VARPTR(stringbb$))+mki$(16*51)+mki$(i*th+60)+mki$(16)+mki$(16)
    INC obcount
    IF even(i)
      obj$(obcount)=mki$(obcount+1)+mki$(-1)+mki$(-1)+mki$(26)+mki$(5)+mki$(0)+mkl$(VARPTR(stringcc$))+mki$(16*49)+mki$(i*th+60+24)+mki$(16*5)+mki$(16)
      INC obcount
      obj$(obcount)=mki$(obcount+1)+mki$(-1)+mki$(-1)+mki$(26)+mki$(5)+mki$(0)+mkl$(VARPTR(stringdd$))+mki$(16*49)+mki$(i*th+60+6*8)+mki$(16*5)+mki$(16)
      INC obcount
    ENDIF
  NEXT i
  stringee$="Global Scaling:"+CHR$(0)
  obj$(obcount)=mki$(obcount+1)+mki$(-1)+mki$(-1)+mki$(28)+mki$(0)+mki$(0)+mkl$(VARPTR(stringee$))+mki$(16*15)+mki$(anzchannel*th+100-3*8)+mki$(0)+mki$(0)
  INC obcount
  o_quit=obcount
  string5$="QUIT"+CHR$(0)
  objq$=mki$(0)+mki$(-1)+mki$(-1)+mki$(26)+mki$(39)+mki$(0)+mkl$(VARPTR(string5$))+mki$(bw-9*8)+mki$(anzchannel*th+100-3*16)+mki$(56)+mki$(32)
  ' Box:
  obj0$=mki$(-1)+mki$(1)+mki$(obcount)+mki$(20)+mki$(0)+mki$(0)+mkl$(69912)+mki$(0)+mki$(0)+mki$(bw)+mki$(anzchannel*th+100)
  tree0$=obj0$+obj1$+obj2$+obj3$+obj4$+obj5$
  FOR i=6 TO obcount-1
    tree0$=tree0$+obj$(i)
  NEXT i
  tree0$=tree0$+objq$
  ~objc_draw(varptr(tree0$),0,-1,0,0)
RETURN

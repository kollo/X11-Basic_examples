
' Programm fuer Susan W. fuer Ihre Musiker-Tagungen
' (c) Markus Hoffmann Okt. 2004

DIM day$(6),sym$(16),table(16,16),tut(16,4)
CLR count
fbest=4711
CLS
FOR i=0 TO 15
  READ sym$(i)
NEXT i
RANDOMIZE
DO
  IF count=0
    IF exist("susan.best")
      @load("susan.best")
      ' @sofiset
    ELSE
      @sofiset
      ' @randomset
    ENDIF
  ELSE
    @verbessern
  ENDIF
  INC count

  ARRAYFILL table(),0
  ARRAYFILL tut(),0

  FOR j=0 TO 5
    a$=day$(j)
    FOR i=0 TO 15
      a=PEEK(VARPTR(a$)+i)
      tut(i,a-asc("0"))=tut(i,a-asc("0"))+1
      FOR k=0 TO 15
        b=PEEK(VARPTR(a$)+k)
        IF b=a
          table(i,k)=table(i,k)+1
        ENDIF
      NEXT k
    NEXT i
  NEXT j
  f=0
  FOR i=0 TO 15
    FOR j=0 TO 3
      IF tut(i,j)<1
        ADD f,1
      ENDIF
      IF tut(i,j)>2
        ADD f,tut(i,j)-2
      ENDIF
    NEXT j
  NEXT i
  FOR i=0 TO 15
    FOR j=i+1 TO 15
      IF table(i,j)<1
        ADD f,1
      ENDIF
      IF table(i,j)>2
        ADD f,table(i,j)-2
      ENDIF
    NEXT j
  NEXT i
  IF f<=fbest
    IF f<fbest AND count>1
      BEEP
    ENDIF
    PRINT chr$(27);"[H";
    @printit
    PRINT string$(45,"-")
    PRINT count;" ";timer;" ";INT(timer-t);" ";"f=";f-8*3;"    "
    fbest=f
    OPEN "O",#1,"susan.best"
    PRINT #1,"# Zwischenergebnis von susan.bas (c) Markus Hoffmann"
    PRINT #1,"# date=";date$;" time=";time$;" F=";f-8*3
    FOR h=0 TO 5
      PRINT #1,day$(h)
    NEXT h
    CLOSE #1
    t=timer
    IF f=0
      QUIT
    ENDIF
  ELSE
    PRINT "F= ";f;"  ";fbest
    IF f-8*3>1.5*(fbest-8*3)
      RUN
    ENDIF
  ENDIF
LOOP
QUIT

PROCEDURE verbessern
  LOCAL ui,uj,i,j,f,oo
  CLR oo,f
  PRINT ">";
  counter=RANDOM(64)+1
  DO
    FOR i=0 TO 15
      FOR j=0 TO 15
        IF table(i,j)=0 OR table(i,j)>2 OR (table(i,j)=2 AND oo<>0) AND (i MOD 4)<>(j MOD 4)
          ui=i
          uj=j
          f=1
          DEC counter
        ENDIF
        EXIT if counter=0
      NEXT j
      EXIT if counter=0
    NEXT i
    EXIT if counter=0
    PRINT "EXAUST:";counter,oo;
    INC oo
  LOOP
  PRINT "(";ui;" ";uj;") ";
  FLUSH
  tag=-1
  ' finde den Tag
  FOR i=0 TO 5
    d$=day$(i)
    IF peek(VARPTR(d$)+ui)=PEEK(VARPTR(d$)+uj)
      tag=i
      ' print "Tag ";tag
    ENDIF
    EXIT if tag<>-1
  NEXT i
  IF tag<>-1

    ' wir versetzen jetzt ui an einen anderen Tag wo ein Spieler
    ' gleichen Typs beim gleichen Tutor spielt

    dd$=day$(tag)
    tauschtag=-1
    FOR i=0 TO 5
      d$=day$(i)
      typ=(ui MOD 4)
      idx=(ui DIV 4)
      IF i<>tag
        FOR j=0 TO 3
          IF peek(VARPTR(d$)+j*4+typ)=PEEK(VARPTR(dd$)+ui) AND j<>idx AND PEEK(VARPTR(dd$)+j*4+typ)=PEEK(VARPTR(d$)+ui)
            tauschtag=i
            tauschidx=j
          ENDIF
          EXIT if tauschtag<>-1
        NEXT j
      ENDIF
      EXIT if tauschtag<>-1
    NEXT i
    IF tauschtag<>-1
      PRINT "<";tag;",";tauschtag;"> ";
      FLUSH
      d$=day$(tauschtag)
      a=PEEK(VARPTR(dd$)+ui)
      b=PEEK(VARPTR(dd$)+tauschidx*4+typ)
      c=PEEK(VARPTR(d$)+ui)
      d=PEEK(VARPTR(d$)+tauschidx*4+typ)
      POKE varptr(dd$)+ui,b
      POKE varptr(dd$)+tauschidx*4+typ,a
      POKE varptr(d$)+ui,d
      POKE varptr(d$)+tauschidx*4+typ,c
      day$(tauschtag)=d$
    ELSE
      PRINT "*TT-*";
      FLUSH
    ENDIF
    day$(tag)=dd$
  ELSE
    PRINT "*t-*";
    FLUSH
  ENDIF
RETURN

PROCEDURE printit
  PRINT "        1                2                3              4                5                6"
  PRINT day$(0)'day$(1)'day$(2)'day$(3)'day$(4)'day$(5)
  PRINT "       1       2       3       4        T"
  PRINT "    A B C D A B C D A B C D A B C D : 1 2 3 4"
  PRINT "    -----------------------------------------"
  FOR i=0 TO 15
    PRINT chr$(ASC("A")+(i MOD 4))'":"'
    FOR j=0 TO 15
      IF table(i,j)<1 OR table(i,j)>2 AND (i MOD 4)<>(j MOD 4)
        PRINT chr$(27);"[1m";
      ENDIF
      PRINT table(i,j)'
      PRINT chr$(27);"[m";
    NEXT j
    PRINT ":"'
    FOR j=0 TO 3
      IF tut(i,j)<1 OR tut(i,j)>2
        PRINT chr$(27);"[1m";
      ENDIF
      PRINT tut(i,j)'
      PRINT chr$(27);"[m";
    NEXT j
    PRINT
  NEXT i
  PRINT string$(45,"-")
  PRINT "day   Tut A   Tut B    Tut C    Tut D"
  PRINT "---+--------+--------+--------+--------+"
  FOR i=0 TO 5
    PRINT i+1;"  |";
    d$=day$(i)
    FOR j=0 TO 3
      FOR l=0 TO 3
        FOR k=0 TO 3
          IF peek(VARPTR(d$)+4*k+l)=ASC("0")+j
            PRINT chr$(ASC("A")+l);STR$(k+1);
          ENDIF
        NEXT k
      NEXT l
      PRINT "|";
    NEXT j
    PRINT
  NEXT i
RETURN

' Permutationen von 4 Zahlen (Symmetrische Gruppe)

DATA 0123,0132,0213,0231,1023,1032,1203,1230,2103,2130,2013,2031,3120,3102,3210,3201

' Erzeugt zufaellige (aber gueltige) Verteilung

PROCEDURE randomset
  global1=RANDOM(65536)
  global2=RANDOM(65536)
  global3=RANDOM(65536)
  global4=RANDOM(65536)
  global5=RANDOM(65536)
  global6=RANDOM(65536)

  day$(0)=@quintett$(global1)
  day$(1)=@quintett$(global2)
  day$(2)=@quintett$(global3)
  day$(3)=@quintett$(global4)
  day$(4)=@quintett$(global5)
  day$(5)=@quintett$(global6)
RETURN

' Erzeugt zufaellige Verteilung, bei der die Bedingung
' Fuer den Tutor erfuellt ist

PROCEDURE sofiset
  LOCAL d$,i,a,j
  d$=@quintett$(RANDOM(65536))
  day$(0)=d$
  FOR j=1 TO 5
    FOR i=0 TO 15
      a=(PEEK(VARPTR(d$)+i)-asc("0")+1) mod 4
      POKE varptr(d$)+i,a+ASC("0")
    NEXT i
    day$(j)=d$
  NEXT j
RETURN

' Wandelt 16 Bit Integer-Zahl in ein Gueltiges Quartett/Quintett-Format

FUNCTION quintett$(a)
  LOCAL f$,t$,i,j,s$
  t$=SPACE$(16)
  f$=HEX$(a,4,4)
  FOR i=0 TO 3
    a=VAL("0x"+CHR$(PEEK(VARPTR(f$)+i)))
    FOR j=0 TO 3
      s$=sym$(a)
      POKE varptr(t$)+i+j*4,PEEK(VARPTR(s$)+j)
    NEXT j
  NEXT i
  RETURN t$
ENDFUNCTION
PROCEDURE load(f$)
  LOCAL i
  CLR i
  OPEN "I",#1,f$
  WHILE not eof(#1)
    LINEINPUT #1,t$
    IF left$(t$)<>"#"
      day$(i)=t$
      INC i
    ENDIF
    EXIT if i=6
  WEND
  CLOSE #1
RETURN

' ########################################################
' ##        P L O T T E R   2  (c) Markus Hoffmann      ##
' ##                                                    ##
' ## Idee: 1986, Erstversion 1986, Bearbeitet bis 1991  ##
' ##                                                    ##
' ## Quelltext darf nicht(!) frei kopiert werden !      ##
' ##                                                    ##
' ########################################################
'
' ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
' ++ Hier werden die Funktionen eingegeben :                                ++
' ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
' Alle Funktionen werden in einem Koordinatensystem dargestellt.
' Um eine Funktion nicht darzustellen, einfach f(x)=0 deklarieren.

schaarstart=-1
schaarend=1
schaarstep=0.1
'
' ++++++++++++++++++++++++++++++++++++++++
' ++ Weitere Parameter Voreingestellt:  ++
' ++++++++++++++++++++++++++++++++++++++++
x1=-10
x2=10
y1=-2.2
y2=2.2
ex=1
ey=1
xab$="X"
yab$="Y"
'
'
' ~WIND_GET(4,0,bx&,by&,bw&,bh&)
bx=0
by=0
bw=640
bh=400

schwarz=COLOR_RGB(0,0,0)
weiss=COLOR_RGB(1,1,0)
rot=COLOR_RGB(1,0,0)

GET_GEOMETRY 1,bx,by,bw,bh

' GRAPHMODE 0
ON ERROR GOSUB errrr

COLOR weiss,rot
CLEARW
PBOX bx,by,bx+bw,by+bh

COLOR rot,weiss

' DEFTEXT 1,0,0,4
TEXT 30,370,"von Markus Hoffmann"
' DEFTEXT 1,16,0,13
' Koordinatentransformationen:
SHOWPAGE
@make(bx,by,bw,bh)
KEYEVENT a

CLR iii
ff:
ALERT 2," Fl�chen f�llen ?",2,"JA|NEE",balert
IF balert=1
  PAUSE 1
  DEFFILL ,2,iii+1
  DEFMOUSE 5
  MOUSEEVENT a

  FILL MOUSEX,MOUSEY
  DEFMOUSE 0
  INC iii
  GOTO ff
ENDIF
END
PROCEDURE errrr
  ON ERROR GOSUB errrr
  OUT 2,7
  PRINT ERR$(ERR)
  ' RESUME NEXT
RETURN
PROCEDURE make(bx,by,bw,bh)
  '
  ' Koordinatenkreuz:
  '
  ' ---Hilfslinien
  COLOR schwarz,weiss
  FOR x=0 TO MAX(ABS(x1),ABS(x2)) STEP ex
    DEFLINE 0x1211111f,1
    ' DEFLINE &X11111111111111111010101010101011,0,0,0
    LINE @kx(x),by&,@kx(x),by&+bh&
    LINE @kx(-x),by&,@kx(-x),by&+bh&
    DEFLINE 1,1,0,0
    LINE @kx(x),@ky(0)-2,@kx(x),@ky(0)+2
    LINE @kx(-x),@ky(0)-2,@kx(-x),@ky(0)+2
  NEXT x
  FOR y=0 TO MAX(ABS(y1),ABS(y2)) STEP ey
    DEFLINE 0x1211111f,1
    ' DEFLINE &X11111111111111111010101010101011,0,0,0
    LINE bx&,@ky(y),bx+bw,@ky(y)
    LINE bx&,@ky(-y),bx+bw,@ky(-y)
    DEFLINE 1,1,0,0
    LINE @kx(0)-2,@ky(y),@kx(0)+2,@ky(y)
    LINE @kx(0)-2,@ky(-y),@kx(0)+2,@ky(-y)
  NEXT y
  ' ---Koordinatenachsen
  DEFLINE 1,1,0,1
  LINE @kx(0),by+bh,@kx(0),by&    !Y-Achse
  TEXT @kx(0)+10,by+15,yab$
  LINE bx&,@ky(0),bx+bw,@ky(0)    !X-Achse
  TEXT bx+bw-8*(LEN(xab$)),@ky(0),xab$
  ' ---Achsenbeschriftung
  ' DEFTEXT 1,0,0,6
  TEXT @kx(ex)-4,@ky(0)+12,STR$(ex)
  TEXT @kx(0)+3,@ky(ey)+3,STR$(ey)
  '
  ' Funktionen zeichnen
  '
  SHOWPAGE
  ' DEFLINE 0,0,0,0
  ' ---F(x)
  yy=@ky(0)
  FOR x=bx TO bx+bw
    y=@ky(@f(@bkx(x)))
    LINE x-1,yy,x,y
    yy=y
  NEXT x
  SHOWPAGE
  ' ---T(x)
  yy=@ky(0)
  FOR x=bx TO bx+bw
    y&=@ky(@t(@bkx(x)))
    LINE x-1,yy,x,y
    yy=y
  NEXT x
  SHOWPAGE
  ' ---schaar(x)
  FOR a=schaarstart TO schaarend STEP schaarstep
    yy=@ky(0)
    FOR x=bx TO bx+bw
      y=@ky(@schaar(@bkx(x),a))
      LINE x-1,yy,x,y
      yy=y
    NEXT x
    SHOWPAGE
  NEXT a
RETURN

DEFFN f(x)=SIN(x)                         ! # Funktion F(x)
DEFFN t(x)=COS(x)*SIN(x)+SIN(x)           ! # Funktion T(x)
DEFFN schaar(x,a)=a/(x+ABS(x==0)*0.01)    ! # Funktionenschaar

DEFFN kx(x)=bw/(x2-x1)*(x-x1)+bx
DEFFN bkx(x)=(x-bx)/bw*(x2-x1)+x1
DEFFN ky(y)=-bh/(y2-y1)*(y+y1)+by
DEFFN bky(y)=-(y-by)/bh*(y2-y1)-y1

#!/bin/xbasic

' This script redirects a request for a page to another page
' This can be done dynamically depending on
' HTTP_USER_AGENT or any other information
' Also a log-file is created in /tmp/
' (c) Markus Hoffmann 1999
'

PRINT "HTTP/1.1 302 Moved Temporarily"+chr$(13)

t$=env$("QUERY_STRING")
newurl$="http://search.metacrawler.com/crawler?"+t$

h$=env$("REMOTE_ADDR")

ua$=env$("HTTP_USER_AGENT")

' create log entry

OPEN "A",#1,"/tmp/nph-redir.log"
PRINT #1,date$+" "+time$+" "+h$+" "+t$+" "+ua$
CLOSE #1

PRINT "Location: http://search.metacrawler.com/crawler?"+t$+chr$(13)
PRINT "Connection: close"+chr$(13)
PRINT "Content-Type: text/plain"+chr$(13)
PRINT ""+chr$(13)
PRINT "302 Moved Temporarily"+chr$(13)
FLUSH
QUIT

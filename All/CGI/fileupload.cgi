#!/usr/bin/xbasic
' Test cgi-script with X11-Basic (c) Markus Hoffmann

chuncklen=100000

PRINT "Content-type: text/html"+chr$(13)
PRINT ""+chr$(13)
FLUSH
PRINT "<html><head><TITLE>File upload CGI</TITLE><head><body>"
PRINT "<h1>Uploading...</h1>"
PRINT "<pre>"
PRINT "Length=";val(env$("CONTENT_LENGTH"))
FLUSH

length=val(env$("CONTENT_LENGTH"))
WHILE length>chuncklen
  t$=t$+input$(chuncklen)
  SUB length,chuncklen
WEND
IF length
  t$=t$+input$(length)
ENDIF
PRINT "got: ";len(t$);" Bytes."

WORT_SEP t$,chr$(13)+chr$(10),0,endmarker$,t$
PRINT endmarker$

SPLIT t$,chr$(13)+chr$(10),0,d$,t$
PRINT d$
SPLIT t$,chr$(13)+chr$(10),0,a$,t$
PRINT a$
SPLIT t$,chr$(13)+chr$(10),0,a$,t$
PRINT a$

PRINT len(t$),11708+len(endmarker$)
IF right$(t$,len(endmarker$)+6)=chr$(13)+chr$(10)+endmarker$+"--"+chr$(13)+chr$(10)
  t$=left$(t$,len(t$)-len(endmarker$)-6)
ELSE
  PRINT "ERROR, something is wrong."
  MEMDUMP varptr(t$)+len(t$)-32,32
ENDIF
FLUSH
c$=t$

SPLIT d$,"; ",0,a$,d$
WHILE len(a$)
  SPLIT a$,"=",0,b$,bb$
  IF b$="name"
    PRINT b$;" --> ";declose$(bb$)
  ELSE if b$="filename"
    filename$=declose$(bb$)
    PRINT b$;" --> ";declose$(bb$)
  ENDIF
  SPLIT d$,"; ",0,a$,d$
WEND
PRINT "got: ";len(c$);" Bytes."

IF len(c$) AND len(filename$)
  PRINT "saving...<"+filename$+">"+len(filename$)
  IF exist("/tmp/"+filename$)
    PRINT "Existig file was replaced."
  ENDIF
  IF right$(filename$,4)=".csv"
    BSAVE "/tmp/"+filename$,varptr(c$),len(c$)
    PRINT "file "+filename$+" saved."
  ENDIF
ENDIF

PRINT "</pre>"
FLUSH
r$=env$("REMOTE_ADDR")
h$=env$("REMOTE_HOST")
OPEN "A",#1,"/tmp/WEBfileupload.log"
PRINT #1,date$+" "+time$+" "+r$+" "+h$+" "+filename$
CLOSE #1
PRINT "<h1>finish!</h1>"
PRINT "<h3>Another upload:</h3>"
PRINT "<form action=/cgi-bin/fileupload.cgi enctype=multipart/form-data method=post>"
PRINT "Choose a file from your local path:<br>"
PRINT "<input name=Datei type=file size=50 maxlength=100000 accept=text/*>"
PRINT "<INPUT type=submit  value=send>"
PRINT "<input type=reset   value=clear>"
PRINT "</FORM>"
PRINT "<hr><h6>(c) Markus Hoffmann cgi with X11-basic</h6></body></html>"
FLUSH
QUIT

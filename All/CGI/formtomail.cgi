#!/usr/bin/xbasic

' Formtomail.cgi (c) Markus Hoffmann 2001
' Das Programm verschickt Emails mit dem Inhalt des Formulars
' Version vom Okt. 2004

' Die Prozedur formtomail.cgi bewirkt, dass die ins Formular
' eingegebenen Parameter mit ihren Werten per Mail an die Adresse
' geschickt werden, die in dem Input-Feld mit dem Namen "Mailto" unter
' "Value" angegeben ist. Die beiden Input-Felder "Mailto" und "Subject"
' muessen genau in der unten spezifizierten Form angegeben werden
' (Gross-/Kleinschreibung bei "Mailto" und "Subject" beachten). Das
' Input-Feld "Subject" ist dafuer da, dass der Benutzer bei mehreren
' verschiedenen Formularen eine Zuordnung vornehmen kann (da es nicht
' moeglich ist, den Text des Formulars mitzuschicken, sondern nur die
' Parameter). Diese beiden Felder haben das Attribut "hidden", damit der
' Benutzer, der das Formular ausfuellt, sie nicht veraendern kann. >

PRINT "Content-type: text/html"+chr$(13)
PRINT ""+chr$(13)
FLUSH
DIM value$(200)
t$=""

PRINT "<html><head></head><body>"
PRINT "Vielen Dank f&uuml;r das Ausf&uuml;llen des Formulars.<p>"
PRINT "Es wird so schnell wie m&ouml;glich bearbeitet...<p>"
PRINT "<pre>"

r$=env$("REMOTE_ADDR")
h$=env$("REMOTE_HOST")
refer$=env$("HTTP_REFERER")
length=val(env$("CONTENT_LENGTH"))

IF length
  FOR i=0 TO length-1
    t$=t$+chr$(inp(-2))
  NEXT i
  orig$=t$
  count=0
  SPLIT t$,"&",1,a$,t$
  WHILE len(t$)
    value$(count)=a$
    INC count
    PRINT a$
    SPLIT t$,"&",1,a$,t$
  WEND
  value$(count)=a$
  INC count
  PRINT a$
ENDIF
IF count
  FOR i=0 TO count-1
    SPLIT value$(i),"=",1,a$,b$
    IF a$="Mailto"
      mailto$=b$
    ENDIF
    IF a$="Subject"
      subject$=b$
    ENDIF
  NEXT i
  mailto$=replace$(mailto$,"%40","@")

  FLUSH
  IF upper$(right$(mailto$,3))=".DE"
    s$="mail -s "+chr$(34)+"[Form-to-Mail]: "+subject$+chr$(34)+" "+mailto$+" << EOF"+chr$(13)
    FOR i=0 TO count-1
      s$=s$+value$(i)+chr$(10)
    NEXT i
    s$=S$+chr$(10)+"EOF"+chr$(10)
    SYSTEM s$
    status$="sent to "+mailto$
  ELSE
    status$="unsent ("+mailto$+")"
  ENDIF
ENDIF
PRINT "</pre>"
PRINT "</body></html>"
FLUSH

OPEN "A",#1,"/tmp/WEBformtomail.log"
PRINT #1,date$+" "+time$+" "+r$+" "+h$+" ";
PRINT #1,"B="+chr$(34)+refer$+chr$(34)+" ";
PRINT #1,"C="+chr$(34)+orig$+chr$(34)+" Status="+status$
CLOSE #1
QUIT

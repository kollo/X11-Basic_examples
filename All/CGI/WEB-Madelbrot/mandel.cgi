#!/usr/bin/xbasic
host$=""
'
' mandel.cgi (c) Markus Hoffmann 1999   V. 1.01
' darf mit dem Paket X11-Basic weitergegeben werden
'

PRINT "Content-type: text/html"
PRINT
t$=env$("REQUEST_URI")
SPLIT t$,"?",1,a$,t$
SPLIT t$,"?",1,t$,u$

IF len(t$)<2
  x1=-2
  y1=-2
  x2=2
  y2=2
ELSE
  SPLIT t$,"&",1,a$,t$
  WHILE len(a$)
    SPLIT a$,"=",1,a$,b$
    IF a$="x1"
      x1=val(b$)
    ELSE if a$="x2"
      x2=val(b$)
    ELSE if a$="y1"
      y1=val(b$)
    ELSE if a$="y2"
      y2=val(b$)
    ENDIF
    SPLIT t$,"&",1,a$,t$
  WEND
ENDIF

IF len(u$)
  SPLIT u$,",",1,x$,y$
  nx=val(x$)/256*(x2-x1)+x1
  ny=(256-val(y$))/256*(y2-y1)+y1
  u=abs(x2-x1)
  x1=nx-u/4
  x2=nx+u/4
  u=abs(y2-y1)
  y1=ny-u/4
  y2=ny+u/4
ENDIF
IF x1=-2
  r$=env$("REMOTE_ADDR")
  h$=env$("REMOTE_HOST")
  OPEN "A",#1,"/tmp/WEBMANDEL.log"
  PRINT #1,date$+" "+time$+" "+r$+" "+h$
  CLOSE #1
ENDIF

gifname$="mandelgif.cgi?x1="+str$(x1)+"&x2="+str$(x2)+"&y1="+str$(y1)+"&y2="+str$(y2)

PRINT "<HTML> <HEAD> <TITLE>Markus Hoffmann's Mandelbrodmenge</TITLE></HEAD>"
PRINT "<BODY bgcolor="#ffffff" link=2200aa vlink=008800>"
PRINT "<h6>(c) Markus Hoffmann 2006"
VERSION
PRINT "</h6>"
PRINT "<center><H1> Die Mandelbrodmenge interaktiv</H1><HR>"
PRINT "<form name=querybox action="+host$+"/cgi-bin/mandel.cgi method=get>"
PRINT "<a href="+host$+"/cgi-bin/mandel.cgi?x1="+str$(x1)+"&x2="+str$(x2)+"&y1="+str$(y1)+"&y2="+str$(y2)+"><img src=/cgi-bin/"+gifname$+" ismap align=left></a>"
PRINT "Klicken Sie in das Bild, um einen Bereich zu vergr&ouml;&szlig;ern. Alternativ k&ouml;nnen Sie die Koordinaten auch eingeben."
PRINT "<table><tr><td>"
PRINT "X1=<input type=text name=x1 value="+str$(x1)+" size=18><br>"
PRINT "X2=<input type=text name=x2 value="+str$(x2)+" size=18><br>"
PRINT "y1=<input type=text name=y1 value="+str$(y1)+" size=18><br>"
PRINT "Y2=<input type=text name=y2 value="+str$(y2)+" size=18><br>"
PRINT "<td>"
PRINT "<input value="+chr$(34)+"Koordinaten Berechnen"+chr$(34)+" type="+chr$(34)+"submit"+chr$(34)+">"
PRINT "<a href="+host$+"/cgi-bin/mandel.cgi?> Zur&uuml;ck zum Start </a><p>"
PRINT "<a href="+host$+"/cgi-bin/mandelposter.cgi?x1="+str$(x1)+"&x2="+str$(x2)+"&y1="+str$(y1)+"&y2="+str$(y2)+"> Ausschnitt als 800x800 Poster </a>"
PRINT "<input type=hidden name=format value=2>"
PRINT "</table>"
PRINT "</form><p></center><HR><br>"
PRINT "<I>Kommentare oder Anregungen zu dieser WWW-Seite bitte "
PRINT "<A HREF=mailto:hoffmann@homenet.all>hierhin</A>.</I><P>"
PRINT "<FONT FACE="+chr$(34)+"ARIAL,HELVETICA"+chr$(34)+" SIZE=1>"
PRINT "Erzeugt am "+time$+" "+date$
PRINT "</FONT></BODY></HTML>"
QUIT

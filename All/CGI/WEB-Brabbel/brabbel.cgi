#!/usr/bin/xbasic
'
' brabbel.cgi von Markus Hoffmann (Jan 2001)
' 2nd revision Jan. 2006
'
host$=""
PRINT "Content-type: text/html"
PRINT
t$=ENV$("REQUEST_URI")
SPLIT t$,"?",1,a$,t$
iq=4
ut=0
SPLIT t$,"&",1,a$,t$
WHILE LEN(a$)
  SPLIT a$,"=",1,b$,a$
  IF b$="url"
    url$=a$
  ELSE IF b$="IQ"
    iq=min(15,max(val(a$),1))
  ELSE IF b$="button"
    IF a$="URL+Testen"
      ut=1
    ENDIF
  ELSE
    PRINT "Unbekannte Option: "+b$+" : "+a$+"<p>"
  ENDIF
  SPLIT t$,"&",1,a$,t$
WEND

url$=@purify$(url$)

tmpfile$="/tmp/brabbeltmp"+str$(stimer)

IF LEN(url$)=0
  url$="http://polarfrost.homenet.all/~hoffmann/texte/party.html"
ENDIF

IF LEFT$(url$,5)="http:"
  SPLIT url$,"//",1,uhost$,url$
ENDIF
SPLIT url$,"/",1,uhost$,url$
url$="/"+url$

PRINT "<HTML> <HEAD> <TITLE>Markus Hoffmann's Webgebrabbel</TITLE></HEAD>"
PRINT "<BODY bgcolor="#ffffff" link=2200aa vlink=008800>"
PRINT "<h6>(c) Markus Hoffmann 2001-2006"
VERSION
PRINT "</h6>"
PRINT "<center><H1> Webgebrabbel</H1><HR>"
PRINT "Geben Sie die Web-Seite ein, &uuml;ber die gebrabbelt werden soll. Der Intelligenzquotient bestimmt, "
PRINT "wie sinnig der Ausgabetext sein soll: 1=unsinnig -- 10=Orginaltext. Werte 3,4,5 sind sinnvoll."
PRINT "<form name=querybox action="+host$+"/cgi-bin/brabbel.cgi method=get>"
PRINT "<font size=2 color=000000>"

PRINT "URL=<input type=text name=url value="+chr$(34)+"http://"+uhost$+url$+chr$(34)+" size=60><br>"
PRINT "Intelligenzquotient=<input type=text name=IQ value="+chr$(34)+str$(iq)+chr$(34)+" size=2>"

r$=ENV$("REMOTE_ADDR")
h$=ENV$("REMOTE_HOST")
OPEN "A",#1,"/tmp/WEBBrabbel.log"
PRINT #1,DATE$+" "+TIME$+" "+r$+" "+h$+" ";
PRINT #1,"UT="+STR$(ut)+" IQ="+STR$(iq)+" ";
PRINT #1,ENCLOSE$("http://"+uhost$+url$)
CLOSE #1

PRINT "<input name=button value="+chr$(34)+"Losbrabbeln"+chr$(34)+" type="+chr$(34)+"submit"+chr$(34)+">"
PRINT "<input name=button value="+chr$(34)+"URL Testen"+chr$(34)+" type="+chr$(34)+"submit"+chr$(34)+">"
PRINT "</font>"
PRINT "</form><p></center><HR><font size=2 color=ff0000>"
FLUSH
s$="wget -O "+tmpfile$+".brab "+chr$(34)+uhost$+url$+chr$(34)+" ; "
s$=s$+"html2text -nobs "+tmpfile$+".brab > "+tmpfile$+" ; "
s$=s$+"rm -f "+tmpfile$+".brab"
SYSTEM s$
IF ut=0
  OPEN "I",#1,tmpfile$
  l=LOF(#1)
  t$=SPACE$(l+1)
  CLOSE #1
  BLOAD tmpfile$,VARPTR(t$),l
  u$=TRIM$(t$)
  READ a$,b$
  WHILE a$<>""
    u$=REPLACE$(u$,a$,b$)
    READ a$,b$
  WEND
  BSAVE tmpfile$,VARPTR(u$),LEN(u$)

  s$="/usr/local/bin/brabbel -intelligenz "+STR$(iq)+" -laenge "+STR$(MIN(l,5000))+" -input "+tmpfile$
ELSE
  s$="echo '</font><font size=2 color=1000ff><pre>' ; cat "+tmpfile$
ENDIF
s$=s$+" ; "+"rm -f "+tmpfile$
SYSTEM s$

PRINT "</font></pre><hr><font size=1>*** Webgebrabbel Version: 1.00 (c) Markus Hoffmann "
PRINT " *** letzte Bearbeitung 5.1.2001 </font><hr>"
PRINT "<I>Kommentare oder Anregungen zu dieser WWW-Seite bitte "
PRINT "<A HREF=mailto:hoffmann@physik.uni-bonn.de>hierhin</A>.</I><P>"
PRINT "<FONT FACE="+chr$(34)+"ARIAL,HELVETICA"+chr$(34)+" SIZE=1>"
PRINT "Erzeugt am "+time$+" "+date$
PRINT "</FONT></BODY></HTML>"
QUIT

DATA "~","-"
DATA "!!","!","??","?","==","=","--","-","**","*"
DATA "",""

FUNCTION purify$(g$)
  LOCAL i
  g$=REPLACE$(g$,"+"," ")
  g$=REPLACE$(g$,"%0A"," ")
  g$=REPLACE$(g$,"%0D"," ")
  FOR i=0 TO 255
    g$=REPLACE$(g$,"%"+UPPER$(HEX$(i,2,2)),CHR$(i))
  NEXT i
  RETURN g$
ENDFUNCTION

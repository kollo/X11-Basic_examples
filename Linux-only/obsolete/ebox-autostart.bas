PRINT "X11-Basic Autostarter for ebox. (c) Markus Hoffmann"

' here we could check if music is connected to USB

DIM mfile$(10000)
CLR anzmfiles

IF NOT EXIST("/tmp/musik.lst")
  SYSTEM "sudo /home/dsl/bin/music-check"
ENDIF

PRINT "reading music list..."

IF EXIST("/tmp/musik.lst")
  OPEN "I",#1,"/tmp/musik.lst"
  WHILE NOT EOF(#1)
    LINEINPUT #1,t$
    mfile$(anzmfiles)="/mnt/"+t$
    INC anzmfiles
  WEND
  CLOSE #1
  PRINT anzmfiles;" Files."
ENDIF

IF anzmfiles
  SYSTEM "txt2snd -s 'Es wurden "+STR$(anzmfiles)+" Musikstuecke gefunden.'"
ENDIF
DO
  @menu
LOOP

IF 0
  PRINT "Autoplay the Music Files:"
  DO
    i=RANDOM(anzmfiles)
    SYSTEM "sudo mpg123 "+CHR$(34)+mfile$(i)+CHR$(34)
    PAUSE 2
    IF INP?(-2)
      a=INP(-2)
      EXIT IF a=27
      EXIT IF a=3
      PRINT a
    ENDIF
  LOOP
ENDIF
QUIT

PROCEDURE menu
  exitflag=0
  PRINT "Wenn Sie einen Bildschirm haben und mit X-session fortfahren wollen,"
  PRINT "Dann druecken Sie ein grosses 'X'"
  @option("Wenn Sie die Zeit wissen wollen, druecken Sie 1")
  WHILE exitflag=0
    PAUSE 0.5
    @checkaction
  WEND
RETURN
PROCEDURE option(t$)
  PRINT t$
  SYSTEM "txt2snd -s '"+t$+"'"
  @checkaction
RETURN
PROCEDURE checkaction
  LOCAL a
  in$=INKEY$
  IF LEN(in$)
    PRINT LEN(in$)
    a=ASC(LEFT$(in$)) AND 255
    IF a=ASC("X")
      QUIT
    ELSE IF a=ASC("1")
      SYSTEM "zeit"
    ELSE
      SYSTEM "txt2snd -s 'unbekannte Taste "+STR$(a)+", versuchen Sie es nochmal.'"
      PRINT "unbekannte Taste "+STR$(a)+". versuchen Sie es nochmal."
      exitflag=1
    ENDIF
  ENDIF
RETURN

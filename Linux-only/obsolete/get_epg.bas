' get_epg.bas   --- get all(!) currently available EPG data from a DVB-T device
' and creates a nice WEB-Page
' (c) Markus Hoffmann 2006
'
'
' needs: tzap, dvb-epg-get, channels.conf

DIM tt$(100000)
anztt=0
DIM eid(10000)
anzeid=0
DIM dates$(100)
anzdates=0
DIM channels$(100)
anzchannels=0
zappid=-1
sender$=""
notune=0
htmldir$="/srv/www/htdocs/epgdata/"
chanfile$=ENV$("HOME")+"/.channels.conf"
i=1
@loadsettings(ENV$("HOME")+"/.DVB-T.rc")
WHILE LEN(PARAM$(i))
  IF LEFT$(PARAM$(i))="-"
    IF PARAM$(i)="--help" OR PARAM$(i)="-h"
      @intro
      @using
    ELSE IF PARAM$(i)="--version"
      @intro
      QUIT
    ELSE IF PARAM$(i)="--channel" OR PARAM$(i)="-c"
      INC i
      IF LEN(PARAM$(i))
        sender$=PARAM$(i)
      ENDIF
    ELSE IF PARAM$(i)="--notune" OR PARAM$(i)="-n"
      notune=true
    ELSE IF PARAM$(i)="--pid" OR PARAM$(i)="-p"
      INC i
      IF LEN(PARAM$(i))
        pid=VAL(PARAM$(i))
      ENDIF
    ELSE IF PARAM$(i)="-o"
      INC i
      IF LEN(PARAM$(i))
        outputfilename$=PARAM$(i)
      ENDIF
    ELSE
      collect$=collect$+PARAM$(i)+" "
    ENDIF
  ELSE
    inputfile$=PARAM$(i)
    IF NOT EXIST(inputfile$)
      PRINT param$(0)+": "+inputfile$+": file or path not found"
      CLR inputfile$
    ENDIF
  ENDIF
  INC i
WEND
IF notune
  sender$="x"
ENDIF
IF NOT EXIST(htmldir$)
  SYSTEM "mkdir "+htmldir$
ENDIF
@loadchannels(chanfile$)

s$=REPLACE$(sender$," ","_")
s$=REPLACE$(s$,"/","_")
s$=REPLACE$(s$,"*","_")
htmlname$=htmldir$+"EPG-"+s$+"-"+DATE$+".html"
SYSTEM "rm -f "+htmldir$+"index.html"
SYSTEM "ln -s -T EPG-"+s$+"-"+DATE$+".html "+htmldir$+"index.html"
data1$="/tmp/EPG-"+s$+"-"+DATE$+".dat"
data2$="/tmp/EPG-"+s$+"-"+DATE$+".dat2"
data3$="/tmp/EPG-"+s$+"-"+DATE$+".dat3"

IF NOT EXIST(data1$)
  PRINT "Get EPG-Data from the DVB-T device:"
  OPEN "O",#1,data1$
  PRINT #1,"# EPGDATA scanned at "+date$+" "+time$
  CLOSE #1
  IF LEN(sender$)
    @dosender(sender$)
  ELSE
    PRINT "rm -f "+htmldir$+"*"+RIGHT$(DATE$,4)+MID$(DATE$,4,2)+LEFT$(DATE$,2)+"*.html"
    SYSTEM "rm -f "+htmldir$+"*"+RIGHT$(DATE$,4)+MID$(DATE$,4,2)+LEFT$(DATE$,2)+"*.html"
    FOR i=0 TO anzchannels-1
      @dosender(channels$(i))
    NEXT i
  ENDIF
  IF zappid<>-1
    SYSTEM "kill "+STR$(zappid)
  ENDIF
ENDIF
@makehtml
QUIT
PROCEDURE intro
  PRINT "get_epg.bas V.1.00 (c) Markus Hoffmann 2006"
  VERSION
RETURN
PROCEDURE using
  PRINT "Usage: "+PARAM$(0)+" [options] "
  PRINT "Options:"
  PRINT "  -h, --help               Display this information"
  PRINT "  -c, --channel  channel   scan only channel     "
  PRINT "  -o <file>                Place the output into <file>"
RETURN

PROCEDURE makehtml
  LOCAL i
  IF NOT EXIST(data3$)
    IF NOT EXIST(data2$)
      OPEN "I",#1,data1$
      WHILE NOT EOF(#1)
        LINEINPUT #1,t$
        t$=TRIM$(t$)
        IF LEN(t$) AND LEFT$(t$)<>"#"
          ee=VAL(@getval$(t$,"EventID"))
          IF @chkeid(ee)=0
            tt$(anztt)=t$
            INC anztt
            eid(anzeid)=ee
            INC anzeid
            PRINT INT(LOC(#1)/LOF(#1)*1000)/10;"  ";CHR$(13);
            FLUSH
          ENDIF
        ENDIF
      WEND
      CLOSE #1
      OPEN "O",#1,data2$
      FOR i=0 TO anztt-1
        PRINT #1,tt$(i)
      NEXT i
      CLOSE #1
    ENDIF
    PRINT "Sorting ..."
    SYSTEM "sort "+data2$+" > "+data3$
    SYSTEM "rm -f "+data2$
  ENDIF
  anztt=0
  OPEN "I",#1,data3$
  WHILE NOT EOF(#1)
    LINEINPUT #1,t$
    tt$(anztt)=t$
    INC anztt
  WEND
  CLOSE #1
  PRINT "Outputting to "+htmlname$
  OPEN "O",#2,htmlname$
  PRINT #2,"<h1>Programm vom "+date$+"</h1>"
  PRINT #2,"<h2>Das l&auml;uft gerade: <a href="+ENCLOSE$("Aktuell.html")+">"+time$+"</a></h2>"
  PRINT #2,"<h2>Filme demn&auml;chst: <a href="+ENCLOSE$("Filme.html")+">"+date$+"</a></h2>"
  @scanactuell
  @collectdates

  FOR j=0 TO anzchannels-1
    a$=channels$(j)
    PRINT #2,"<li><b>"+a$+"</b><td>";
    a$=REPLACE$(a$," ","_")
    a$=REPLACE$(a$,"/","_")
    a$=REPLACE$(a$,"*","_")
    FOR i=0 TO anzdates-1
      dd$=dates$(i)
      dd$=RIGHT$(dd$,2)+"."+MID$(dd$,5,2)
      @scanprg(a$,dates$(i))
      IF EXIST(htmldir$+"EPG-"+a$+"-"+dates$(i)+".html")
        PRINT #2,"<a href="+ENCLOSE$("EPG-"+a$+"-"+dates$(i)+".html")+">";dd$;"</a> ";
      ENDIF
    NEXT i
  NEXT j
  PRINT #2,"<tr>"
  @scanmovie
  t$=SYSTEM$("ls "+htmldir$+"EPG--*.html")
  IF LEN(t$)
    PRINT #2,"<h2>Alte Seiten:</h2>"
    WHILE LEN(t$)
      SPLIT t$,CHR$(10),0,a$,t$
      SPLIT a$,"EPG--",0,b$,a$
      SPLIT a$,".html",0,b$,a$
      PRINT #2,"<a href="+ENCLOSE$("EPG--"+b$+".html")+">"+b$+"</a> "
    WEND
  ENDIF
  PRINT #2,"<p><hr><br><i><font size=1>get_epg.bas V.1.00 "+err$(100)+"</font></i>"
  PRINT #2,"</BODY></HTML>"
  CLOSE #2
  SYSTEM "chmod 644 "+htmldir$+"*.html"
RETURN
PROCEDURE scanprg(s$,d$)
  LOCAL c$,f$,a$,i,count
  a$=REPLACE$(s$," ","_")
  a$=REPLACE$(a$,"/","_")
  a$=REPLACE$(a$,"*","_")
  f$=htmldir$+"EPG-"+a$+"-"+d$+".html"
  IF NOT EXIST(f$)
    PRINT "Scanning "+s$+" "+d$+"..."
    count=0
    OPEN "O",#3,f$
    PRINT #3,"<h1>"+s$+" ("+d$+")</h1>"
    PRINT #3,"<ul>"
    FOR i=0 TO anztt-1
      c$=@getval$(tt$(i),"channel")
      c$=REPLACE$(c$," ","_")
      c$=REPLACE$(c$,"/","_")
      c$=REPLACE$(c$,"*","_")
      IF c$=s$
        c$=@getval$(tt$(i),"start")
        SPLIT c$,";",0,a$,c$
        IF a$=d$
          PRINT #3,"<li> ";
          PRINT #3,@chtml$(tt$(i),1)
          INC count
        ENDIF
      ENDIF
    NEXT i
    PRINT #3,"</ul>"
    CLOSE #3
    IF count=0
      SYSTEM "rm -f "+f$
    ENDIF
  ENDIF
RETURN

PROCEDURE collectdates
  LOCAL c$,i,j,found
  PRINT "Scanning for dates: ";
  FLUSH
  FOR i=0 TO anztt-1
    c$=@getval$(tt$(i),"start")
    @progress(anztt,i)
    IF LEN(c$)
      found=0
      SPLIT c$,";",0,c$,a$
      FOR j=0 TO anzdates-1
        IF dates$(j)=c$
          found=1
          BREAK
        ENDIF
      NEXT j
      IF found=0
        PRINT c$;" ";
        FLUSH
        dates$(anzdates)=c$
        INC anzdates
      ENDIF
    ENDIF
  NEXT i
  PRINT
RETURN
PROCEDURE scanmovie
  LOCAL cat$,c$
  PRINT "Scanning movies..."
  OPEN "O",#1,htmldir$+"Filme.html"
  PRINT #1,"<h1>Filme</h1>"
  PRINT #1,"<ul>"
  FOR i=0 TO anztt-1
    cat$=@getval$(tt$(i),"category")
    IF cat$="Movie / Drama"
      c$=@getval$(tt$(i),"channel")
      PRINT #1,"<li> <font size=4>";c$;"<font> "
      PRINT #1,@chtml$(tt$(i),0)
    ENDIF
  NEXT i
  PRINT #1,"</ul>"
  CLOSE #1
RETURN
PROCEDURE scanactuell
  LOCAL runs,c$,i
  PRINT "Scanning current program..."
  OPEN "O",#1,htmldir$+"Aktuell.html"
  PRINT #1,"<h1>Aktuelles Programm am "+DATE$+" ("+TIME$+")</h1>"
  PRINT #1,"<ul>"
  FOR i=0 TO anztt-1
    @progress(anztt,i)
    runs=VAL(@getval$(tt$(i),"RunningStatus"))
    IF runs>0
      c$=@getval$(tt$(i),"channel")
      PRINT #1,"<li> <font size=4>";c$;"<font> "
      PRINT #1,@chtml$(tt$(i),1)
    ENDIF
  NEXT i
  PRINT #1,"</ul>"
  CLOSE #1
  PRINT
RETURN

FUNCTION chtml$(g$,nodat)
  LOCAL dd$,tt$,start$,stop$,runs,cat$,title$,des$,dur$
  back$=""
  start$=@getval$(g$,"start")
  SPLIT start$,";",0,dd$,tt$
  IF RIGHT$(tt$,3)=":00"
    tt$=LEFT$(tt$,LEN(tt$)-3)
  ENDIF
  IF nodat
    start$=tt$
  ELSE
    start$=RIGHT$(dd$,2)+"."+MID$(dd$,5,2)+" "+tt$
  ENDIF
  stop$=@getval$(g$,"stop")
  SPLIT stop$,";",0,dd$,tt$
  IF RIGHT$(tt$,3)=":00"
    tt$=LEFT$(tt$,LEN(tt$)-3)
  ENDIF
  stop$=tt$

  runs=VAL(@getval$(g$,"RunningStatus"))
  IF runs=4
    back$=back$+"<font color=#800000>"
  ELSE IF runs=1
    back$=back$+"<font color=#00ff00>"
  ELSE IF runs=2
    back$=back$+"<font color=#ff0000>"
  ELSE IF runs=3
    back$=back$+"<font color=#808000>"
  ELSE
    back$=back$+"<font>"
  ENDIF
  title$=@getval$(g$,"TITLE")
  des$=@getval$(g$,"des")
  dur$=@getval$(g$,"duration")
  IF RIGHT$(dur$,3)=":00"
    dur$=LEFT$(dur$,LEN(dur$)-3)
    IF LEFT$(dur$,2)="0:"
      dur$=RIGHT$(dur$,2)+"m"
    ELSE
      dur$=REPLACE$(dur$,":","h")
    ENDIF
  ENDIF

  back$=back$+"<big>"+start$+"</big> <b>"+title$+"</b> "
  IF des$<>title$
    back$=back$+des$+"  "
  ENDIF
  back$=back$+@getval$(g$,"EventID")+" "+dur$+" "

  back$=back$+"<font size=2>"
  back$=back$+" (bis "+stop$+") "+@getval$(g$,"video_aspect")+" "+@getval$(g$,"stereo")+" "
  back$=back$+"</font>"
  cat$=@getval$(g$,"category")
  IF cat$="Movie / Drama"
    back$=back$+"<font color=#ff0000 size=2>"+cat$+"</font> "
  ELSE
    back$=back$+"<font color=#ff8000 size=2>"+cat$+"</font> "
  ENDIF
  back$=back$+"</font>"
  info$=@getval$(g$,"Info")
  IF LEN(info$)
    back$=back$+"<br>"+info$
  ENDIF
  RETURN back$
ENDFUNCTION

FUNCTION chkeid(id)
  LOCAL i
  FOR i=0 TO anzeid-1
    IF id=eid(i)
      RETURN 1
    ENDIF
  NEXT i
  RETURN 0
ENDFUNCTION

PROCEDURE dosender(s$)
  IF notune=0
    PRINT "Tuneto "+s$
    OPEN "A",#2,data1$
    PRINT #2,""
    PRINT #2,"# "+S$+" "+DATE$+" "+TIME$
    CLOSE #2
    @tuneto(s$)
  ENDIF
  SYSTEM "dvb-epg-get >> "+data1$
RETURN

PROCEDURE tuneto(sender$)
  IF zappid<>-1
    SYSTEM "kill "+STR$(zappid)
  ENDIF
  SYSTEM "xterm -geometry 80x12 -bg \#a3ff60 -sb -e 'tzap -r "+ENCLOSE$(sender$)+"' &"
  PAUSE 4
  zappid=@getpid("tzap")
  IF zappid=-1
    PRINT "ERROR"
    QUIT
  ENDIF
RETURN
FUNCTION getunixtime(dat$,tim$)
  LOCAL h,m,s
  h=VAL(LEFT$(tim$,2))
  m=VAL(MID$(tim$,4,2))
  s=VAL(RIGHT$(tim$,2))
  RETURN (JULIAN(dat$)-JULIAN("01.01.1970"))*24*60*60+(h-1)*3600+m*60+s
ENDFUNCTION
FUNCTION getpid(f$)
  LOCAL t$,pid,a$
  pid=-1
  t$=SYSTEM$("ps x")

  WHILE LEN(t$)
    SPLIT t$,CHR$(10),0,a$,t$
    IF MID$(a$,28,LEN(f$))=f$
      pid=VAL(LEFT$(a$,6))
    ENDIF
  WEND
  RETURN pid
ENDFUNCTION
FUNCTION getval$(t$,f$)
  LOCAL a$,val$
  val$=""
  SPLIT t$," ",1,a$,t$
  WHILE LEN(a$)
    a$=TRIM$(a$)
    SPLIT a$,"=",1,name$,val$
    EXIT IF UPPER$(name$)=UPPER$(f$)
    val$=""
    SPLIT t$," ",1,a$,t$
  WEND
  IF LEFT$(val$)=CHR$(34)
    val$=DECLOSE$(val$)
  ENDIF
  RETURN val$
ENDFUNCTION
PROCEDURE progress(a,b)
  LOCAL t$
  PRINT CHR$(13);"[";STRING$(b/a*32,"-");">";STRING$((1.03-b/a)*32,"-");"| ";STR$(INT(b/a*100),3,3);"% ]";
  FLUSH
RETURN
PROCEDURE loadsettings(f$)
  LOCAL t$,a$,b$
  IF EXIST(f$)
    OPEN "I",#9,f$
    WHILE NOT EOF(#9)
      LINEINPUT #9,t$
      t$=TRIM$(t$)
      IF LEFT$(t$)<>"#"
        SPLIT t$,"=",1,a$,b$
        IF UPPER$(a$)="DEVICE"
          devicename$=b$
        ELSE IF UPPER$(a$)="CHANNEL"
          sender$=b$
        ELSE IF UPPER$(a$)="EPG_DATA_DIR"
          htmldir$=b$
        ENDIF
      ENDIF
    WEND
    CLOSE #9
  ENDIF
RETURN
PROCEDURE loadchannels(f$)
  LOCAL t$,a$
  PRINT "load channels [";
  IF EXIST(f$)
    OPEN "I",#9,f$
    WHILE NOT EOF(#9)
      LINEINPUT #9,t$
      t$=TRIM$(t$)
      IF LEFT$(t$)<>"#"
        SPLIT t$,":",0,a$,t$
        channels$(anzchannels)=a$
        INC anzchannels
        PRINT ".";
      ENDIF
    WEND
    CLOSE #9
    PRINT "]"
  ELSE
    PRINT "ERROR: could not load "+f$
    QUIT
  ENDIF
RETURN

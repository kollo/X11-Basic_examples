' Automatically format all X11-basic files
'
'
' needs basformat.bas, a three-way merge tool like meld
'
'
'
formatter$="/home/hoffmann/c/X11-Basic_examples/All/Utilities/basformat.bas"
merger$="meld"
path$=DIR$(0)
mask$="*.bas"   ! list only .bas files

DIM files$(100000)
DIM dirs$(10000)
anzfiles=0
anzdir=1
dirs$(0)=path$   ! This is the starting directory
dirpointer=0
WHILE dirpointer<anzdir
  ON ERROR CONT            ! Skip any error like permission denied or so....
  a$=FSFIRST$(dirs$(dirpointer),"*")
  WHILE len(a$)
    SPLIT a$," ",0,typ$,name$
    IF typ$="d"                ! Is it a directory?
      IF name$<>"." AND name$<>".."
        dirs$(anzdir)=dirs$(dirpointer)+"/"+name$
        INC anzdir
      ENDIF
    ELSE
      IF GLOB(name$,mask$)            ! Check if the filename matches the pattern...
        files$(anzfiles)=dirs$(dirpointer)+"/"+name$
        INC anzfiles
      ENDIF
    ENDIF
    ON ERROR CONT          ! Skip any error like permission denied or so....
    a$=FSNEXT$()
  WEND
  INC dirpointer
WEND
PRINT "Found ";anzfiles;" files in ";anzdir;" directories."
' Now list all files found (with full path name)
IF anzfiles>0
  FOR i=0 TO anzfiles-1
    PRINT files$(i)
    @doit(files$(i))
  NEXT i
ENDIF
PRINT "Found ";anzfiles;" files in ";anzdir;" directories."
QUIT

PROCEDURE doit(file$)
  CHDIR "/tmp"
  IF EXIST("/tmp/a.bas")
    KILL "/tmp/a.bas"
  ENDIF
  SYSTEM "xbasic "+formatter$+" "+file$
  w1$=WORD$(SYSTEM$("md5sum /tmp/a.bas"),1)
  w2$=WORD$(SYSTEM$("md5sum "+file$),1)
  PRINT w1$,w2$
  IF w1$=w2$
    PRINT "files are identical."
  ELSE
    SYSTEM merger$+" a.bas "+file$
  ENDIF
RETURN

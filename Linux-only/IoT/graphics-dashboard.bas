'
' Example program to demonstrate the Usage together with MQTT
'
' This implements a simple dashbord to display the values
' produced by a temperature sensor.
' (c) Markus Hoffmann 2019 
' 
'
BROKER "tcp://localhost:1883"   ! This will use the local broker, e.q. mosquitto
yellow=COLOR_RGB(1,1,0)
black=COLOR_RGB(0,0,0)
white=COLOR_RGB(1,1,1)
groesse=0.5
offset=22
faktor=1000
weiss=GET_COLOR(65535,65535,65535)
grau=GET_COLOR(65535/1.2,65535/1.2,65535/1.2)
schwarz=GET_COLOR(0,0,0)
rot=GET_COLOR(65535,0,0)

SIZEW ,400,400
CLEARW 
@button(100,20,"QUIT",0)
@button(200,20,"Zero",0)
@button(300,20,"Funktion",0)
COLOR white

TEXT 10,10,"Sensor Dashboard"
TEXT 8,3*16,"Temperature:     °C"
TEXT 8,4*16,"Humidity:        %"
TEXT 8,5*16,"Dew Point:       °C"
TEXT 8,7*16,"Status:"
TEXT sx,sy-16,"Sinus:"

DIM x(100),y(100),tops$(100)
DIM fl(100) ! field length
anztops=0
CLS
READ a
WHILE a>0
  READ b
  READ t$
  READ f
  x(anztops)=a
  y(anztops)=b
  fl(anztops)=f
  tops$(anztops)=t$
  INC anztops
  SUBSCRIBE t$,,callback
  READ a
wend

sx=10
sy=180
SUBSCRIBE "SINUS",,sinus

DO
'  MOUSEEVENT x,y,k
PAUSE 0.5
  IF k
    IF @inbutton(100,20,"QUIT",x,y)
      @button(100,20,"QUIT",1)      
      SHOWPAGE
      PAUSE 0.5
      QUIT
    ELSE IF @inbutton(200,20,"Zero",x,y)
      PUBLISH "CMD","zero"
    ELSE IF @inbutton(300,20,"Funktion",x,y)
      PUBLISH "CMD","func"
    ENDIF
  ENDIF
LOOP
QUIT


DATA 120,48,"TEMPERATURE",3
DATA 120,64,"HUMIDITY",3
DATA 120,80,"DEWPOINT",3
DATA 144,32,"ACTIVITY",1
DATA 144,152,"COUNTER",8
DATA 120,112,"STATUS",20
DATA 200,55,"TIME",8
DATA 8,128,"CMD",10
DATA -1

PROCEDURE callback(topic$,message$)
  LOCAL i
  FOR i=0 TO anztops-1
    IF topic$=tops$(i)
      COLOR yellow
      TEXT x(i),y(i),message$
      SHOWPAGE
      if topic$="TEMPERATURE"
        @display(VAL(message$))
      endif
    ENDIF
  NEXT i
RETURN
PROCEDURE sinus(topic$,message$)
  local v
  v=VAL(message$)
  color black
  PBOX sx,sy,sx+100,sy+16
  color yellow
  PBOX sx+50,sy,sx+50+v*50,sy+16
  color white
  BOX sx,sy,sx+100,sy+16
  SHOWPAGE
RETURN



PROCEDURE display(wert)
  COLOR schwarz
  PBOX 20,192,800*groesse+200,200*groesse+300
  COLOR weiss
  DEFLINE ,25*groesse,2
  DEFTEXT 1,groesse,2*groesse
  LTEXT 400-LTEXTLEN(STR$(wert)),200,STR$(wert)
  SHOWPAGE
RETURN



' Malt einen Button mit text
' sel=1 --> Der Button wird selektiert dargestellt
PROCEDURE button(button_x,button_y,button_t$,sel)
  LOCAL x,y,w,h
  DEFLINE ,1
  DEFTEXT 1,0.05,0.1,0
  button_l=ltextlen(button_t$)
  x=button_x-button_l/2-10
  y=button_y-10
  w=button_l+20
  h=20
  COLOR grau
  PBOX x+5,y+5,x+w+5,y+h+5
  COLOR abs(sel)*schwarz+abs(not sel)*weiss
  PBOX x,y,x+w,y+h
  IF sel=-1
    COLOR weiss
  ELSE
    COLOR schwarz
  ENDIF
  BOX x,y,x+w,y+h
  BOX x-1,y-1,x+w+1,y+h+1
  LTEXT button_x-button_l/2,button_y-5,button_t$
RETURN


FUNCTION inbutton(button_x,button_y,button_t$,mx,my)
  LOCAL x,y,w,h,button_l

  DEFTEXT 1,0.05,0.1,0
  button_l=ltextlen(button_t$)
  x=button_x-button_l/2-10
  y=button_y-10
  w=button_l+20
  h=20
  IF mx>=x AND my>=y AND mx<=x+w AND my<=y+h
    RETURN TRUE
  ELSE
    RETURN FALSE
  ENDIF
ENDFUNC



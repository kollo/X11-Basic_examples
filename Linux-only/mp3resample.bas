#!/usr/bin/xbasic
' Resample the mp3 input file to 44100 kHz and stereo
' so that it can be burned to an audio CD
' (c) Markus Hoffmann 2008
'
' needs mpg123, lame, sox
'
tmpdir$="/tmp/"
tmpdir$="./"

tmpfile1$=tmpdir$+"delme1.wav"
tmpfile2$=tmpdir$+"delme2.wav"

outputfilename$="a.mp3"

WHILE LEN(PARAM$(i))
  IF LEFT$(PARAM$(i))="-"
    IF PARAM$(i)="--help" OR PARAM$(i)="-h"
      @intro
      @using
    ELSE IF PARAM$(i)="--version"
      @intro
      QUIT
    ELSE IF PARAM$(i)="-o"
      INC i
      IF LEN(PARAM$(i))
        outputfilename$=PARAM$(i)
      ENDIF
    ENDIF
  ELSE
    inputfile$=PARAM$(i)
  ENDIF
  INC i
WEND
IF LEN(inputfile$)
  IF NOT EXIST(inputfile$)
    PRINT "mp3resample: "+inputfile$+": file or path not found"
    CLR inputfile$
    QUIT
  ENDIF
  IF EXIST(outputfilename$)
    PRINT "mp3resample: Outputfilename already exists: ";outputfilename$
  ELSE
    @convert
  ENDIF
ELSE
  PRINT "mp3resample: No input files"
ENDIF
QUIT
PROCEDURE intro
  PRINT "mp3resample V.1.10 (c) Markus Hoffmann 2008-2008"
  VERSION
RETURN
PROCEDURE using
  PRINT "Usage: mp3resample [options] file..."
  PRINT "Options:"
  PRINT "  -h, --help		    Display this information"
  PRINT "  -o <file>		    Place the output into <file>"
RETURN

PROCEDURE convert
  PRINT "Decode mp3: "+inputfile$
  PRINT "## mpg123 "+inputfile$+" -w "+tmpfile1$
  SYSTEM "mpg123 "+inputfile$+" -w "+tmpfile1$
  PRINT "resample audio..."
  PRINT "##sox "+tmpfile1$+" -r 44100 -c2 "+tmpfile2$
  SYSTEM "sox "+tmpfile1$+" -r 44100 -c2 "+tmpfile2$
  SYSTEM "rm -f "+tmpfile1$
  PRINT "encoding mp3..."
  PRINT "##lame "+tmpfile2$+" "+outputfilename$
  SYSTEM "lame "+tmpfile2$+" "+outputfilename$
  SYSTEM "rm -f "+tmpfile2$
RETURN

' This program is supposed to create a .fig file with a
' bar graph of content contained in a .csv file exported from
' a Project management tool.
' (c) by Markus Hoffmann 2013 for DESY
'
'
DIM zeile$(2000)
DIM id(2000)
DIM ispath(2000)
DIM level(2000)
DIM typ$(2000)
DIM name$(2000)
DIM duration$(2000)
DIM pred$(2000)
DIM complete(2000)
DIM wbs$(2000)
DIM slack$(2000)
DIM milestone(2000)
DIM d$(2000)

anzzeile=0

DIM blacklist$(100)
anzblacklist=0

prelevel=0
DIM prelevelid$(100)
prelevelid$(0)="0"

i=1
CLR inputfile$,collect$
outputfilename$="x.out"
WHILE LEN(PARAM$(i))
  IF LEFT$(PARAM$(i))="-"
    IF param$(i)="--help" OR PARAM$(i)="-h"
      '
    ELSE
      collect$=collect$+PARAM$(i)+" "
    ENDIF
  ELSE
    inputfile$=PARAM$(i)
    IF NOT EXIST(inputfile$)
      PRINT "doit: "+inputfile$+": file or path not found"
      CLR inputfile$
    ENDIF
  ENDIF
  INC i
WEND
IF LEN(inputfile$)=0
  QUIT
ENDIF

OPEN "O",#4,"a.fig"
PRINT #4,"#FIG 3.2"
PRINT #4,"Landscape"
PRINT #4,"Center"
PRINT #4,"Metric"
PRINT #4,"A4"
PRINT #4,"100.00"
PRINT #4,"Single"
PRINT #4,"-2"
PRINT #4,"1200 2"
@ps_dicke(1)
@ps_font(1)
@ps_color(0)
@ps_bcolor(7)
@ps_tmode(0)
@ps_groesse(9)
@ps_depth(50)
@ps_text(100,0,"created by Markus Hoffmann "+DATE$)

OPEN "I",#1,inputfile$
LINEINPUT #1,t$
WHILE NOT EOF(#1)
  LINEINPUT #1,t$
  zeile$(anzzeile)=t$
  SPLIT t$,CHR$(9),0,a$,t$
  id(anzzeile)=VAL(a$)
  SPLIT t$,CHR$(9),0,a$,t$
  SPLIT t$,CHR$(9),0,a$,t$
  name$(anzzeile)=a$
  SPLIT t$,CHR$(9),0,a$,t$
  duration$(anzzeile)=a$
  SPLIT t$,CHR$(9),0,a$,t$
  typ$(anzzeile)=a$
  '  print a$
  SPLIT t$,CHR$(9),0,a$,t$
  level(anzzeile)=VAL(a$)
  '  print level,prelevel

  SPLIT t$,CHR$(9),0,a$,t$
  SPLIT t$,CHR$(9),0,a$,t$
  pred$(anzzeile)=a$
  SPLIT t$,CHR$(9),0,a$,t$
  d$(anzzeile)=a$
  SPLIT t$,CHR$(9),0,a$,t$
  SPLIT t$,CHR$(9),0,a$,t$
  SPLIT t$,CHR$(9),0,a$,t$
  SPLIT t$,CHR$(9),0,a$,t$
  SPLIT t$,CHR$(9),0,a$,t$
  SPLIT t$,CHR$(9),0,a$,t$
  slack$(anzzeile)=a$
  SPLIT t$,CHR$(9),0,a$,t$
  slack$(anzzeile)=a$
  SPLIT t$,CHR$(9),0,a$,t$
  SPLIT t$,CHR$(9),0,a$,t$
  IF right$(a$)="%"
    a$=LEFT$(a$,LEN(a$)-1)
  ENDIF
  complete(anzzeile)=VAL(a$)
  SPLIT t$,CHR$(9),0,a$,t$
  SPLIT t$,CHR$(9),0,a$,t$
  SPLIT t$,CHR$(9),0,a$,t$
  SPLIT t$,CHR$(9),0,a$,t$

  SPLIT t$,CHR$(9),0,a$,t$
  SPLIT t$,CHR$(9),0,a$,t$
  SPLIT t$,CHR$(9),0,a$,t$
  SPLIT t$,CHR$(9),0,a$,t$
  SPLIT t$,CHR$(9),0,a$,t$
  SPLIT t$,CHR$(9),0,a$,t$
  SPLIT t$,CHR$(9),0,a$,t$
  SPLIT t$,CHR$(9),0,a$,t$
  SPLIT t$,CHR$(9),0,a$,t$
  SPLIT t$,CHR$(9),0,a$,t$
  SPLIT t$,CHR$(9),0,a$,t$
  SPLIT t$,CHR$(9),0,a$,t$
  SPLIT t$,CHR$(9),0,a$,t$
  SPLIT t$,CHR$(9),0,a$,t$
  wbs$(anzzeile)=a$
  SPLIT t$,CHR$(9),0,a$,t$
  SPLIT t$,CHR$(9),0,a$,t$
  IF a$="Yes"
    milestone(anzzeile)=true
  ELSE
    milestone(anzzeile)=false
  ENDIF
  SPLIT t$,CHR$(9),0,a$,t$
  SPLIT t$,CHR$(9),0,a$,t$
  SPLIT t$,CHR$(9),0,a$,t$
  SPLIT t$,CHR$(9),0,a$,t$
  SPLIT t$,CHR$(9),0,a$,t$
  IF glob(wbs$(anzzeile),"*") AND NOT glob(wbs$(anzzeile),"2.[123]") \
    AND not glob(wbs$(anzzeile),"1.3.1[456790]")
    INC anzzeile
  ENDIF
WEND
PRINT "/* "+inputfile$+" read, "+STR$(anzzeile)+" lines */"

@ps_depth(51)
FOR i=0 TO 52
  @ps_text(@kt(i*7),@kl(-0.2),str$(i))
  @ps_color(29)
  @ps_line(@kt(i*7),@kl(0),@kt(i*7),300)
  @ps_color(1)
NEXT i

FOR i=0 TO 52
  @ps_text(@kt(i*7+365),@kl(-0.2),str$(i))
  @ps_color(28)
  @ps_line(@kt(i*7+365),@kl(0),@kt(i*7+365),300)
  @ps_color(1)
NEXT i
@ps_depth(50)

@ps_font(14)

FOR i=0 TO anzzeile-1
  ' if glob(wbs$(i),"9*") and level(i)<4
  IF len(wbs$(i))>0
    PRINT wbs$(i);
    @ps_font(16)
    @ps_text(0,@kl(i+0.8),wbs$(i))
    @ps_font(20)

    ' Jetzt bei Sammlern die anzahl der untergeordneten Zeilen finden

    a$=wbs$(i)
    pos=LEN(a$)
    FOR j=i TO anzzeile-1
      EXIT if MID$(wbs$(j),pos,1)<>right$(a$)
    NEXT j
    PRINT i,j,"sammler=";j-i

    sammler=j-i

    ' pause 1
    si$=""
    IF left$(slack$(i),1)="-"
      si$=si$+"!"
    ELSE if LEFT$(slack$(i),3)="0 d"
      si$=si$+"*"
    ELSE
      SPLIT slack$(i)," ",0,a$,b$
      fff=0
      IF b$="dys" OR b$="dys?" OR b$="dy" OR b$="dy?"
        fff=1
      ELSE if b$="wks" OR b$="wks?" OR b$="wk" OR b$="wk?"
        fff=5
      ELSE if b$="mons" OR b$="mons?"
        fff=21
      ELSE
        ' print "***** "+a$+" "+b$
      ENDIF
      IF fff*val(a$)<22
        si$=si$+"+"
      ENDIF
    ENDIF
    IF milestone(i)
      si$=si$+"x"
    ENDIF
    IF len(si$)
      si$="("+si$+")"
    ENDIF
    IF complete(i)=100
      si$=si$+"e"
    ELSE if complete(i)>0
      si$=si$+"."
    ENDIF
    PRINT left$(" "+si$+"      ",7);
    @ps_text(14,@kl(i+0.8),si$)

    ' for j=0 to level(i)
    ' print " ";
    ' next j

    PRINT name$(i)+" ",d$(i);
    @ps_text(20,@kl(i+0.8),name$(i))
    @ps_text(80,@kl(i+0.8),d$(i))
    IF milestone(i)=0
      PRINT " "+duration$(i);
      @ps_text(100,@kl(i+0.8),duration$(i))
    ENDIF
    IF complete(i)=100
      PRINT " (erledigt)";
      ' @ps_text(112,@kl(i+0.8),"(done)")
    ELSE if complete(i)>0
      PRINT " (laeuft)";
      @ps_text(112,@kl(i+0.8),"(in progress)")
    ENDIF
    PRINT
    dd$=WORD$(d$(i),2)
    dd$=LEFT$(dd$,LEN(dd$)-2)+"20"+RIGHT$(dd$,2)
    js=JULIAN(dd$)-julian("01.01.2015")
    IF js>0
      IF milestone(i)
        IF complete(i)=100
          @ps_color(2)
        ELSE
          @ps_color(4)
        ENDIF
        @ps_circle(@kt(js),@kl(i),1)
        @ps_text(@kt(js)+3,@kl(i+0.3),d$(i))
      ELSE
        IF complete(i)=100
          @ps_color(2)
        ELSE
          @ps_color(4)
        ENDIF
        dd1$=WORD$(duration$(i),1)
        dd2$=WORD$(duration$(i),2)
        jd=VAL(dd1$)
        IF dd2$="wk"
          MUL jd,7
        ELSE if dd2$="wks"
          MUL jd,7
        ELSE if dd2$="dys"
          MUL jd,1
          ADD jd,2*int(jd/5)
        ELSE
          PRINT dd1$,dd2$
          PAUSE 1
        ENDIF
        IF sammler>1
          @ps_ctext(@kt(js+jd/2),@kl(i+0.8),name$(i))
          @ps_pbox(@kt(js),@kl(i+0.1),@kt(js+jd),@kl(i+sammler-0.1))
        ELSE
          @ps_pbox(@kt(js),@kl(i+0.1),@kt(js+jd),@kl(i+1-0.1))
          @ps_tcolor(1)
          @ps_ctext(@kt(js+jd/2),@kl(i+0.8),duration$(i))
          @ps_tcolor(0)
        ENDIF
      ENDIF
    ENDIF
    ' " (#"+str$(id(i))+")"
  ENDIF
  ' endif
NEXT i
QUIT

FUNCTION bl(tt$)
  LOCAL i
  FOR i=0 TO anzblacklist-1
    IF blacklist$(i)=tt$
      RETURN true
    ENDIF
  NEXT i
  RETURN false
ENDFUNCTION

FUNCTION namebreak$(tt$)
  LOCAL a$,b$,c$
  IF LEN(tt$)<20
    RETURN tt$
  ENDIF
  a$=RIGHT$(tt$,LEN(tt$)-20)
  SPLIT a$," ",0,b$,c$
  RETURN left$(tt$,20)+b$+"\n"+c$
ENDFUNCTION

PROCEDURE ps_line(x1,y1,x2,y2)
  @mmm
  PRINT #4,"2 1 0 "+STR$(dicken)+" "+STR$(colorn)+" 7 "+STR$(depth)+" 0 -1 0.000 0 0 -1 0 0 2"
  PRINT #4," "+STR$(x1)+" "+STR$(y1)+" "+STR$(x2)+" "+STR$(y2)
RETURN
PROCEDURE ps_dotline(x1,y1,x2,y2)
  @mmm
  PRINT #4,"2 1 2 "+STR$(dicken)+" "+STR$(colorn)+" 7 0 0 -1 3.000 0 0 -1 0 0 2"
  PRINT #4," "+STR$(x1)+" "+STR$(y1)+" "+STR$(x2)+" "+STR$(y2)
RETURN
PROCEDURE ps_box(x1,y1,x2,y2)
  @mmm
  PRINT #4,"2 1 0 "+STR$(dicken)+" "+STR$(colorn)+" 7 0 0 -1 0.000 0 0 -1 0 0 5"
  PRINT #4," "+STR$(x1)+" "+STR$(y1)+" "+STR$(x2)+" "+STR$(y1);
  PRINT #4," "+STR$(x2)+" "+STR$(y2)+" "+STR$(x1)+" "+STR$(y2);
  PRINT #4," "+STR$(x1)+" "+STR$(y1)
RETURN
PROCEDURE ps_pbox(x1,y1,x2,y2)
  @mmm
  PRINT #4,"2 1 0 "+STR$(dicken)+" "+STR$(colorn)+" "+STR$(bcolorn)+" 50 0 20 0.000 0 0 -1 0 0 5"
  PRINT #4," "+STR$(x1)+" "+STR$(y1)+" "+STR$(x2)+" "+STR$(y1);
  PRINT #4," "+STR$(x2)+" "+STR$(y2)+" "+STR$(x1)+" "+STR$(y2);
  PRINT #4," "+STR$(x1)+" "+STR$(y1)
RETURN
PROCEDURE ps_circle(x1,y1,r)
  LOCAL x2,y2
  x2=x1+2*r
  y2=y1+2*r
  ' print "----"
  ' print x1,y2,x2,y2
  @mmm
  ' print "---"
  PRINT #4,"1 3 0 "+STR$(dicken)+" "+STR$(colorn)+" 7 50 0 20 0.000 1 0 ";
  PRINT #4," "+STR$(x1)+" "+STR$(y1)+" "+STR$(INT(r*45))+" "+STR$(INT(r*45));
  PRINT #4," "+STR$(x1)+" "+STR$(y1)+" "+STR$(x2)+" "+STR$(y2)
RETURN
PROCEDURE ps_text(x1,y1,t$)
  @mmm
  PRINT #4,"4 "+STR$(tmoden)+" 0 40 0 "+STR$(fontn)+" "+STR$(groessen)+" "+STR$(anglen,6,6)+" 4 165 4830 "+STR$(INT(x1))+" "+STR$(INT(y1))+" "+t$+"\001"
RETURN
PROCEDURE ps_ctext(x1,y1,t$)
  @mmm
  PRINT #4,"4 "+STR$(1)+" "+STR$(tcolor)+" 40 0 "+STR$(fontn)+" "+STR$(groessen)+" "+STR$(anglen,6,6)+" 4 165 4830 "+STR$(INT(x1))+" "+STR$(INT(y1))+" "+t$+"\001"
RETURN
PROCEDURE ps_picture(x1,y1,x2,y2,t$)
  @mmm
  PRINT #4,"2 5 0 1 0 -1 "+" 50 -1 -1 0.000 0 0 -1 0 0 5"
  PRINT #4,"	    0 "+t$
  PRINT #4,"	     "+STR$(x1)+" "+STR$(y1)+" "+STR$(x2)+" "+STR$(y1);
  PRINT #4," "+STR$(x2)+" "+STR$(y2)+" "+STR$(x1)+" "+STR$(y2);
  PRINT #4," "+STR$(x1)+" "+STR$(y1)
RETURN
PROCEDURE mmm
  MUL x1,45
  MUL y1,45
  MUL x2,45
  MUL y2,45
  x1=INT(x1)
  y1=INT(y1)
  x2=INT(x2)
  y2=INT(y2)
RETURN

PROCEDURE ps_color(c)
  colorn=c
RETURN
PROCEDURE ps_bcolor(c)
  bcolorn=c
RETURN
PROCEDURE ps_tcolor(c)
  tcolor=c
RETURN
PROCEDURE ps_pattern(c)
  patternn=c
RETURN
PROCEDURE ps_dicke(c)
  dicken=c
RETURN
PROCEDURE ps_angle(c)
  anglen=c/180*pi
RETURN
PROCEDURE ps_groesse(c)
  groessen=c
RETURN
PROCEDURE ps_tmode(c)
  tmoden=c
RETURN
PROCEDURE ps_font(c)
  fontn=c
RETURN
PROCEDURE ps_depth(c)
  depth=c
RETURN

DEFFN kt(x)=130+x/7*2
DEFFN kl(x)=20+x*4

blacklist:

DATA "Incoming"
DATA "Outgoing"
DATA "Buildings"
DATA "Injector Milestones",
DATA "Linac Milestones"
DATA "dummy for critical path"
DATA "Electron Beamline Milestones"
DATA "Photon Beamline Milestones"
DATA "Instrument Milestones"
DATA "Sonstiges"
DATA "Transportation"
DATA "Dumps"
DATA "Cryomodules"
DATA "Software releases"
DATA "Undulator Systems"
DATA "Cryo System"
DATA "Instrument available"
DATA "Cryo"
DATA "X-HE2-S2","X-HE3-S3","X-HE4-S4","XCIN"
DATA "***"

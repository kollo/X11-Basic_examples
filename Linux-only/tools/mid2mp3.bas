' *.mid to *.mp3 converter  V.1.01     (c) Markus Hoffmann
'
' Letzte Bearbeitung 02/2009
'
' needs timidity and lame
'
tmpfile$="/tmp/tst.wav"
verbose=1
i=1
WHILE LEN(PARAM$(i))
  IF LEFT$(PARAM$(i))="-"
  ELSE
    inputfile$=PARAM$(i)
    IF NOT EXIST(inputfile$)
      PRINT PARAM$(0)+": "+inputfile$+": file or path not found"
      CLR inputfile$
    ELSE
      @doit
    ENDIF
  ENDIF
  INC i
WEND
QUIT
PROCEDURE doit
  IF LEN(inputfile$)
    PRINT "<-- ";inputfile$
    rumpf$=inputfile$
    WHILE LEN(rumpf$)
      SPLIT rumpf$,"/",1,a$,rumpf$
    WEND
    rumpf$=a$
    IF NOT EXIST(tmpfile$)
      PRINT "nice timidity -Ow -o "+tmpfile$+" "+ENCLOSE$(inputfile$)
      SYSTEM "nice timidity -Ow -o "+tmpfile$+" "+ENCLOSE$(inputfile$)
    ENDIF
    SYSTEM "nice lame "+tmpfile$
    IF EXIST(tmpfile$+".mp3")
      SYSTEM "rm "+tmpfile$
    ELSE
      PRINT "ERROR: QUIT."
      QUIT
    ENDIF
    SYSTEM "mv "+tmpfile$+".mp3 "+STR$(i,2,2,1)+".mp3"
    ' system "mp3info -l "+ENCLOSE$(l$)+" -a "+ENCLOSE$(a$)+" -n "+str$(i)+" "+str$(i,2,2,1)+".mp3"
    SYSTEM "mp3info -t "+ENCLOSE$(rumpf$)+" "+STR$(i,2,2,1)+".mp3"
    SYSTEM "mp3info -c "+ENCLOSE$("mid2mp3 (c) MH 2006")+" "+STR$(i,2,2,1)+".mp3"
    ' system "mv "+str$(i,2,2,1)+".mp3 "+ENCLOSE$("`mp3info -p ")+"%n_%a-%t.mp3"+ENCLOSE$(" "+str$(i,2,2,1)+".mp3`")
  ENDIF
RETURN

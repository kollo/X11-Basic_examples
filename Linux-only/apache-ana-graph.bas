' takes an access.log from apache and generates a graph showing
' the browsing path of the users. (c) Markus Hoffmann 2010-03-30
'
' needs graphviz
'
' Zeichnet einen Uebersichtgraph aus den Zugriffdaten der Webseiten.
' written in X11-Basic (C) Markus Hoffmann 2010-03-30
'
' dot a.grf -Teps -oa.eps
'
' neato a.grf -Goverlap=false -Gsplines=true -Teps -oa.eps
'
CLR count
DIM line$(100000)
DIM gravity(100000)
ARRAYFILL gravity(),0

inputfile$="/var/log/apache2/access_log"
outputfilename$="a.grf"
i=1
WHILE LEN(PARAM$(i))
  IF LEFT$(PARAM$(i))="-"
    IF PARAM$(i)="--help" OR PARAM$(i)="-h"
      @intro
      @using
    ELSE IF PARAM$(i)="--version"
      @intro
      QUIT
    ELSE IF PARAM$(i)="-o"
      INC i
      IF LEN(PARAM$(i))
        outputfilename$=PARAM$(i)
      ENDIF
    ELSE
      collect$=collect$+PARAM$(i)+" "
    ENDIF
  ELSE
    inputfile$=PARAM$(i)
    IF NOT EXIST(inputfile$)
      PRINT "ana: "+inputfile$+": file or path NOT found"
      CLR inputfile$
    ENDIF
  ENDIF
  INC i
WEND

IF LEN(inputfile$)
  OPEN "O",#2,outputfilename$
  @doit(inputfile$)
  CLOSE #2
ELSE
  PRINT "ana: No input files"
ENDIF
QUIT
PROCEDURE intro
  PRINT "Graphical WEB-Statistics V.1.15 (c) Markus Hoffmann 2005-2010"
  VERSION
RETURN
PROCEDURE using
  PRINT "Usage: ana [options] file..."
  PRINT "Options:"
  PRINT "  -h, --help               Display this information"
  PRINT "  -o <file>                Place the output into <file>"
RETURN

PROCEDURE doit(f$)
  LOCAL incount
  LOCAL outcount
  incount=0
  outcount=0
  OPEN "I",#1,f$
  PRINT #2,"digraph all {"
  PRINT #2," "+ENCLOSE$("/")+" [shape=box,color=red];"
  PRINT #2," "+ENCLOSE$("/wiki/")+" [shape=box,color=red];"
  PRINT #2," UPLOAD [shape=box,color=blue];"
  PRINT #2," SEARCH [shape=box,color=red];"
  PRINT #2," MOVE [shape=box,color=red];"
  PRINT #2," WANTED [shape=box,color=green];"
  PRINT #2," LOGIN [shape=box,color=red];"
  PRINT #2," LOGOUT [shape=box,color=red];"
  PRINT #2," CHANGES [shape=box,color=red];"
  PRINT #2," TIMETABLE_INPUT [shape=box,color=green];"
  PRINT #2," Main_Page [color=red];"
  PRINT #2," Sandbox [color=green];"
  PRINT #2," "+ENCLOSE$("timetable-visits.cgi")+" [fontcolor=red];"
  PRINT #2," "+ENCLOSE$("timetable.cgi")+" [fontcolor=red];"
  PRINT #2," "+ENCLOSE$("BRABBEL")+" [fontcolor=red];"
  PRINT #2," CONTRIBUTION [shape=box,color=green];"
  '  print #2," ALLPAGES [shape=box,color=green];"

  WHILE NOT EOF(#1)
    LINEINPUT #1,t$
    INC incount
    IF NOT GLOB(t$,"*onobook*") AND NOT GLOB(t$,"*/common/*") AND NOT \
      GLOB(t$,"*/thumb/*") AND NOT GLOB(t$,"*.css&*") AND NOT \
      GLOB(t$,"*/graphviz/*") AND NOT GLOB(t$,"*/images/*") AND NOT \
      GLOB(t$,"*opensearch_desc.php*") AND NOT \
      GLOB(t$,"*/LLRF-Logo-klein.png*") AND NOT \
      GLOB(t$,"*/9mA-plots/*") AND NOT GLOB(t$,"*/favicon.ico*") AND NOT \
      GLOB(t$,"*.js&*") AND NOT GLOB(t$,"*POST *") AND NOT \
      GLOB(t$,"*/wiki/index.php?action=ajax*") AND NOT \
      GLOB(t$,"*/api.php*")
      ' AND LEFT$(t$,12)="131.169.132."
      SPLIT t$," ",0,a$,t$
      ip$=a$
      SPLIT t$," ",0,a$,t$
      SPLIT t$," ",0,a$,t$
      SPLIT t$," ",0,a$,t$
      SPLIT t$," ",0,a$,t$
      SPLIT t$," ",0,a$,t$
      a$=REPLACE$(a$,"GET /wiki/index.php/","")
      a$=REPLACE$(a$,"GET /wiki/index.php?title=","")
      a$=REPLACE$(a$,"GET /cgi-bin/","")
      a$=REPLACE$(a$," HTTP/1.1","")
      a$=REPLACE$(a$,"GET ","")
      a$=@repl_common$(a$)

      SPLIT t$," ",0,b$,t$
      SPLIT t$," ",0,b$,t$
      SPLIT t$," ",0,b$,t$

      b$=@repl_common$(b$)

      @patmatch("*&diff=*","[label="+CHR$(34)+"diff"+CHR$(34)+",fontcolor=orange]")
      @patmatch("*S:Whatlinkshere*","[label="+CHR$(34)+"link"+CHR$(34)+",fontcolor=blue]")
      @patmatch("*PAGES*","[shape=box,fontcolor=blue,color=green]")
      @patmatch("*&printable=yes*","[label="+CHR$(34)+"print"+CHR$(34)+",fontcolor=blue]")
      @patmatch("*->history*","[label="+CHR$(34)+"history"+CHR$(34)+",fontcolor=green]")
      @patmatch("*->submit*","[label="+CHR$(34)+"submit"+CHR$(34)+",fontcolor=blue]")
      @patmatch("*->edit*","[label="+CHR$(34)+"edit"+CHR$(34)+",fontcolor=green]")
      @patmatch("*->delete*","[label="+CHR$(34)+"delete"+CHR$(34)+",fontcolor=red]")

      @patmatch("*/selfhtml/*.gif*","[label="+CHR$(34)+".gif"+CHR$(34)+"]")
      @patmatch("*.css*","[label="+CHR$(34)+".css"+CHR$(34)+"]")

      @patmatch("SPEECH","[shape=box,fontcolor=red]")
      @patmatch("BOOK","[shape=box,color=blue]")
      @patmatch("COLLECTION","[shape=box,color=green]")

      IF b$=ENCLOSE$("-")
        PRINT #2,ENCLOSE$("-"+ip$+"-");" [shape=box, color=red, label="+ENCLOSE$(ip$)+"];"
        PRINT #2,ENCLOSE$("-"+ip$+"-");" -> ";a$;" ;"
      ELSE IF GLOB(b$,"*http://*")
        PRINT #2,b$+" [shape=box, color=blue, fontcolor=red];"
        @addline(b$+" -> "+a$)
      ELSE
        @addline(b$+" -> "+a$)
      ENDIF
      INC outcount
    ENDIF
  WEND

  IF count>0
    last$=""
    FOR i=0 TO count-1
      IF line$(i)<>last$
        IF gravity(i)>1
          PRINT #2,line$(i)+" [style=bold,weight="+STR$(gravity(i))+",label="+STR$(gravity(i))+"] ;"
        ELSE
          PRINT #2,line$(i)+" ;"
        ENDIF
        last$=line$(i)
      ENDIF
    NEXT i
  ENDIF
  'print #2,"overlap=scale"
  'print #2,"size=10"
  PRINT #2,"fontname="+ENCLOSE$("Arial")+";"
  PRINT #2,"graph [fontname="+ENCLOSE$("Arial")+"];"
  PRINT #2,"concentrate=true;"
  PRINT #2,"label="+ENCLOSE$(f$+"\n produced with X11-Basic and graphviz "+DATE$+" "+TIME$)+";"
  PRINT #2,"}"
  CLOSE #1
  PRINT incount;" -> ";outcount
RETURN

PROCEDURE patmatch(mpat$,mmatch$)
  IF GLOB(a$,mpat$) OR GLOB(b$,mpat$)
    IF GLOB(a$,mpat$)
      PRINT #2,a$+" "+mmatch$+" ;"
    ELSE IF GLOB(b$,mpat$)
      PRINT #2,b$+" "+mmatch$+" ;"
    ENDIF
    PRINT "p";
    FLUSH
    INC outcount
  ENDIF
RETURN
PROCEDURE addline(lll$)
  LOCAL i
  IF count>0
    FOR i=0 TO count-1
      IF line$(i)=lll$
        gravity(i)=gravity(i)+1
	RETURN
      ENDIF
    NEXT i
    line$(count)=lll$
    gravity(count)=1
    PRINT ".";
    INC count
    FLUSH
  ELSE
    line$(count)=lll$
    gravity(count)=1
    INC count
    PRINT ".";
    FLUSH
  ENDIF
RETURN

FUNCTION repl_common$(in$)
  oo$=in$
  oo$=REPLACE$(oo$,"mskpc14.desy.de","mskpc14")
  oo$=REPLACE$(oo$,"msk.desy.de","msk")
  oo$=REPLACE$(oo$,"localhost:8080","mskpc14")
  oo$=REPLACE$(oo$,"%2F","/")
  oo$=REPLACE$(oo$,"http://mskpc14/wiki/index.php?title=","")
  oo$=REPLACE$(oo$,"http://mskpc14/wiki/index.php/","")
  oo$=REPLACE$(oo$,"http://mskpc14/cgi-bin/","")
  oo$=REPLACE$(oo$,"http://mskpc14/","/")
  oo$=REPLACE$(oo$,"http://msk/","m:/")
  oo$=REPLACE$(oo$,"%3A",":")
  oo$=REPLACE$(oo$,"%40","@")
  oo$=REPLACE$(oo$,"%27","'")
  oo$=REPLACE$(oo$,"%20","-")
  oo$=REPLACE$(oo$,"&action=","->")
  oo$=REPLACE$(oo$,"?action=","->")
  oo$=REPLACE$(oo$,"&oldid=","|")
  oo$=REPLACE$(oo$,"&section=","#")
  oo$=REPLACE$(oo$,"&redirect=no","")
  oo$=REPLACE$(oo$,"&go=Go","")
  oo$=REPLACE$(oo$,"&feed=atom","")
  oo$=REPLACE$(oo$,"&feed=rss","")
  oo$=REPLACE$(oo$,"&from=Mskpc14","")
  oo$=REPLACE$(oo$,"&from=","@")
  oo$=REPLACE$(oo$,"&prefix=","!")
  oo$=REPLACE$(oo$,"&go=Seite","")
  oo$=REPLACE$(oo$,"&direction=next","")
  oo$=REPLACE$(oo$,"&hidemyself=1","")
  oo$=REPLACE$(oo$,"/2007/02","")
  oo$=REPLACE$(oo$,"&namespace=0","")
  oo$=REPLACE$(oo$,"->raw&","->raw")
  oo$=REPLACE$(oo$,"Special:Whatlinkshere/","L:")
  IF LEFT$(oo$,15)=CHR$(34)+"Special:Search"
    oo$="SEARCH"
  ELSE IF LEFT$(oo$,15)=CHR$(34)+"Special:Upload"
    oo$="UPLOAD"
  ELSE IF LEFT$(oo$,14)=CHR$(34)+"Special:Book/"
    oo$="BOOK"
  ELSE IF LEFT$(oo$,28)=CHR$(34)+"/wiki/extensions/Collection"
    oo$="COLLECTION"
  ELSE IF GLOB(oo$,"*eingabemaske.cgi?*")
    oo$="TIMETABLE_INPUT"
  ELSE IF GLOB(oo$,"*mandel.cgi?*")
    oo$="MANDEL"
  ELSE IF GLOB(oo$,"*brabbel.cgi?*")
    oo$="BRABBEL"
  ELSE IF GLOB(oo$,"*sprachserver.cgi?*")
    oo$="SPEECH"
  ELSE IF GLOB(oo$,"*mandelgif.cgi?*")
    oo$="MANDELGIF"
  ELSE IF GLOB(oo$,"*mandelposter.cgi?*")
    oo$="MANDELPOSTER"
  ELSE IF GLOB(oo$,"*Special:Movepage*")
    oo$="MOVE"
  ELSE IF GLOB(oo$,"*Special:Wantedpage*")
    oo$="WANTED"
  ELSE IF GLOB(oo$,"*Special:Userlogin*")
    oo$="LOGIN"
  ELSE IF GLOB(oo$,"*Special:Lonelypag*")
    oo$="LONELYPAGES"
  ELSE IF GLOB(oo$,"*Special:Userlogou*")
    oo$="LOGOUT"
  ELSE IF GLOB(oo$,"*Special:Contribut*")
    oo$="CONTRIBUTION"
  ELSE IF GLOB(oo$,"*Special:Allpages*")
    oo$="ALLPAGES"
  ELSE IF GLOB(oo$,"*Special:Recentchanges*")
    oo$="CHANGES"
  ELSE IF GLOB(oo$,"*/ana_poster/a.png.*")
    oo$=ENCLOSE$(".png")
  ELSE IF GLOB(oo$,"*/ana_poster/*/a.png.*")
    oo$=ENCLOSE$(".png")
  ENDIF
  oo$=REPLACE$(oo$,"Special:","S:")
  oo$=REPLACE$(oo$,"Image:","I:")
  oo$=REPLACE$(oo$,"User:","U:")
  oo$=REPLACE$(oo$,"/Files/","F:")
  RETURN oo$
ENDFUNCTION
